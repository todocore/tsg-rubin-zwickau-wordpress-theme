# Für Entwickler

## Voraussetzungen für die Arbeit an diesem Projekt
- Wenn nötig, Linux (z.B. XUbuntu) in einer VM installieren
- ggf. aktuelle Quellen für unten stehende Pakete ermitteln und dem System hinzufügen
- Paket `docker-ce` installieren
- Paket `nodejs` installieren
- Paket `git` installieren
- Pakete für bevorzugte IDE installieren (z.B. "Visual Studio Code")
- Alias für `localhost` in `/etc/hosts` eintragen (z.B. `rubin.dev`)

## Vorbereitung der Entwicklungsumgebung
- dieses Git-Projekt klonen (`git clone ...`)
- aktuelle Version von Wordpress herunterladen und nach `docker_wordpress` entpacken
- Docker in den Swarm-Modus versetzen (`docker swarm init`)
- Webseite im Browser mit oben vergebenem Alias (z.B. `http://rubin.dev`) aufrufen und Wordpress installieren (DB-Daten sind in `docker/stack.yml` ersichtlich)
- in den WP-Dateien einen symbolischen Link auf das Theme anlegen (`cd ./docker_wordpress/wp_content/themes && ln -sv ../../../src/rubin-zwickau-theme.dev rubin-zwickau-theme`)
- in Wordpress anmelden und Rubin-Theme aktivieren

## Entwicklungs-Loop
- eigene IDE starten
- `./webspace up` startet lokalen Webserver (mit `STRG+C` abbrechen, sobald alle Container gestartet sind)
- `./webspace dev` startet den Stylsheet-Prozessor
- ... {*tun, was immer zu tun ist*}
- noch laufendes `./webspace dev` mit `STRG+C` abbrechen
- `./webspace down` beendet den lokalen Webserver

Der aktuelle Zustand des lokalen Webservers kann mit `./webspace` ermittelt werden. (Ob die Docker-Container gestartet sind.)

Serverlogs können durch Ausführung von `./webspace watch` überwacht werden. Das Skript kann mit `STRG+C` wieder beendet werden.

# Für Benutzer

Das Theme ist speziell für die Anwendungsfälle des TSG Rubin Zwickau e.V. erstellt.

Datenstrukturen und Funktionen können nicht in allen Fällen 1:1 für andere Vereine übernommen werden.

## MGVO-Anbindung

Der API-Zugang kann im Wordpress Customizer eingestellt werden.

Die Cache-Zeit bestimmt, wie schnell Daten aus MGVO auf die Webseite übertragen und dann dort sichtbar sind werden.

## Gutenberg Blöcke

- Trainingsabsagen
- Kalender für Events mit verschiedenen Layouts
- Trainingsübersicht mit Kontaktmöglichkeit für Terminanfragen
- Hero-Image mit variable Positionierung für z.B. die Startseite

## öffentliche CSS-Klassen zur Verwendung mit Widget-Containern

- `rubin-widget-column` - für Spalten in denen Widgets des Themes enthalten sind
- `rubin-widget-row` - für Zeilen in denen Widgets des Themes enthalten sind
- `align-to-rubin-widget-column` - für Spalten, die neben einer Widget-Spalte angezeigt werden
- `align-to-rubin-widget-row` - für Zeilen, die unter oder über einer Widget-Zeile angezeigt werden
- `align-to-rubin-widget` - für Bereiche, die an eine Widget-Spalte und eine Widget-Zeile angrenzen

## empfohlene Wordpress-Plugins zur Verwendung mit diesem Theme

- ***Duplicator***: zum Backup und Verschiebung der Wordpress-Installation
- ***FileBird Lite***: Bessere Verwaltung der Mediathek
- ***Simple Image Widget***: zur Anzeige von Sponsoren oder ähnlichen verlinkten Bildern
- ***Smart Slider 3***: Anzeige von Bilderserien als animierter Slider und mehr
- ***WP Forms Lite***: zur Anzeige von einfachen (Kontakt)Formularen
- ***WPS Hide Login***: um die Login-URL aus dem Schussfeld von Bots und Crawlern zu nehmen
- ein Cookie-Banner-Plugin (z.B. ***Complianz***)