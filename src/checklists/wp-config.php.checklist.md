# disable file edit
**add to config**
```
define( 'DISALLOW_FILE_EDIT', true );
```

# WP_CACHE
**set**
```
define( 'WP_CACHE', true );
```

# WP_DEBUG
**set**
```
define( 'WP_DEBUG', true );
```
**or unset**
```
define( 'WP_DEBUG', false );
```

if this dows not help use this in addition:
```
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);
```

# constant for security test
**add to config**
```
define( 'ALLOW_RUBIN_THEME', 'ALLOW_RUBIN_THEME' );
```