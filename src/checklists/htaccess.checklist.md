# .htaccess

**Add outside of WORDPRESS block**
```
<IfModule mod_rewrite.c>
	RewriteEngine On

  RewriteRule "^.*wp_config.php.*$" '-' [NC,F]

  RewriteRule "^.*\.cache.*$" '-' [NC,F]

  RewriteCond "%{REMOTE_ADDR}" "!=%{SERVER_ADDR}"
  RewriteRule "^.*(xmlrpc.php|wp-mail.php|wp-cron.php|wp-login).*$" '-' [NC,F]
</IfModule>
```

**Add on testsystenm**
```
AuthType Basic
AuthName "Passwort notwendig"
AuthUserFile {absolute file path on server}/.htpasswd
AuthGroupFile /dev/null
Require valid-user
```

use this to find absolute file path on server for `AuthUserFile`:
```
<?php
echo $_SERVER['SCRIPT_FILENAME'];
?>
```
