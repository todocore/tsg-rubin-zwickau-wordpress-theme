<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_handle_mgvo_training_sendmail(WP_REST_Request $request) {
  global $mgvoService;

  if (!empty($request['city']) || !empty($request['street'])) {
    $wpResponse = new WP_REST_Response($response);
    return $wpResponse;
  }

  $response = array();
  $WEEK = array(
    'Mo' => __('Montag', RUBIN_TEXT_DOMAIN),
    'Di' => __('Dienstag', RUBIN_TEXT_DOMAIN),
    'Mi' => __('Mittwoch', RUBIN_TEXT_DOMAIN),
    'Do' => __('Donnerstag', RUBIN_TEXT_DOMAIN),
    'Fr' => __('Freitag', RUBIN_TEXT_DOMAIN),
    'Sa' => __('Samstag', RUBIN_TEXT_DOMAIN),
    'So' => __('Sonntag', RUBIN_TEXT_DOMAIN),
  );

  $data = $mgvoService->getMGVO()->read_betreuer();
  $trainer = array();
  if (is_array($data) && is_array($data['objar'])) {
    foreach ($data['objar'] as $entity) {
      $key = $entity['trid'];
      if ($entity['inaktiv'] != '1') {
        $trainer[$key] = array(
          'id' => $key,
          'email' => $entity['email'],
          'vorname' => $entity['vorname'],
        );
      }
    }
  }
  if (sizeof($trainer) === 0) {
    $response['reason'] = 'could not fetch trainer data from MGVO';
    $wpResponse = new WP_REST_Response($response, 500);
    return $wpResponse;
  }

  $gid = $request['group'];
  $group = false;
  $toTrainer = '';
  $cc = '';
  $data = $mgvoService->getMGVO()->read_gruppen();
  if (is_array($data) && is_array($data['objar'])) {
    foreach ($data['objar'] as $entity) {
      foreach ($entity['gruzar'] as $subgroup) {
        $woTag = $mgvoService->unifiedWeekday($subgroup['wotag']);
        $key = $mgvoService->trainingGroupId(
          $entity['grukat'],
          $entity['gruid'],
          $subgroup['ortid'],
          $woTag,
          $subgroup['startzeit'],
          $subgroup['endzeit'],
          $entity['lfdnr']
        );

        if ($key === $gid) {
          $group = $entity;
          $group['day'] = $WEEK[$woTag];
          $group['ort'] = $subgroup['ortbez'];
          $group['startzeit'] = $subgroup['startzeit'];
          $group['endzeit'] = $subgroup['endzeit'];
          $group['isFull'] = (!emptyval($group['gzfld02']) && !emptyval($woTag)) ? substr_count(strtolower($group['gzfld02']), strtolower($woTag)) > 0 : false;
          $toTrainer = $trainer[$subgroup['trid']];
          $all = explode(';', $entity['tridall']);
          if (is_array($all)) {
            foreach ($all as $one) {
              $coTrainer = $trainer[$one];
              if (isset($coTrainer) && $coTrainer['id'] !== $toTrainer['id']) {
                $cc .= ($cc === '' ? '' : ',').$coTrainer['email'];
              }
            }
          }
          break;
        }
      }
      if ($group != false) {
        break;
      }
    }
  }
  if ($toTrainer === '') {
    $response['reason'] = 'could not find trainer for given group';
    $wpResponse = new WP_REST_Response($response, 500);
    return $wpResponse;
  }

  $forcesTo = get_theme_mod('development-training-contact-setting');
  $bcc = get_theme_mod('mgvo-bcc-setting');

  $from = $request['email'];
  $phone = $request['phone'];
  $fromName = $request['name'];
  $forName = $request['otherName'];
  $who = $request['who'];
  $age = $request['age'];
  $skill = $request['skill'];
  $text = $request['text'];
  $dsgvo = $request['dsgvo'];
  $member = $request['member'];

  if ($group['isFull'] === true) {
    $subject = __('TSG Rubin Zwickau: neuer Eintrag in die WARTELISTE', RUBIN_TEXT_DOMAIN);
  } else {
    $subject = __('TSG Rubin Zwickau: neue Terminanfrage', RUBIN_TEXT_DOMAIN);
  }

  $body = '';
  $toVorname = isset($toTrainer['vorname']) ? $toTrainer['vorname'] : 'VORNAME';
  $toEmail = isset($toTrainer['email']) ? $toTrainer['email'] : 'EMAIL';
  if (!emptyval($forcesTo)) {
    $body .= __('################################################################################', RUBIN_TEXT_DOMAIN)."\n\n";
    $body .= __('Diese Mail wäre eigentlich an folgende Empfänger gesendet worden', RUBIN_TEXT_DOMAIN)."\n\n";
    $body .= '   TO: '.$toEmail."\n";
    $body .= '   CC: '.$cc."\n";
    $body .= '   BCC: '.$bcc."\n\n";
    $body .= __('Aber in Wordpress ist eine Umleitung auf Deine Emailadresse gesetzt.', RUBIN_TEXT_DOMAIN)."\n\n";
    $body .= __('################################################################################', RUBIN_TEXT_DOMAIN)."\n\n";
  }
  $body .= __('Hallo', RUBIN_TEXT_DOMAIN).' '.$toVorname.','."\n\n";
  if ($group['isFull'] === true) {
    $body .= __('auf unserer Homepage gab es einen neuen Eintrag in die WARTELISTE für folgende Trainingsgruppe:', RUBIN_TEXT_DOMAIN)."\n\n";
  } else {
    $body .= __('auf unserer Homepage gab es eine neue Anfrage für folgende Trainingsgruppe:', RUBIN_TEXT_DOMAIN)."\n\n";
  }
  $body .= '   '.$group['grukat'].' - '.$group['grubez']."\n";
  $body .= '   '.$group['ort']."\n";
  $body .= '   '.$group['day'].', '.$group['startzeit'].' bis '.$group['endzeit']."\n\n";
  $body .= __('Folgende Daten wurden bei der Anfrage angegeben:', RUBIN_TEXT_DOMAIN)."\n\n";
  $body .= '   '.__('Name:', RUBIN_TEXT_DOMAIN).' '.$fromName."\n";
  $body .= '   '.__('Email:', RUBIN_TEXT_DOMAIN).' '.$from."\n";
  $body .= '   '.__('Telefon:', RUBIN_TEXT_DOMAIN).' '.$phone."\n";
  $body .= '   '.__('Bestimmung:', RUBIN_TEXT_DOMAIN).' ';
  if ($who === 'me') {
    $body .= __('Für sich selbst', RUBIN_TEXT_DOMAIN)."\n";
  } else if ($who === 'child') {
    $body .= __('Für ein Kind', RUBIN_TEXT_DOMAIN);
    if (!emptyval($forName)) {
      $body .= ' '.__('namens', RUBIN_TEXT_DOMAIN).' '.$forName."\n";
    } else {
      $body .= ' '.__('(Name wurde nicht angegeben)', RUBIN_TEXT_DOMAIN)."\n";
    }
  } else if ($who === 'other') {
    $body .= __('Für eine andere Person (eventuell Gutschein)', RUBIN_TEXT_DOMAIN);
    if (!emptyval($forName)) {
      $body .= ' '.__('namens', RUBIN_TEXT_DOMAIN).' '.$forName."\n";
    } else {
      $body .= ' '.__('(Name wurde nicht angegeben)', RUBIN_TEXT_DOMAIN)."\n";
    }
  } else {
    $body .= __('keine Angabe (das sollte nicht passieren - bitte informiere den Homepage-Admin darüber)', RUBIN_TEXT_DOMAIN)."\n";
  }
  $body .= '   '.__('Mitgliedschaft:', RUBIN_TEXT_DOMAIN).' '.$member."\n";
  if (emptyval($age)) {
    $body .= '   '.__('Alter:', RUBIN_TEXT_DOMAIN)."\n";
  } else {
    $body .= '   '.__('Alter:', RUBIN_TEXT_DOMAIN).' '.$age.' '.__('Jahre', RUBIN_TEXT_DOMAIN)."\n";
  }
  $body .= '   '.__('Tanzerfahrung:', RUBIN_TEXT_DOMAIN).' ';
  if ($skill === 'no') {
    $body .= __('Keine', RUBIN_TEXT_DOMAIN)."\n\n";
  } else if ($skill === 'beginner') {
    $body .= __('Einsteiger', RUBIN_TEXT_DOMAIN)."\n\n";
  } else if ($skill === 'advanced') {
    $body .= __('Fortgeschritten', RUBIN_TEXT_DOMAIN)."\n\n";
  } else {
    $body .= __('keine Angabe (das sollte nicht passieren - bitte informiere den Homepage-Admin darüber)', RUBIN_TEXT_DOMAIN)."\n\n";
  }
  if (!emptyval($text)) {
    $body .= __('Desweiteren gab es folgende Anmerkungen zur Anfrage:', RUBIN_TEXT_DOMAIN)."\n";
    $body .= __('--------------------------------------------------------------------------------', RUBIN_TEXT_DOMAIN)."\n\n";
    $body .= $text."\n\n";
    $body .= __('--------------------------------------------------------------------------------', RUBIN_TEXT_DOMAIN)."\n\n";
  }
  if ($group['isFull'] === true) {
    $body .= __('Bitte teile dem Absender zeitnah mit, mit welcher Wartezeit zu rechnen wäre.', RUBIN_TEXT_DOMAIN)."\n";
  } else {
    $body .= __('Bitte melde Dich zeitnah beim Absender.', RUBIN_TEXT_DOMAIN)."\n";
  }
  $body .= __('(Du kannst dafür direkt auf diese Mail antworten, wenn Du magst.)', RUBIN_TEXT_DOMAIN)."\n\n\n";
  $body .= __('Viele Grüße', RUBIN_TEXT_DOMAIN)."\n";
  $body .= __('Dein TSG Rubin', RUBIN_TEXT_DOMAIN)."\n";
  $body .= "\n\n";
  if (isset($dsgvo) && strlen($dsgvo) > 0) {
    $body .= __('P.S.: Der Absender war mit folgendem DSGVO-Hinweis einverstanden:', RUBIN_TEXT_DOMAIN)."\n";
    $body .= $dsgvo."\n";
  } else {
    $body .= __('P.S.: Der Absender war NICHT mit dem DSGVO-Hinweis einverstanden.', RUBIN_TEXT_DOMAIN)."\n";
  }

  $to = emptyval($forcesTo) ? $toEmail : $forcesTo;
  $cc = emptyval($forcesTo) ? $cc : '';
  $bcc = emptyval($forcesTo) ? $bcc : '';

  $sent = wp_mail($to, $subject, $body, array(
    'From: '.$from,
    'Reply-To: '.$from,
    'Cc: '.$cc,
    'Bcc: '.$bcc
  ));
  if (!$sent) {
    $response['reason'] = 'error while sending email';
    // $response['email'] = array(
    //   'From: '.$from,
    //   'To: '.$to,
    //   'Reply-To: '.$from,
    //   'Cc: '.$cc,
    //   'Bcc: '.$bcc,
    //   'Subject: '.$subject,
    //   'Body: '.$body
    // );
    $wpResponse = new WP_REST_Response($response, 500);
    return $wpResponse;
  }

  $wpResponse = new WP_REST_Response($response);
  return $wpResponse;
}
add_action('rest_api_init', function () {
  register_rest_route( 'rubin/v1', '/mgvo/training/sendmail', array(
    'methods' => 'POST',
    'callback' => 'rubin_handle_mgvo_training_sendmail',
  ));
});
