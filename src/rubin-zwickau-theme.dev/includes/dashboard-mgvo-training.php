<?php
defined(ALLOW_RUBIN_THEME) or die();

class Rubin_Dashboard_MGVO_Training {

  public function register() {
    wp_add_dashboard_widget(
      'rubin_dashboard_mgvo_training',
      __('MGVO Trainingsgruppen', RUBIN_TEXT_DOMAIN),
      array($this, 'show'),
      array($this, 'createPages'),
    );
  }

  public function show() {
    global $mgvoService;

    $data = $mgvoService->fetchTrainingGroups();
    $groups = $this->sanitize_groups($data);
    get_template_part('/parts/widget/mgvo-training/dashboard', null, array(
      'groups' => $groups,
      'editable' => false,
    ));
  }

  public function createPages() {
    global $mgvoService;

    $data = $mgvoService->fetchTrainingGroups();
    $groups = $this->sanitize_groups($data);

    if (isset($_POST['submit'])) {
      foreach ($groups as $group) {
        if (isset($_POST['RUBIN_CREATE_'.$group['id']])) {
          wp_insert_post(array(
            'post_type' => 'page',
            'post_title' => __('Informationen zur Trainingskategorie ', RUBIN_TEXT_DOMAIN).$group['name'],
            'post_name' => $group['url'],
          ));
        }
      }
    }

    get_template_part('/parts/widget/mgvo-training/dashboard', null, array(
      'groups' => $groups,
      'editable' => true,
    ));
  }
  
  private function sanitize_groups($entities=array()) {
    $groups = array();
    foreach($entities['objar'] as $entity) {
      $cId = $entity['grukat'];
      $url = 'trainingskategorie-'.strtolower($cId);
      if (!isset($groups[$cId])) {
        $groups[$cId] = array(
          'id' => $cId,
          'name' => $entity['kbez'],
          'url' => $url,
          'exists' => get_page_by_path($url),
        );
      }
    }
    return $groups;
  }

}

if (!isset($rubinDashboardWidgetMgvoTraining)) {
  $rubinDashboardWidgetMgvoTraining = new Rubin_Dashboard_MGVO_Training();
}
add_action('wp_dashboard_setup', array($rubinDashboardWidgetMgvoTraining, 'register'));
