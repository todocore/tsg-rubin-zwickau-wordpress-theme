<?php
defined(ALLOW_RUBIN_THEME) or die();

define('RUBIN_TRAINING_SHOW', 'WIDGET_SHOW');
define('RUBIN_TRAINING_ASK', 'WIDGET_ASK');

function rubin_enqueue_block_scripts() {
  $blockAssets = include(get_template_directory().'/assets/blocks.asset.php');
  wp_enqueue_script('rubin-blocks', get_template_directory_uri().'/assets/js/blocks.js', $blockAssets['dependencies'], $blockAssets['version']);
}
add_action('admin_enqueue_scripts', 'rubin_enqueue_block_scripts');