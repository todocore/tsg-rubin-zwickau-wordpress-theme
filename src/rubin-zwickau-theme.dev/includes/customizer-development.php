<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_register_customizer_development_section($wp_customize) {
  $wp_customize->add_section('development-section', array(
    'title' => __('Theme Entwicklung', RUBIN_TEXT_DOMAIN),
    'priority' => 999,
    'description' => __('Einstellungen für den Theme-Entwickler (sollten im produktiven Umfeld nicht ohne Grund verwendet werden)', RUBIN_TEXT_DOMAIN) 
  ));

  $wp_customize->add_setting('development-breakpoint-helper-setting', array('default' => false));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'development-breakpoint-helper-control', array(
    'label' => __('Aktuellen Breakpoint anzeigen?', RUBIN_TEXT_DOMAIN),
    'section' => 'development-section',
    'settings' => 'development-breakpoint-helper-setting',
    'type' => 'checkbox'
  )));

  $wp_customize->add_setting('development-vue-devmode-setting', array('default' => false));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'development-vue-devmode-control', array(
    'label' => __('VueJS im Entwicklermodus nutzen?', RUBIN_TEXT_DOMAIN),
    'section' => 'development-section',
    'settings' => 'development-vue-devmode-setting',
    'type' => 'checkbox'
  )));

  $wp_customize->add_setting('development-training-contact-setting', array('default' => ''));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'development-training-contact-control', array(
    'label' => __('Terminanfragen auf diese Emailadresse umleiten', RUBIN_TEXT_DOMAIN),
    'section' => 'development-section',
    'settings' => 'development-training-contact-setting',
    'type' => 'email'
  )));
}
add_action('customize_register', 'rubin_register_customizer_development_section');
