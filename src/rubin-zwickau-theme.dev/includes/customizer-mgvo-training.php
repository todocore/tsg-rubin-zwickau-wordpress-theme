<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_register_customizer_mgvo_training_section($wp_customize) {
  $wp_customize->add_section('mgvo-training-section', array(
    'title' => __('MGVO: Training-Widget', RUBIN_TEXT_DOMAIN),
    'priority' => 10,
    'description' => __('MGVO: Einstellungen zum Trainings-Widget', RUBIN_TEXT_DOMAIN) 
  ));

  $wp_customize->add_setting('mgvo-bcc-setting', array('default' => ''));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'mgvo-bcc-control', array(
    'label' => __('Terminanfragen immer auch an diese Emailadresse(n) senden (BCC):', RUBIN_TEXT_DOMAIN),
    'section' => 'mgvo-training-section',
    'settings' => 'mgvo-bcc-setting',
    'type' => 'email',
    'input_attrs' => array(
			'placeholder' => __( 'kommaseparierte Liste möglich' ),
      'multiple' => 'multiple'
    )
  )));

  $wp_customize->add_setting('mgvo-ignore-setting', array('default' => ''));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'mgvo-ignore-control', array(
    'label' => __('Standard-Prefix für zu ignorierende Trainingsgruppen (kann pro Widget überschrieben werden):', RUBIN_TEXT_DOMAIN),
    'section' => 'mgvo-training-section',
    'settings' => 'mgvo-ignore-setting',
    'type' => 'text',
    'input_attrs' => array(
			'placeholder' => __( 'kommaseparierte Liste möglich' ),
    )
  )));
}
add_action('customize_register', 'rubin_register_customizer_mgvo_training_section');
