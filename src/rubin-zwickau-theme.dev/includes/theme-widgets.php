<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_register_widgets() {
  register_sidebar(
    array(
      'id' => 'footer_widgets',
      'name' => __('Footer', RUBIN_TEXT_DOMAIN),
      'description' => __('Widgets im oberen Footer-Bereich', RUBIN_TEXT_DOMAIN),
      'before_title' => '<div class="tw-font-bold">',
      'after_title' => '</div>',
      'before_widget' => '<div class="rubin-footer-widget tw-px-4">',
      'after_widget' => '</div>'
    ),
  );
}
add_action('widgets_init', 'rubin_register_widgets');
