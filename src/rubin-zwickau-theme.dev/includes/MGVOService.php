<?php
defined(ALLOW_RUBIN_THEME) or die();

include_once get_template_directory().'/mgvo-api/libs/mgvo_hpapi.php';

class MGVOService {

  var $mgvo;
  function getMGVO() {
    return $this->mgvo;
  }

  function __construct($userId,$secret,$cacheTime,$upload_dir) {
    $this->mgvo = new MGVO_HPAPI($userId,$secret,$cacheTime,$upload_dir);
  }

  function fetchEventsByYear($calNr, $year) {
    $events = $this->mgvo->read_vkal($calNr,date('Y', $year));
    if (!is_array($events)) {
      $events = array();
    }
    return $events;
  }

  function fetchRefusals() {
    $refusals = $this->mgvo->read_training_fail();
    if (!is_array($refusals)) {
      $refusals = array();
    }

    $refusals = array_filter(is_array($refusals['objar']) ? $refusals['objar'] : array(), function($r) {
      return !emptyval($r['sdat']) && !emptyval($r['starttime']);
    });

    return $refusals;
  }

  function fetchTrainingGroups() {
    $groups = $this->mgvo->read_gruppen();
    if (!is_array($groups)) {
      $groups = array();
    }
    return $groups;
  }

  function sanitizeEventsByDate($events) {
    $rows = array();
    foreach($events['objar'] as $idx => $event) {
      $startdat = $event['startdat'];
      $enddat = $event['enddat'];
      if (emptyval($enddat)) {
        $enddat = $startdat;
      }

      $key = $event["ortid"].$event['startzeit'];
      $row=array(
        'id' => $idx,
        'color' => $event['bgcol'],
        'gid' => $event["gid"],
        'name' => $event["bez"],
        'ortid' => $event["ortid"],
        'ort' => $event["ortbez"],
        'trainer' => emptyval($event['mgname']) ? '' : $event['mgname'],
        'startdat' => $startdat,
        'enddat' => $enddat,
        'tage' => $this->uiEventDateRange($startdat, $enddat),
        'startzeit' => $event['startzeit'],
        'endzeit' => $event['endzeit'],
        'zeitraum' => $this->uiEventTimeRange($event['startzeit'], $event['endzeit']),
        'fromCalendar' => !emptyval($event['vkalnr']),
        'fromEvent' => !emptyval($event['eventnr']),
        'sort' => $event['ortbez'].$event['startzeit'],
        'type' => $event['type'],
      );

      if ($startdat === $enddat) {
        $date = $startdat;
        if (!array_key_exists($date, $rows)) {
          $rows[$date] = array();
        }
        $rows[$date][$key] = $row;
      } else {
        $begin = new DateTime($startdat);
        $end = new DateTime($enddat);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $dt) {
          $date = $dt->format("Y-m-d");
          if (!array_key_exists($date, $rows)) {
            $rows[$date] = array();
          }
          $rows[$date][$key] = $row;
        }
      }
    }
    return $rows;
  }

  function sanitizeRefusals($refusals, $includePassed, $instance) {
    $limitByDays = $instance['limitByDays'];
    $ignore = $instance['ignore'];
    $now = strtotime('now');
    $deadline = !isset($limitByDays) || $limitByDays == 0 ? strtotime('+ 10 years', $now) : strtotime('+ '.($limitByDays - 1).' days', $now);
    $lastDay = date('Ymd', $deadline);

    $data = array();
    foreach ($refusals as $refusal) {
      $gId = $refusal['gruid'];
      $gBez = $refusal['grbez'];
      $oId = $refusal['ortid'];
      $oBez = $refusal['ortsbez'];
      $day = $refusal['sdat'];
      $tym = $refusal['starttime'];
      $key = $gId.$oId.$day.$tym;

      if ($this->doesMatchOne($ignore, [$gId, $gBez, $oId, $oBez])) {
        continue;
      }

      $today = date('Ymd', strtotime('today'));
      $date = date('Ymd', strtotime($refusal['sdat']));
      $time = date('His', strtotime($refusal['starttime']));
      $pubDate = date('Y-m-d', strtotime($refusal['sdat'])).' '.date('H:i:s', strtotime($refusal['starttime']));
      $pubDate = strtotime($pubDate);

      if ($date > $lastDay) {
        continue;
      }

      $refusal['zeitraum'] = $this->uiTrainingTimeRange($refusal['starttime'], $refusal['endtime']);
      $refusal['sort'] = $date.$time;
      $refusal['pubDate'] = strtotime('-3 hours', $pubDate);
      if ($includePassed || $date >= $today) {
        $data[$key] = $refusal;
      }
    }

    uasort($data, function($a, $b) {
      return strcmp($a['sort'], $b['sort']);
    });

    return $data;
  }

  function convertTrainingGroupsToEvents($groups, $refusals, $instance) {
    $includeOpen = $instance['includeOpen'];
    $includeClosed = $instance['includeClosed'];
    $includeRefusals = $instance['includeRefusals'];
    $ignore = $instance['ignore'];

    $rows = array();
    foreach($groups['objar'] as $group) {
      if ($group['inaktiv'] === '1') {
        continue;
      }

      $isOpen = $group['gzfld01'] === 'offen';
      if (($isOpen && !$includeOpen) || (!$isOpen && !$includeClosed)) {
        continue;
      }

      $gId = $group['gruid'];
      $gBez = $group['grubez'];
      $gCol = $group['bgcol'];
      $gTrn = $group['trname'];

      foreach ($group['gruzar'] as $subgroup) {
        $oId = $subgroup['ortid'];
        $oBez = $subgroup['ortbez'];
        $gStart = $subgroup['startzeit'];
        $gEnde = $subgroup['endzeit'];

        if ($this->doesMatchOne($ignore, [$gId, $gBez, $oId, $oBez])) {
          continue;
        }

        $nextDays = $this->getNextTrainingDays($group, $subgroup, $refusals, $includeRefusals, 1095, false);
        foreach ( $nextDays as $nextDay ) {
          if ($nextDay['refused'] && $includeRefusals) {
            $rows[] = array(
              'startdat' => $nextDay['date'],
              'enddat' => $nextDay['date'],
              'ortid' => $oId,
              'ortbez' => $oBez,
              'bgcol' => $gCol,
              'gid' => $gId,
              'bez' => $oBez.': frei',
              'mgname' => '',
              'startzeit' => $gStart,
              'endzeit' => $gEnde,
              'sort' => $oBez.$gStart,
              'type' => 'TRAINING'
            );
          } else {
            $rows[] = array(
              'startdat' => $nextDay['date'],
              'enddat' => $nextDay['date'],
              'ortid' => $oId,
              'ortbez' => $oBez,
              'bgcol' => $gCol,
              'gid' => $gId,
              'bez' => $gBez,
              'mgname' => $gTrn,
              'startzeit' => $gStart,
              'endzeit' => $gEnde,
              'sort' => $oBez.$gStart,
              'type' => 'TRAINING'
            );
          }
        }
      }
    }

    return array('objar' => $rows);
  }

  function sanitizeTrainingGroupsByWeek($entities=array(), $refusals, $instance) {
    if (!is_array($entities)) {
      return array();
    }

    if (!is_array($entities['objar'])) {
      return array();
    }

    $WEEK = array(
      'Mo' => array('name' => __('Montag', RUBIN_TEXT_DOMAIN)),
      'Di' => array('name' => __('Dienstag', RUBIN_TEXT_DOMAIN)),
      'Mi' => array('name' => __('Mittwoch', RUBIN_TEXT_DOMAIN)),
      'Do' => array('name' => __('Donnerstag', RUBIN_TEXT_DOMAIN)),
      'Fr' => array('name' => __('Freitag', RUBIN_TEXT_DOMAIN)),
      'Sa' => array('name' => __('Samstag', RUBIN_TEXT_DOMAIN)),
      'So' => array('name' => __('Sonntag', RUBIN_TEXT_DOMAIN)),
    );

    $ignore = $instance['ignore'];

    $byId = array();
    foreach($entities['objar'] as $group) {
      $cId = $group['grukat'];
      $gId = $group['gruid'];

      $limitCategory = $instance['limitCategory'];
      if (!emptyval($limitCategory) && $limitCategory !== $cId && $limitCategory !== RUBIN_TRAINING_SHOW && $limitCategory !== RUBIN_TRAINING_ASK) {
        continue;
      }

      $limitGroup = $instance['limitGroup'];
      if (!emptyval($limitGroup) && $limitGroup !== $cId.$gId && $limitGroup !== RUBIN_TRAINING_SHOW) {
        continue;
      }

      if ($group['inaktiv'] === '1') {
        continue;
      }

      $isO = $group['gzfld01'] === 'offen';
      $addO = $instance['includeOpen'];
      $addC = $instance['includeClosed'];
      if (($isO && !$addO) || (!$isO && !$addC)) {
        continue;
      }

      $cBez = $group['kbez'];
      $gBez = $group['grubez'];
      $gCol = $group['bgcol'];
      $gTrn = $group['trname'];

      foreach ($group['gruzar'] as $subgroup) {
        $oId = $subgroup['ortid'];
        $oBez = $subgroup['ortbez'];
        $gStart = $subgroup['startzeit'];
        $gEnde = $subgroup['endzeit'];
        $woTag = $this->unifiedWeekday($subgroup['wotag']);
        $turnus = $subgroup['turnus'];

        if ($this->doesMatchOne($ignore, [$gId, $gBez, $oId, $oBez])) {
          continue;
        }

        $byId[$cId] = $cBez;

        $day = $woTag;
        $exit = 0;
        $daily = $turnus && $turnus === '1';
        do {
          $uiGroup = array(
            'id' => $this->trainingGroupId(
              $group['grukat'],
              $group['gruid'],
              $subgroup['ortid'],
              $day,
              $subgroup['startzeit'],
              $subgroup['endzeit'],
              $group['lfdnr']
            ),
            'cId' => $cId,
            'cName' => $group['kbez'],
            'gId' => $gId,
            'gName' => $gBez,
            'oId' => $oId,
            'oName' => $oBez,
            'dId' => $day,
            'dName' => $WEEK[$day]['name'],
            'turnus' => $this->uiTrainingTurnus($turnus),
            'startzeit' => $gStart,
            'endzeit' => $gEnde,
            'zeitraum' => $this->uiTrainingTimeRange($gStart, $gEnde),
            'isFull' => isset($group['gzfld02']) ? substr_count($group['gzfld02'], $day) > 0 : false,
            'nextdat' => $this->getNextTrainingDays($group, $subgroup, $refusals, $instance['includeRefusals'], isset($instance['dateCount']) ? $instance['dateCount'] : 4, true),
            'description' => $group['grutxt'],
          );

          if (!is_array($WEEK[$day]['groups'])) {
            $WEEK[$day]['groups'] = array();
          }
          if (!is_array($WEEK[$day]['groups'][$cId])) {
            $WEEK[$day]['groups'][$cId] = array();
          }
          $WEEK[$day]['groups'][$cId][] = $uiGroup;

          $exit += 1;
          if ($daily) {
            $day = $this->getTrainingWeekdaySuccessor($day);
          }
        } while ($day !== $woTag && $exit < 10);
      }
    }

    return array(
      'byId' => $byId,
      'week' => $WEEK,
    );
  }

  function sanitizeTrainingGroupsForAdmin($entities=array(), $ignore='') {
    $categories = array();
    $groups = array();
    foreach($entities['objar'] as $entity) {
      $oId = $entity['ortid'];
      $oBez = '';
      $orte = $entity['gruzar'];
      foreach ($orte as $ort) {
        if ($ort['ortid'] == $oId) {
          $oBez = $ort['ortbez'];
          break;
        }
      }
      $cId = $entity['grukat'];
      $c = $entity['kbez'];
      $gId = $entity['gruid'];
      $g = $entity['grubez'];
      if (!$this->doesMatchOne($ignore, [$gId, $g, $oId, $oBez])) {
        if (!isset($categories[$cId])) {
          $categories[$cId] = $c;
        }
        if (!isset($groups[$gId])) {
          $groups[$cId.$gId] = $g;
        }
      }
    }
    asort($categories);
    asort($groups);
    return array(
      'categories' => $categories,
      'groups' => $groups,
    );
  }

  function sortEvents($events) {
    ksort($events, SORT_STRING);
    $sorted = array();
    foreach ($events as $key => $eventsOfDay) {
      usort($eventsOfDay, function($a,$b) {
        return strcmp($a['sort'], $b['sort']);
      });
      $sorted[$key] = $eventsOfDay;
    };
    return $sorted;
  }

  function cleanTrainingsFromHolidays($events, $instance) {
    $except = $instance['forceHolidays'];
    $cleanedEvents = array();
    foreach ($events as $day => $eventsOfDay) {
      $remainder = array();
      // first check if one of the days events is a holiday event
      foreach ($eventsOfDay as $event) {
        if ($event['type'] == 6) {
          $remainder[] = $event;
        }
      }
      if (sizeof($remainder) > 0) {
        // take over all other non training events
        foreach ($eventsOfDay as $event) {
          $gId = $event['gid'];
          $gBez = $event['name'];
          $type = $event['type'];

          $isHoliday = $type == 6;
          $isTraining = $type == 'TRAINING';
          if (!$isHoliday && !$isTraining) {
            $remainder[] = $event;
          }
          // test only group name and id - requirement does not work when testing location as well
          else if ($isTraining && $this->doesMatchOne($except, [$gId, $gBez])) {
            $remainder[] = $event;
          }
        }
        $cleanedEvents[$day] = $remainder;
      } else {
        $cleanedEvents[$day] = $eventsOfDay;
      }
    }
    return $cleanedEvents;
  }

  function convertEventsToListLayout($entities, $pageSize) {
    $rows = array();
    $today = date('Y-m-d', strtotime('now'));
    $page = array();
    foreach ($entities as $date => $events) {
      if ($date >= $today) {
        foreach ($events as $event) {
          if ($event['fromEvent']) {
            $event['name'] = $event['name'];
            $event['date'] = $date;
            $page[] = $event;
            if (sizeof($page) === $pageSize) {
              $rows[] = $page;
              $page = array();
            }
          }
        }
      }
    }
    if (sizeof($page) > 0) {
      $rows[] = $page;
    }
    return $rows;
  }

  function convertEventsToCompactLayout($firstDay, $lastDay, $allEvents) {
    $newEntries = array();
    $thisDay = $firstDay;
    while ($thisDay <= $lastDay) {
      // for day navigation
      $prevDay = strtotime('-1 day', $thisDay);
      $nextDay = strtotime('+1 day', $thisDay);
      // for week navigation
      $prevWeek = strtotime('-1 week', $thisDay);
      $nextWeek = strtotime('+1 week', $thisDay);
      // for month navigation
      $prevMonth = strtotime('-1 month', $thisDay);
      $nextMonth = strtotime('+1 month', $thisDay);
      // for year navigation
      $prevYear = strtotime('-1 year', $thisDay);
      $nextYear = strtotime('+1 year', $thisDay);

      // fill new entities
      $date = date('Y-m-d', $thisDay);
      $newEntry = array();
      $newEntry['prevDay'] = $prevDay < $firstDay ? null : date('Y-m-d', $prevDay);
      $newEntry['prevWeek'] = $prevWeek < $firstDay ? null : date('Y-m-d', $prevWeek);
      $newEntry['prevMonth'] = $prevMonth < $firstDay ? null : date('Y-m-d', $prevMonth);
      $newEntry['prevYear'] = $prevYear < $firstDay ? null : date('Y-m-d', $prevYear);
      $newEntry['thisDay'] = date('Y-m-d', $thisDay);
      $newEntry['thisWeek'] = date('W', $thisDay);
      $newEntry['thisWeekDay'] = date('N', $thisDay);
      $newEntry['thisMonth'] = date('m', $thisDay);
      $newEntry['thisFirstMonday'] = date('Y-m-d', strtotime('last monday', strtotime(date('Y-m', $thisDay).'-01')));
      $newEntry['nextDay'] = $nextDay > $lastDay ? null : date('Y-m-d', $nextDay);
      $newEntry['nextWeek'] = $nextWeek > $lastDay ? null : date('Y-m-d', $nextWeek);
      $newEntry['nextMonth'] = $nextMonth > $lastDay ? null : date('Y-m-d', $nextMonth);
      $newEntry['nextYear'] = $nextYear > $lastDay ? null : date('Y-m-d', $nextYear);
      $newEntry['uiDate'] = date('d.', $thisDay);
      if (is_array($allEvents[$date])) {
        $newEvents = array();
        foreach ($allEvents[$date] as $event) {
          $hasColor = array_key_exists('color', $event) && strlen(trim($event['color'])) > 0;
          $bgColor = $hasColor ? $event['color'] : 'transparent';
          $msmStart = (strtotime('1970-01-01 '.$event['startzeit']) - strtotime('1970-01-01')) / 60;
          $msmEnde = (strtotime('1970-01-01 '.$event['endzeit']) - strtotime('1970-01-01')) / 60;

          $event['bgColor'] = $bgColor;
          $event['duration'] = $msmEnde - $msmStart;
          $event['msm'] = $msmStart - (8 * 60); // display starts at 8:00;

          $newEvents[] = $event;
        }
        $newEntry['events'] = $newEvents;
      } else {
        $newEntry['events'] = null;
      }

      $newEntries[$date] = $newEntry;

      // iterate
      $thisDay = $nextDay;
    }
    return $newEntries;
  }

  function trainingGroupId($gKat='', $gId='', $oId='', $woTag='', $gStart='00:00:00', $gEnde='00:00:00', $lfdnr=0) {
    $id = ''
      .$gKat.'-'
      .$gId.'-'
      .$oId.'-'
      .$woTag.'-'
      .date('His', strtotime($gStart)).'-'
      .date('His', strtotime($gEnde)).'-'
      .$lfdnr
      .'';

    $id = preg_replace('/[^a-zA-Z0-9 -]/','_', $id);

    return $id;
  }

  function getNextTrainingDays($group, $subgroup, $refusals, $includeRefusals, int $limit, $startNow) {
    $woTag = $this->unifiedWeekday($subgroup['wotag']);
    $weekDay = $this->getTrainingWeekday($woTag);
    $now = strtotime('now');
    if (!$startNow) {
      $now = strtotime('- 18 months', $now);
    };

    $lastDay = emptyval($group['kursbis']) ? strtotime('+ 36 months', $now) : strtotime($group['kursbis']);
    if ($lastDay < $now) {
      return array();
    }

    $firstDay = emptyval($group['kursvon']) ? $now : strtotime($group['kursvon']);
    if (date('w', $firstDay) !== $weekDay) {
      $firstDay = strtotime('next '.$this->getTrainingWeekdayPhrase($woTag), $firstDay);
    }

    $gid = $group['gruid'];
    $oid = $subgroup['ortid'];
    $tym = $subgroup['startzeit'];
    $days = array();
    $added = 0;
    $day = $firstDay;
    while ($day <= $lastDay && $added < $limit) {
      $key = date('Y-m-d', $day);

      $isRefused = is_array($refusals[$gid.$oid.$key.$tym]);
      if ($includeRefusals || !$isRefused) {
        $days[] = array(
          'date' => $key,
          'uiDate' => date('d.m.Y', strtotime($key)),
          'refused' => $isRefused,
        );
        $added += ($isRefused ? 0 : 1);
      }
      $day = strtotime($this->getTrainingTurnusPhrase($subgroup['turnus']), $day);
    }

    return $days;
  }

  function doesMatchOne($haystack, $needles) {
    $haystack = is_string($haystack) ? trim($haystack) : '';
    $haystack = preg_replace('/,[ ]+/', ',', $haystack);
    $haystack = preg_replace('/[ ]+,/', ',', $haystack);
    $haystack = strlen($haystack) > 0 ? '^('.preg_replace('/,/', '|', $haystack).')' : '';

    $match = false;
    foreach ($needles as $needle) {
      $match = $match || preg_match("/$haystack/i", trim($needle)) === 1;
    }

    return strlen($haystack) > 0 ? $match : false;
  }

  function unifiedWeekday($day) {
    switch ($day) {
      case 'Mo': case '1': case 1: return 'Mo';
      case 'Di': case '2': case 2: return 'Di';
      case 'Mi': case '3': case 3: return 'Mi';
      case 'Do': case '4': case 4: return 'Do';
      case 'Fr': case '5': case 5: return 'Fr';
      case 'Sa': case '6': case 6: return 'Sa';
      case 'So': case '7': case 7: return 'So';
    }
  }

  function getTrainingWeekday($day) {
    switch ($day) {
      case 'Mo': return 1;
      case 'Di': return 2;
      case 'Mi': return 3;
      case 'Do': return 4;
      case 'Fr': return 5;
      case 'Sa': return 6;
      case 'So': return 0;
    }
  }

  function getTrainingWeekdaySuccessor($day) {
    switch ($day) {
      case 'Mo': return 'Di';
      case 'Di': return 'Mi';
      case 'Mi': return 'Do';
      case 'Do': return 'Fr';
      case 'Fr': return 'Sa';
      case 'Sa': return 'So';
      case 'So': return 'Mo';
    }
  }

  function getTrainingWeekdayPhrase($day) {
    switch ($day) {
      case 'Mo': return 'Monday';
      case 'Di': return 'Tuesday';
      case 'Mi': return 'Wednesday';
      case 'Do': return 'Thursday';
      case 'Fr': return 'Friday';
      case 'Sa': return 'Saturday';
      case 'So': return 'Sunday';
    }
  }

  function getTrainingTurnusPhrase($turnus) {
    switch ($turnus) {
      case 1: return '+ 1 day';
      case 2: return '+ 1 week';
      case 3: return '+ 2 weeks';
      case 4: return '+ 1 month';
    }
  }

  function mergeEvents($all, $events) {
    foreach ($events as $date => $eventsByDay) {
      if (!array_key_exists($date, $all)) {
        $all[$date] = array();
      }
      foreach ($eventsByDay as $key => $event) {
        $all[$date][$key] = $event;
      }
    }
    return $all;
  }

  function uiEventTimeRange($start, $end) {
    $von = __('von ', RUBIN_TEXT_DOMAIN);
    $bis = __(' bis ', RUBIN_TEXT_DOMAIN);
    $ab = __('ab ', RUBIN_TEXT_DOMAIN);
    $endet = __('endet ', RUBIN_TEXT_DOMAIN);
    $uhr = __(' Uhr', RUBIN_TEXT_DOMAIN);

    $hasStart = !emptyval($start);
    $hasEnd = !emptyval($end);
    if ($hasStart && !$hasEnd) {
      return $ab.date('H:i', strtotime($start)).$uhr;
    } else if (!$hasStart && $hasEnd) {
      return $endet.date('H:i', strtotime($start)).$uhr;
    } else if ($hasStart && $hasEnd) {
      return $von.date('H:i', strtotime($start)).$bis.date('H:i', strtotime($end)).$uhr;
    } else {
      return false;
    }
  }

  function uiEventDateRange($start, $end) {
    $WOCHENTAG = array(
      __('Montag', RUBIN_TEXT_DOMAIN),
      __('Dienstag', RUBIN_TEXT_DOMAIN),
      __('Mittwoch', RUBIN_TEXT_DOMAIN),
      __('Donnerstag', RUBIN_TEXT_DOMAIN),
      __('Freitag', RUBIN_TEXT_DOMAIN),
      __('Samstag', RUBIN_TEXT_DOMAIN),
      __('Sonntag', RUBIN_TEXT_DOMAIN),
    );

    $sdt = strtotime($start);
    $edt = strtotime($end);
    $bis = __(' bis ', RUBIN_TEXT_DOMAIN);

    if (date('Ymd', $sdt) === date('Ymd', $edt)) {
      $wotag = date('N', $sdt) - 1;
      return $WOCHENTAG[$wotag].', '.date('j.n.Y', $sdt);
    } else if (date('Ym', $sdt) === date('Ym', $edt)) {
      return date('j.', $sdt).$bis.date('j.n.Y', $sdt);
    } else if (date('Y', $sdt) === date('Y', $edt)) {
      return date('j.n.', $sdt).$bis.date('j.n.Y', $sdt);
    } else {
      return date('j.n.Y', $sdt).$bis.date('j.n.Y', $sdt);
    }
  }

  function uiTrainingTimeRange($start, $end) {
    $von = __('von ', RUBIN_TEXT_DOMAIN);
    $bis = __(' bis ', RUBIN_TEXT_DOMAIN);
    $ab = __('ab ', RUBIN_TEXT_DOMAIN);
    $endet = __('endet ', RUBIN_TEXT_DOMAIN);
    $uhr = __(' Uhr', RUBIN_TEXT_DOMAIN);

    $hasStart = !emptyval($start);
    $hasEnd = !emptyval($end);
    if ($hasStart && !$hasEnd) {
      return $ab.date('H:i', strtotime($start)).$uhr;
    } else if (!$hasStart && $hasEnd) {
      return $endet.date('H:i', strtotime($start)).$uhr;
    } else if ($hasStart && $hasEnd) {
      return $von.date('H:i', strtotime($start)).$bis.date('H:i', strtotime($end)).$uhr;
    } else {
      return false;
    }
  }

  function uiTrainingTurnus($entity) {
    switch ($entity) {
      case '1': return __('täglich', RUBIN_TEXT_DOMAIN);
      case '2': return __('wöchentlich', RUBIN_TEXT_DOMAIN);
      case '3': return __('14-tägig', RUBIN_TEXT_DOMAIN);
      case '4': return __('monatlich', RUBIN_TEXT_DOMAIN);
    }
  }

}