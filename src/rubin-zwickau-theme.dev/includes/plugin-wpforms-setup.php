<?php
defined(ALLOW_RUBIN_THEME) or die();

/**
 * Show values in Dropdown, checkboxes, and Multiple Choice.
 * @link https://wpforms.com/developers/add-field-values-for-dropdown-checkboxes-and-multiple-choice-fields/
 */
add_action( 'wpforms_fields_show_options_setting', '__return_true' );
