<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_enqueue_styles() {
  global $CSS_RUBIN, $CSS_TAILWIND;
  wp_enqueue_style('rubinStyle', get_stylesheet_uri());
  wp_enqueue_style('rubinTailwind', get_template_directory_uri().'/'.$CSS_TAILWIND, array('rubinStyle'));
  wp_enqueue_style('rubinRubin', get_template_directory_uri().'/'.$CSS_RUBIN, array('rubinTailwind'));
}
add_action( 'wp_enqueue_scripts', 'rubin_enqueue_styles' );

function rubin_enqueue_admin_styles() {
  global $CSS_RUBIN, $CSS_TAILWIND;
  wp_enqueue_style('rubinTailwind', get_template_directory_uri().'/'.$CSS_TAILWIND);
}
add_action('admin_enqueue_scripts', 'rubin_enqueue_admin_styles');