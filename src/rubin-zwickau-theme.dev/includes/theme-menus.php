<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_register_menus() {
  register_nav_menus(array(
    'primary' => __('Hauptmenü', RUBIN_TEXT_DOMAIN),
    'footer' => __('Footermenü', RUBIN_TEXT_DOMAIN)
  ));
}
add_action( 'init', 'rubin_register_menus' );
