<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_svg($filePath, $attributes=array()) {
  $marker = '<svg ';
  $svg = file_get_contents($filePath);
  $svgPos = stripos($svg, $marker);
  if ($svgPos === false) {
    echo '<!-- '.$filePath.' -->';
    echo '<!-- SVG does not contain svg tag - cant apply attributes -->';
    echo $svg;
    echo '';
  } else {
    $before = substr($svg, 0, $svgPos + strlen($marker) + 1);
    $after = substr($svg, $svgPos + strlen($marker));
    echo '<!-- '.$filePath.' -->';
    echo $before;
    foreach ($attributes as $key => $value) {
      echo ' '.str_replace('=', '-', $key).'="'.$value.'"';
    }
    echo ' '.$after;
    echo '';
  }
}
