<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_register_customizer_mgvo_api_section($wp_customize) {
  $wp_customize->add_section('mgvo-api-section', array(
    'title' => __('MGVO: API', RUBIN_TEXT_DOMAIN),
    'priority' => 10,
    'description' => __('MGVO: Zugriff auf die API der Vereinsverwaltung', RUBIN_TEXT_DOMAIN) 
  ));

  $wp_customize->add_setting('mgvo-userid-setting', array(
    'default' => '',
    'sanitize_callback' => 'sanitize_key'
  ));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'mgvo-userid-control', array(
    'label' => __('Call-ID/User-ID?', RUBIN_TEXT_DOMAIN),
    'section' => 'mgvo-api-section',
    'settings' => 'mgvo-userid-setting',
    'type' => 'text'
  )));

  $wp_customize->add_setting('mgvo-secret-setting', array(
    'default' => '',
    'sanitize_callback' => 'sanitize_key'
  ));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'mgvo-secret-control', array(
    'label' => __('API-Key/Passwort?', RUBIN_TEXT_DOMAIN),
    'section' => 'mgvo-api-section',
    'settings' => 'mgvo-secret-setting',
    'type' => 'password'
  )));

  $wp_customize->add_setting('mgvo-cache-setting', array(
    'default' => '5',
    'sanitize_callback' => 'rubin_sanitize_mgvo_cache_setting'
  ));
  $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'mgvo-cache-control', array(
    'label' => __('Aufbewahrungszeit (in Minuten) für Abfragedaten?', RUBIN_TEXT_DOMAIN),
    'section' => 'mgvo-api-section',
    'settings' => 'mgvo-cache-setting',
    'type' => 'text',
    'input_attrs' => array(
			'placeholder' => __( 'in ganzen Minuten' )
		)
  )));
}
add_action('customize_register', 'rubin_register_customizer_mgvo_api_section');

function rubin_sanitize_mgvo_cache_setting($value, $setting) {
  if (!preg_match("/^[1-9]+[0-9]*$/", $value)) {
    return new WP_Error('invalid_value', __('Bitte die Minuten als ganze Zahl eingeben!'));
  }

  $intValue = intval($value);
  $defValue = intval($setting->default);
  if ($intValue < $defValue) {
    return new WP_Error('invalid_value', __('Diese Zeit ist zu kurz!'));
  }

  return $value;
}
