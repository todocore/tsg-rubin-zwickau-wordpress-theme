<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_register_backend_default4js() {
  global $mgvoService;

  $defaultIgnore = get_theme_mod('mgvo-ignore-setting');
  $defaultIgnore = isset($defaultIgnore) ? $defaultIgnore : '';
    
  $data = $mgvoService->getMGVO()->read_gruppen();
  $data = $mgvoService->sanitizeTrainingGroupsForAdmin($data, $defaultIgnore);

  echo '<script type="text/javascript">';
  echo 'window.rubinThemeDefaults = {';
  echo '  ignore: \''.$defaultIgnore.'\',';
  echo '  categories: {';
  foreach ($data['categories'] as $k => $v) {
    if (isset($k) && $k !== '') {
      echo $k.': \''.$v.'\',';
    }
  }
  echo '  },';
  echo '}';
  echo '</script>';
  echo '\n';
}
add_action('admin_head', 'rubin_register_backend_default4js');