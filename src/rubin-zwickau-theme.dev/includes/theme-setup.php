<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_theme_slug_setup() {
   global $CSS_RUBIN, $CSS_TAILWIND;
   add_theme_support('align-wide');
   add_theme_support('editor-color-palette');
   add_theme_support('editor-font-sizes');
   add_theme_support('editor-gradient-presets');
   add_theme_support('html5');
   add_theme_support('post-thumbnails', array('post'));
   add_theme_support('responsive-embeds');
   add_theme_support('title-tag');
   add_theme_support('wp-block-styles');
   // remove_theme_support('widgets-block-editor');
}
add_action('after_setup_theme', 'rubin_theme_slug_setup');
