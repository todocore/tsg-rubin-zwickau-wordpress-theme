<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_enqueue_scripts() {
  if (get_theme_mod('development-vue-devmode-setting') === true) {
    wp_enqueue_script('vuejs-dev', get_template_directory_uri().'/assets/js/vue-2.6.14.js', array('jquery'), '2.6.14');
  } else {
    wp_enqueue_script('vuejs', get_template_directory_uri().'/assets/js/vue-2.6.14.min.js', array('jquery'), '2.6.14');
  }
}
add_action( 'wp_enqueue_scripts', 'rubin_enqueue_scripts' );

function rubin_enqueue_admin_scripts() {
  if (get_theme_mod('development-vue-devmode-setting') === true) {
    wp_enqueue_script('vuejs-dev', get_template_directory_uri().'/assets/js/vue-2.6.14.js', array('jquery'), '2.6.14');
  } else {
    wp_enqueue_script('vuejs', get_template_directory_uri().'/assets/js/vue-2.6.14.min.js', array('jquery'), '2.6.14');
  }
}
add_action('admin_enqueue_scripts', 'rubin_enqueue_admin_scripts');