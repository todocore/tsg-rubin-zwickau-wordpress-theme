<?php

   // In der Datei wird eine Sniplet-Klasse definiert, welche die Methoden der Klasse MGVO_HPAPI (ext_mod_hp.php) 
   // aufruft und zu den jeweiligen Daten HTML-Code zur Ausgabe der Daten generiert.
   // Die Klasse repr�sentiert Beispielcode und muss den individuellen Anforderungen angepasst werden.

   require_once(dirname(__FILE__)."/mgvo_hpapi.php");
   
   class MGVO_SNIPLET {
      public $api;
      private $headline;
      
      function __construct($call_id,$vcryptkey="",$cachemin=5) {
         // call_id: call_id des Vereins
         // vcryptkey: Schl�ssel f�r die synchrone Verschl�sselung. Wird in MGVO in den technischen Parametern eingetragen
         // cachemin: Anzahl Minuten, die ein Ergebnis von MGVO in einem Dateicache zwischengespeichert wird. Wenn zu viele Ausrufe
         //           MGVO erreichen, wird aus Sicherheitsgr�nden kein Ergebnis mehr geliefert
         $this->api = new MGVO_HPAPI($call_id,$vcryptkey,$cachemin);
		 $this->api->set_debuglevel(0);
      }
      
      function set_debuglevel($debuglevel=0) {
         $this->api->set_debuglevel($debuglevel);
         
      }
      
      function set_headline($headline) {
         $this->headline = $headline;
      }
      
      function write_headline($mgvo_headline="") {
         $headline = empty($this->headline) ? $mgvo_headline : $this->headline;
         $sniplet = "<h2>$headline</h2>";
         return $sniplet;
      }
      
      function mgvo_sniplet_vkal($vkalnr,$seljahr) {
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
         $resar = $this->api->read_vkal($vkalnr,$seljahr);
      
         $sniplet = "<div class='mgvo mgvo-vkal'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table class='mgvo-table-b1p1s0'>";
         $sniplet .= "<tr>";
         $sniplet .= "<th>Bezeichnung</th>";
         $sniplet .= "<th>Startdatum</th>";
         $sniplet .= "<th>Startzeit</th>";
         $sniplet .= "<th>Enddatum</th>";
         $sniplet .= "<th>Endzeit</th>";
         $sniplet .= "<th>Ort</th>";
         $sniplet .= "</tr>";
         foreach($resar['objar'] as $idx => $vkr) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>$vkr[bez]</td>";
            $sniplet .= "<td>".date2user($vkr['startdat'],1)."</td>";
            $sniplet .= "<td>$vkr[startzeit]</td>";
            $sniplet .= "<td>".date2user($vkr['enddat'],1)."</td>";
            $sniplet .= "<td>$vkr[endzeit]</td>";
            $sniplet .= "<td>$vkr[ort]</td>";
            $sniplet .= "</tr>";
         }
         $sniplet .= "</table>";
         $sniplet .= "</div>";
         return $sniplet;
      }
      
      function mgvo_sniplet_orte() {
         // Liest die Ortsliste ein
         $resar = $this->api->read_orte();
       
         $sniplet = "<div class='mgvo mgvo-orte'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table class='mgvo-orte'>";
         $sniplet .= "<tr>";
         $sniplet .= "<th>Orts-ID</th>";
         $sniplet .= "<th>Ortsbezeichnung</th>";
         $sniplet .= "</tr>";
         foreach($resar['objar'] as $or) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>$or[ortid]</td>";
            $sniplet .= "<td>$or[ortbez]</td>";
            $sniplet .= "</tr>";
         }
         $sniplet .= "</table><br>";
         $sniplet .= "</div>";
         return $sniplet;
      }
            
      function mgvo_sniplet_betreuer() {
         // Liest die Betreuer ein
         $resar = $this->api->read_betreuer();
         
         $sniplet = "<div class='mgvo mgvo-betreuer'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table class='mgvo-betreuer'>";
         $sniplet .= "<tr>";
         $sniplet .= "<th>Trainer-ID</th>";
         $sniplet .= "<th>Name</th>";
         $sniplet .= "<th>Stra&szlige</th>";
         $sniplet .= "</tr>";
         foreach($resar['objar'] as $or) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>$or[trid]</td>";
            $sniplet .= "<td>$or[nachname], $or[vorname]</td>";
            $sniplet .= "<td>$or[str]</td>";
            $sniplet .= "</tr>";
         }
         $sniplet .= "</table><br>";
         $sniplet .= "</div>";
         return $sniplet;
      }
      
      function mgvo_sniplet_events() {
         // Liest die �ffentlichen Veranstaltungen
         $resar = $this->api->read_events();
       
         $sniplet = "<div class='mgvo mgvo-events'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table class='mgvo-events'>";
         $sniplet .= "<tr>";
         $sniplet .= "<th>Event</th>";
         $sniplet .= "<th>Beschreibung</th>";
         $sniplet .= "<th>Ort</th>";
         $sniplet .= "<th>Datum</th>";
         $sniplet .= "<th>Zeit</th>";
         $sniplet .= "<th>Bestell-URL</th>";
         $sniplet .= "</tr>";
         foreach($resar['objar'] as $or) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>$or[name]</td>";
            $sniplet .= "<td>$or[description]</td>";
            $sniplet .= "<td>$or[ort]</td>";
            $sniplet .= "<td>".date2user($or['startdate'],1)."</td>";
            $sniplet .= "<td>$or[starttime]</td>";
            if (!empty($or['besturl'])) $sniplet .= "<td><a href='$or[besturl]' target=_blank>Bestell-URL</a></td>";
            else $sniplet .= "<td></td>";
            $sniplet .= "</tr>";
         }
         $sniplet .= "</table><br>";
         $sniplet .= "</div>";
         return $sniplet;
 
         return $sniplet;
      }
      
      function mgvo_sniplet_gruppen() { 
         $resar = $this->api->read_gruppen();
       
         $sniplet = "<div class='mgvo mgvo-gruppen'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table class='mgvo-gruppen'>";
         $sniplet .= "<tr>";
         $sniplet .= "<th>Gruppen-ID</th>";
         $sniplet .= "<th>Name</th>";
         $sniplet .= "<th>Betreuer</th>";
         $sniplet .= "</tr>";
         foreach($resar['objar'] as $or) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>".$or['gruid']."</td>";
            $sniplet .= "<td>".$or['grubez']."</td>";
            $sniplet .= "<td>".$or['trnameall']."</td>";
            $sniplet .= "</tr>";
         }
         $sniplet .= "</table><br>";
         $sniplet .= "</div>";
 
         return $sniplet;
      }
      
      function mgvo_sniplet_abteilungen() {
         $resar = $this->api->read_abt();
       
         $sniplet = "<div class='mgvo mgvo-abteilungen'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table class='mgvo-abteilungen'>";
         $sniplet .= "<tr>";
         $sniplet .= "<th>Abteilungs-ID</th>";
         $sniplet .= "<th>Name</th>";
         $sniplet .= "</tr>";
         foreach($resar['objar'] as $or) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>$or[abtid]</td>";
            $sniplet .= "<td>$or[abtbez]</td>";
            $sniplet .= "</tr>";
         }
         $sniplet .= "</table><br>";
         $sniplet .= "</div>";
 
         return $sniplet;
      }
      
      function mgvo_sniplet_training_fail() {
         $resar = $this->api->read_training_fail();

         $sniplet = "<div class='mgvo mgvo-trainingfail'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table class='mgvo-trainingsausfall'>";
         $sniplet .= "<tr>";
         $sniplet .= "<th>Gruppe / Belegung</th>";
         $sniplet .= "<th>Datum</th>";
         $sniplet .= "<th>Zeit</th>";
         $sniplet .= "<th>Ort</th>";
         $sniplet .= "<th colspan=3>neu</th>";
         $sniplet .= "<th>Grund / Veranstaltung</th>";
         $sniplet .= "</tr>";
         foreach($resar['objar'] as $tfr) {
            $sniplet .= "<tr>";
            if (!empty($tfr['grbez'])) $sniplet .= "<td>".$tfr['grbez']." (".$tfr['gruid'].")</td>";
            else $sniplet .= "<td>".$tfr['belbez']."</td>";
            $sniplet .= "<td>".date2user($tfr['sdat'],1)."</td>";
            $sniplet .= "<td>".time2user($tfr['starttime'])." - ".time2user($tfr['endtime'])."</td>";
            $sniplet .= "<td>".$tfr['ortsbez']."</td>";
            if (!emptyval($tfr['neudat'])) $sniplet .= "<td>".date2user($tfr['neudat'],1)."</td>";
            else $sniplet .= "<td></td>";
			
            $sniplet .= "<td>".$tfr['neuzeithtml']."</td>";
			
            $sniplet .= "<td>".$tfr['neuorthtml']."</td>";
            $sniplet .= "<td>".$tfr[ebez]."</td>";
            $sniplet .= "</tr>";
         }
         $sniplet .= "</table><br>";
         $sniplet .= "</div>";
         return $sniplet;
      }
      
      function mgvo_sniplet_read_mitglieder($selparar=NULL) {
         // Selektion von Mitgliedern. 
         $resar = $this->api->read_mitglieder($selparar);
         $sniplet = "<div class='mgvo mgvo-mitglieder'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table class='mgvo-mitglieder'>";
         $sniplet .= "<tr>";
         $sniplet .= "<th>MgNr.</th>";
         $sniplet .= "<th>Nachname</th>";
         $sniplet .= "<th>Vorname</th>";
         $sniplet .= "<th>Stra&szlig;e</th>";
         $sniplet .= "<th>PLZ</th>";
         $sniplet .= "<th>Ort</th>";
         $sniplet .= "<th>Eintritt</th>";
         $sniplet .= "<th>Austritt</th>";
         $sniplet .= "</tr>";
         foreach($resar['objar'] as $mr) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>$mr[mgnr]</td>";
            $sniplet .= "<td>$mr[nachname]</td>";
            $sniplet .= "<td>$mr[vorname]</td>";
            $sniplet .= "<td>$mr[str]</td>";
            $sniplet .= "<td>$mr[plz]</td>";
            $sniplet .= "<td>$mr[ort]</td>";
            $sniplet .= "<td>".date2user($mr['eintritt'],1)."</td>";
            $sniplet .= "<td>".date2user($mr['austritt'],1)."</td>";
            $sniplet .= "</tr>";
         }
         $sniplet .= "</table><br>";
         $sniplet .= "</div>";
         return $sniplet;
      }
      
      function mgvo_sniplet_show_mitglied($mgnr) {
         $mr = $this->api->show_mitglied($mgnr);
         $sniplet = "<div class='mgvo mgvo-mitglieder'>";
         $sniplet .= $this->write_headline("Anzeige Mitglied");
         $sniplet .= "<table class='mgvo-mitglied'>";
         foreach($mr as $fieldname => $value) {
            $sniplet .= "<tr><td>$fieldname:</td><td>$value</td></th>";
         }
         $sniplet .= "</table>";
         $sniplet .= "</div>";
         return $sniplet;
      }
      
      function mgvo_sniplet_list_documents($dokart=NULL) {
         $resar = $this->api->list_documents($dokart);
         $sniplet = "<div class='mgvo mgvo-documents'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table class='mgvo-dokulist'>";
         $sniplet .= "<tr>";
         $sniplet .= "<th>DokNr.</th>";
         $sniplet .= "<th>Dokart</th>";
         $sniplet .= "<th>Name</th>";
         $sniplet .= "<th>Gr&ouml;&szlig;e</th>";
         $sniplet .= "</tr>";
         foreach($resar['objar'] as $dokr) {
            $sniplet .= "<tr>";
            $sniplet .= "<td>".$dokr['doknr']."</td>";
            $sniplet .= "<td>${dokr['dokart']}</td>";
            $sniplet .= "<td><a href='".$dokr[url_display]."' target=_blank>{$dokr[dokbez]}</a></td>";
            $sniplet .= "<td>".$dokr['fsize']."</td>";
            $sniplet .= "</tr>";
         }
         $sniplet .= "</table><br>";
         $sniplet .= "</div>";
         return $sniplet;
      }
      
      function mgvo_sniplet_mitpict($mgnr) {
         $resar = $this->api->get_mitpict($mgnr);
      
         $mpr = $resar['objar'][0];
         $dokname = $mpr['dokname'];
         $fsize = $mpr['fsize'];
         $ctype = $mpr['mimetype'];
         $content = base64_decode($mpr['content']);
         
         header("Content-Type: $ctype");
         header("Content-Length: " . strlen($content));
         header("Content-disposition: inline; filename=\"$dokname\"");
         
         echo $content;
      }
	  function mgvo_sniplet_selbstauskunft_iframe() {
		 if (!check_callid($this->call__id)) return "Keine MGVO Call_ID angegeben, bitte unter Settings eintragen";
		 $sniplet ="<div><iframe src='https://www.mgvo.de/prog/pub_ssrequest.php?call_id=".$this->call__id."' width='500' height='400px' frameborder='0'>Selbstauskunft kann nicht eingebunden werden</iframe></div><br>";
		 return $sniplet;
	  }	
	  											  
      function mgvo_sniplet_mitgliedsantrag_link() {
		 if (!check_callid($this->call__id))  return "Keine MGVO CallID angegeben, bitte unter Settings eintragen";
		 $sniplet = "<a href='https://www.mgvo.de/prog/pub_mitantrag.php?call_id=".$this->call__id."' target='_blank' rel='noopener noreferrer'>Mitgliedschaft online beantragen</a>";
		 return $sniplet;
	  }
	  
	  function mgvo_sniplet_mitgliederbereich_link() {
		 if (!check_callid($this->call__id)) return "Keine MGVO CallID angegeben, bitte unter Settings eintragen";
		 $sniplet = "<a href='https://www.mgvo.de/mgb/login/login.php?call_id=".$this->call__id."' target='_blank' rel='noopener noreferrer'>Login Mitgliederbereich</a>";
		 return $sniplet;
	  }
	  function mgvo_sniplet_kartenbuchung_iframe($eventnr, $target = "iframe") {
		 // die Funktion zeigt die kartenbuchung an. Entwerder als iframe oder als link mit angegebenen Target (i.d.R. _blank)
		 if (!check_callid($this->call__id)) return "Keine MGVO Call_ID angegeben, bitte unter Settings eintragen";
		 if ($target == "iframe") {
			$sniplet ="<div><iframe src='https://www.mgvo.de/prog/pub_bookticket1.php?eventnr=".$eventnr."&call_id=".$this->call__id."' width='800' height='600px' frameborder='0'>Selbstauskunft kann nicht eingebunden werden</iframe></div><br>";
		 } else {
			 $sniplet = "<a href='https://www.mgvo.de/prog/pub_bookticket1.php?eventnr=".$eventnr."&call_id=".$this->call__id."' target='".$target."' rel='noopener noreferrer'>Zur Kartenbestellung</a>";
		 }
		 return $sniplet;
	  }	
	  
	  
	  function mgvo_sniplet_filter_events($resar, $startdate = "now", $enddate="", $number = 0 ){
		
		  if ($startdate != "" ){ $start = new DateTime($startdate);}
		  if ($enddate != "" ) {$end = new DateTime($enddate);}
		  $count = 0;
		
		  $resar2 = array();
		  //$resar = $this->api->read_training_fail();
	  	  foreach($resar as $key => $resar_element ) {
		  	 if ($key != 'objar') {
				$resar2[$key] = $resar_element;
			 }
		  }
			
          foreach($resar['objar'] as $key2 => $element) {
			if ($startdate != ""  and strtotime($element['sdat'])< $start) {continue;}
			if ($enddate != ""  and strtotime($element['sdat'])< $end) {continue;}
			if ($count > $number)   {continue;}
			$resar2['objar'][$key2] = $element;
			return $resar2;
		  }
	  }

 }																					 
   
   
   
   
   
   class MGVO_GEN_SNIPLET extends MGVO_SNIPLET {
 
 // *************************
	  // Generic Sniplets - DEPRICATED
	  //
	  // ANMERKUNG: Diese Klasse haben sich als zu unhandlich erwiesen, es wird empfohlen, diese f�r neue Projekte nicht mehr zu verwenden. 
	  //
	  //
	  //  Mit diesen Funktion lassen sich generisch beliebige HTML-Tabellen aus MGVO erstellen. 
	  //  Mit den Eintr�gen $vkal_use_fields_table, $vkal_head_fields_tablen und $vkal_sanitize_fields_table  wird gesteuert, welche Felder ausgegeben werden
      //  Die verf�gbaren Felder k�nnen dem XML (<objfieldlist>) entnommen werden und sind auch im Array der API verf�gbar)
      //  $vkal_use_fields_table =  zu verwendendene Felder  
      //  $vkal_head_fields_table = dazugeh�rige �berschriften (gleiche Anzahl wie Felder erforderlich)
      //  $vkal_sanitize_fields_table = Felder, die eine Datumsbehandlung ben�tigen
	  //  $vkal_sort gibt den sortfeldnamen vor. �bergabe als String (mit Komma) oder als Array
	  //  $vkal_filter gibt Filterkritieren vor. �bergabe als Feld=String mit Komma ("ortid=saal1,startzeit=20:00:00") oder als Array (array(array('field'=>'ortid','value'=>'saal1'),array('field'=>'startzeit','value'=>'20:00:00'), 
	  //  Nur im Array werten Leer- und Sonderzeichen unterst�tzt. Aktuell wir nur das "=" unterst�tzt. (kine "<" oder "!=")
	  //  $vkal_rewrite_fildes Hiermit k�nnen Felder in andere Felder geschrieben werden (wenn diese leer sind) z.B. resstarttime in startzeit f�r Ortsreservierungen
	  //  Im MGVO Kalender werden, je nach Eintragsursprng eine Vielzahl unterschiedlicher Felder gef�llt, daher ist dies in manchen F�llen notwendig.
	  //  $max_count - Die maxminale Anzahl von Eintr�gen je Seite
	  //  $page - Die Seite (nur in Verbindung mit $maxcount)
	  //  Die Spalten haben jeweils eine CSS-Klasse mgvo-f-<feldname>, z.B. mgvo-f-bez, somit lassen sich im CSS die Spaltenbreiten individuell angeben.
	  //  Wenn im Namen "NOWEB" steht, wird der Eintrag niemals ausgegeben
	  //  Wenn im Namen EXTERN: steht, wird nur der Teil nach EXTERN: ausgegeben. Praktisch um z.B. Extern die Details auszulassen "Saal vermietet an Hans Schmidt EXTERN:Private Veranstaltung"
	  //
	  // *************************


	  function mgvo_gen_sniplet_gruppen ( $vkal_use_fields_table = Null, $vkal_head_fields_table = Null, $vkal_sort = NULL, $vkal_filter = NULL, $vkal_rewrite_fields = NULL, $max_count = NULL, $page= NULL) {
         // Liest die Gruppen ein und gibt sie aus
         //  Verf�gbare Felder: 
		 // Konfig-Anleitung siehe oben
		 if (empty($vkal_use_fields_table) or empty($vkal_head_fields_table)) {
			// Alle Felder mit technischem Namen
            // $vkal_use_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
            // $vkal_head_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
			// Default:
			$vkal_use_fields_table = explode(",","grubez,grutxt,startzeit,endzeit,turnus,ortid,tridall");
			$vkal_head_fields_table = explode(",","Gruppenname,Beschreibung,Start,Ende,Turnus,Ort,Trainer");
        }
        if (count($vkal_use_fields_table) != count($vkal_head_fields_table)) {
            $sniplet .= "Anzahl der Felder und �berschriften in mgvo_sniplet_vkal() nicht gleich";
			return $sniplet;
        }      
        $vkal_sanitize_fields_table  = explode(",","startdat,startzeit,enddat,endzeit");
 
        // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
        $resar = $this->api->read_gruppen($vkalnr,$seljahr);
		return $this->mgvo_generic_sniplet($resar , "mgvo-vkal" ,$vkal_use_fields_table, $vkal_head_fields_table, $vkal_sanitize_fields_table, $vkal_sort, $vkal_filter, $max_count, $page);
	  }	
      
      /*unction mgvo_gen_sniplet_gruppen_bgc ( $vkal_use_fields_table = Null, $vkal_head_fields_table = Null, $vkal_sort = NULL, $vkal_filter = NULL, $vkal_rewrite_fields = NULL, $max_count = NULL, $page= NULL) {
         // Liest die Gruppen ein und gibt sie aus
         //  Verf�gbare Felder: 
		 // Konfig-Anleitung siehe oben
		 if (empty($vkal_use_fields_table) or empty($vkal_head_fields_table)) {
			// Alle Felder mit technischem Namen
            // $vkal_use_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
            // $vkal_head_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
			// Default:
			$vkal_use_fields_table = explode(",","grubez,grutxt,startzeit,endzeit,ortid,tridall");
			$vkal_head_fields_table = explode(",","Gruppenname,Beschreibung,Start,Ende,Ort,Trainer");
            //$vkal_filter= "gzfld03=Herbst2021";
        }
        if (count($vkal_use_fields_table) != count($vkal_head_fields_table)) {
            $sniplet .= "Anzahl der Felder und �berschriften in mgvo_sniplet_vkal() nicht gleich";
			return $sniplet;
        }      
        $vkal_sanitize_fields_table  = explode(",","startdat,startzeit,enddat,endzeit");

        // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
        $resar = $this->api->read_gruppen($vkalnr,$seljahr);
		return $this->mgvo_generic_sniplet($resar , "mgvo-vkal" ,$vkal_use_fields_table, $vkal_head_fields_table, $vkal_sanitize_fields_table, $vkal_rewrite_fields, $vkal_sort, $vkal_filter, $max_count, $page);
	  }	*/ 
      
	  
	  function mgvo_gen_sniplet_vkal($vkalnr,$seljahr, $vkal_use_fields_table = Null, $vkal_head_fields_table = Null, $vkal_sort = NULL, $vkal_filter = NULL, $vkal_rewrite_fields = NULL, $max_count = NULL, $page= NULL) {
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr als HTML. 
         // Verf�gbare Felder: startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb
		 // ortid,eventnr,ressdat,resstime,resedat,resetime,ebez,eltxt,eort,startdat,startzeit,enddat,endzeit,email_org,betr01,betrbez01,ticketbis,saalplan,publish,pub_helfer,resmaildat,helfermaildat,abrechdat,pubdate,url_eventinfo,pm_bar,teilnehmerflag,emtickflag,ort,ortkb,bez,prio,stdabrech,bgcol,ticketmail,verkaufsintro,vorldat,wartelisteaktiv,notiz,betr02,betrbez02,porto,sonderueber,pm_ueber,pm_ls
		 // Konfig-Anleitung siehe oben
		 if (empty($vkal_use_fields_table) or empty($vkal_head_fields_table)) {
			// Alle Felder mit technischem Namen
            // $vkal_use_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
            //$vkal_head_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
			// Default:
			$vkal_use_fields_table = explode(",","startdat,startzeit, bez,ort");
			$vkal_head_fields_table = explode(",","Datum,Start,Veranstaltungen, ort");
        }
        if (count($vkal_use_fields_table) != count($vkal_head_fields_table)) {
            $sniplet .= "Anzahl der Felder und �berschriften in mgvo_sniplet_vkal() nicht gleich";
			return $sniplet;
        }  
		if (empty($vkal_rewrite_fields)) {
			$vkal_rewrite_fields= array(  'resstime' => 'starzeit', 'resetime' => 'endzeit', 'ressdat' => 'startdat');
		}
		
        $vkal_sanitize_fields_table  = explode(",","startdat,startzeit,enddat,endzeit");
 
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
         $resar = $this->api->read_vkal($vkalnr,$seljahr);
	 
		 return $this->mgvo_generic_sniplet($resar , "mgvo-vkal" ,$vkal_use_fields_table, $vkal_head_fields_table, $vkal_sanitize_fields_table, $vkal_rewrite_fields, $vkal_sort, $vkal_filter, $max_count, $page);
	  }
	  
	  
      function mgvo_generic_sniplet($resar, $css_class, $use_fields_table, $head_fields_table, $sanitize_fields_table, $rewrite, $sort, $filter, $max_count, $page) {
         
		 $work_array = 	$resar['objar'];
         //mgvo_add_index( $work_array) ; 
		 
		 $sniplet = "<div class='mgvo ".$css_class."'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table cellpadding=1 cellspacing=0 border=1>";
         $sniplet .= "<tr>";
         
		 // Sortierwerte k�nnen entweder als String �bergeben werden (mit Komma getrent) oder als Array
		 if($sort != NULL && !is_array($sort)) {
			$sort = explode(",",$sort);
		 }
 		 
		 // Filter k�nnen entweder als String mit "feld=Wert,feld=wert,...' �bergeben werden (mit Komma getrent) oder als Array. Leerzeichen im Vergleichswert, Sonderzeichen, etc. werden nur im Array unterst�tzt. 
		 // Gro�/Klein wird ignoriert, es wird bisher nur "=" unterst�tzt. (Kein !=, etc.)
		 /*if($filter != NULL && !is_array($filter))  {
			$filterset = explode(",",$filter);  // "bez,Ball,strpos" oder "ortid,Saal1,="
			$filter = [];
			 foreach($filtersets as $id => $filterset) {
				$filters = explode('=', $filterset);
				if (count($filters) != 2) {error_log("MGVO:mgvo_generic_sniplet: nicht korrekte Filterkritieren".print_r($filtersets)); continue;}  
				$filter[$id]['field'] = trim($filters[0]);
				$filter[$id]['value'] = trim($filters[1]);
				$filter['operator'] = 'EQ'; // For future use
			 } 
		 } */
		 
		 
		 // Sichern der index (damit nach dem Sortieren die URLs f�r Einzelelemente noch passen) - noch nicht klar, ob ben�tigt, kommt auf das sort an. Mit uasort sollte das so gehen
		 //foreach($resar['objar'] as $idx => $vkr) {
		 //	 $vkr['index'] = $idx;
		 //}
		 	 
		 // mgvo_filter_array( $resar, $filterfield, $value, $operator = "==", $and = False) 
		 if ($filter != Null) {
			$work_array = $this->mgvo_filter_array( $work_array, $filter['field'], $filter['value'], $filter['operator'], filter['modus']);	
		 }
		 
		 if($sort != NULL){ 
			 foreach($sort as $sortfield) {
				 uasort ($work_array, function ($a, $b) {if ($a == $b) {return 0;} return ($a[$sortfild] < $b[sortfield]) ? -1 : 1;} );
			 } 
		 }
		 
         foreach ($head_fields_table  as  $header) {
                $sniplet .= "<th class='mgvo-h-".$header."'>".$header."</th>";
         }
         $sniplet .= "</tr>";
		 
		 $max_count=1;
		 //print_r($vkr);
         foreach($work_array as $idx => $vkr) {
			// Sind filterkriterien aktiv, pr�fen, ob diese zutreffen, sonst weiter.
            print_r($vkr);
            echo "Schleife vor filter";
			if($filter != NULL) {
				foreach($filter as $filter2) {
					if (!$vkr[$filter2['Field']] == $filter2['value'] ) { continue; }
				}
			}
			if ( $count != NULL &&  // keine Anzahl angegeben
					( !($count > $max_count && $page = NULL)  // keine Seite angeben, aktuelle Anzahl gr��er Max_Count
					  || ! (($count * $page <= $maxcount) && !(($count * ($page+1)) <=  $maxcount))  // nicht auf der aktuellen Seite 
					)
				) { 
				$count++ ; continue; 
			}
			
            
			if (strstr($vkr['bez'],"NO_WEB")) {continue; } 
					
			if (  strstr($vkr['bez'] , "EXTERN:" )) {
					$vkr['bez'] = str_replace ("EXTERN:","",strstr($vkr['bez'],"EXTERN:")); // Titel ab Extern: (ohne Extern:)
			}
			
			if(stristr(vkr['bez'], "!") != false)  {
				$vkr['bez'] =stristr(vkr['bez'], "!", true);
            }               // alles nach ! entfernen
			
			// Rewrite
			if ( $rewrite != NULL) {
				foreach ($rewrite as $source => $dest) {
				    if ($vkr[$dest]="") {
						$vkr[$dest] = $vkr[$source];
					}
				}
			}
			
            $sniplet .= "<tr>";
            foreach ($use_fields_table as  $field) {
                if (isset($vkr[$field])) {
                    if (in_array($field, $sanitize_fields_table)) {
                        $sniplet .= "<td class='mgvo-f-".$field."'>".date2user($vkr[$field])."</td>"; 
                    } else {
                        $sniplet .= "<td class='mgvo-f-".$field."'>".$vkr[$field]."</td>"; 
                    }
                } else {
                   $sniplet .= "<td class='mgvo-f-".$field."'></td>";
				}
            }
            $sniplet .= "</tr>"; 
			$count++;
         }
         $sniplet .= "</table>";
         $sniplet .= "</div>";
         return $sniplet;
      }
    

	
	  function mgvo_gen_sniplet_vkal_entry($vkalnr,$seljahr, $arrayindex, $use_fields_table = Null, $head_fields_table = Null, $vkal_rewrite_fields = NULL) {
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr und gibt den Temrin mit der EventID aus. 
         //  Verf�gbare Felder: startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb
		 // Konfig-Anleitung siehe oben
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
            //$use_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
			//$head_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
            $use_fields_table = explode(",","startdat,startzeit,endzeit,bez,ortid");
			$head_fields_table = explode(",","Datum,Start,Ende,Veranstaltung,Ort");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und �berschriften in mgvo_sniplet_vkal_entry() nicht gleich";
			return $sniplet;
        }  
		if (empty($vkal_rewrite_fields)) { // �berschreiben von Feldern
			$vkal_rewrite_fields= array('starzeit' => 'resstime', 'endzeit' => 'resetime', 'startdat' => 'ressdat');
		}
		
        //$sanitize_fields_table  = explode(",","startdat"); 
 
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
         $resar = $this->api->read_vkal($vkalnr,$seljahr);
		 
		 return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-vkal-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, $vkal_rewrite_fields );
	  }
	
    
    function mgvo_gen_sniplet_vkal_entry2($vkalnr,$seljahr, $arrayindex, $use_fields_table = Null, $head_fields_table = Null, $vkal_rewrite_fields = NULL) {
         // BGC-Spezialfunktion (aus zwei funktionen zusammen)
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
            //$use_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
			//$head_fields_table = explode(",","startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb");
            $use_fields_table = explode(",","startdat,startzeit,endzeit,bez,ort");
			$head_fields_table = explode(",","Datum,Start,Ende,Veranstaltung,Ort");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und �berschriften in mgvo_sniplet_vkal_entry() nicht gleich";
			return $sniplet;
        }  
		if (empty($vkal_rewrite_fields)) { // �berschreiben von Feldern
			$vkal_rewrite_fields= array('starzeit' => 'resstime', 'endzeit' => 'resetime', 'startdat' => 'ressdat');
		}
	     $sanitize_fields_table  = explode(",","startdat"); 
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
         $resar = $this->api->read_vkal($vkalnr,$seljahr);
		 
		 //return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-vkal-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, $vkal_rewrite_fields );
         
         // function mgvo_generic_sniplet_entry($resar, $arrayindex, $css_class, $use_fields_table, $head_fields_table, $sanitize_fields_table, $rewrite ) {
         $css_class =  "mgvo-vkal-entry";        
         $rewrite = $vkal_rewrite_fields; 
         //error_log("Resarray:");
         //error_log(print_r($resar, true));    
             
         $sniplet = "<div class='mgvo ".$css_class."'>";
         //$sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table cellpadding=1 cellspacing=0 border=1>";
		 
		 //error_log("mgvo_generic_sniplet_entry: arrayindex:".$arrayindex." ccs ".$css_class." use_field ".print_r($use_fields_table, true)." head ".print_r($head_fields_table, true));
         
		 $fields_table = array_combine($use_fields_table, $head_fields_table);
		 //error_log("gen_entry:".substr (print_r($resar, true),0,2500));
		 
		 // Rewrite fehlt noch
		 
		 $sniplet .= "<tr>";
		 foreach ($fields_table  as  $field => $header) {
			$sniplet .= "<tr><th class='mgvo-h-".$field."'>".$header."</th>";
			
			if(!empty($rewrite) &&  in_array( $field,$rewrite)) {
				$resar['objar'][$arrayindex][$field] = $resar['objar'][$arrayindex][$rewrite[$field]];
			}
			
			if (!empty($sanitize_fields_table) && in_array($field, $sanitize_fields_table)) {
				$sniplet .= "<td class='mgvo-f-".$field."'>".date2user($resar['objar'][$arrayindex][$field])."</td></tr>"; 
			} else {
                switch ($field) {
                    case "startzeit":
                    case "endzeit":
                        $sniplet .= "<td class='mgvo-f-".$field."'>".substr($resar['objar'][$arrayindex][$field],0,5)." Uhr</td></tr>";
                        break;
                    case "startdat":
                        $sniplet .= "<td class='mgvo-f-".$field."'>".date2user($resar['objar'][$arrayindex][$field])."</td></tr>"; 
                        break;
                    default:
				        $sniplet .= "<td class='mgvo-f-".$field."'>".$resar['objar'][$arrayindex][$field]."</td></tr>"; 
                }
			}		
		 }
         
         $groupid = $resar['objar'][$arrayindex]['gruid'];
         
         $resar = $this->api->read_gruppen();
         $work_array = 	$resar['objar'];
         
         foreach($work_array as $idx => $vkr) {
             if ($vkr['gruid'] == $groupid) {
                 
                 $sniplet .= "<tr><th class='mgvo-h-grutxt'>Beschreibung</th>";
                 $sniplet .= "<td >".$vkr['grutxt']."</td></tr>";
                 break;
             }
         }
         
         
         
         $sniplet .= "</table>";
         $sniplet .= "</div>";
         return $sniplet;
      
	  }
    
		function mgvo_gen_sniplet_gruppen_entry($arrayindex = '', $gruid = '', $use_fields_table = Null, $head_fields_table = Null) {
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr und gibt den Temrin mit der EventID aus. 
         //  Verf�gbare Felder: startdat,bez,prio,vkalnr,evnr,startzeit,enddat,endzeit,ortid,notiz,rec_day_freq,rec_wk_freq,rec_mon_freq1,rec_mon_tag,rec_mon_freq2,rec_yr_freq1,rec_yr_tag,rec_yr_freq2,rec_range_enddat,ort,ortkb
		 // Konfig-Anleitung siehe oben
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
			// $use_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
            // $head_fields_table = explode(",","gruid,grubez,grutxt,grukat,vorgid,kursvon,kursbis,bgcol,gzfld01,gzfld02,gzfld03,gzfld04,gzfld05,kbez,abtid,lfdnr,wotag,startzeit,endzeit,turnus,ortid,ort,ortkb,tridall,trnameall,trid01,trid,trname,trid02,trfid");
		    $use_fields_table = explode(",","grubez,wotag,startzeit,endzeit,ortid,grutxt,trnameall");
			$head_fields_table = explode(",","Gruppe,Tag,Start,Ende,Ort,Beschreibung,Trainer");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und �berschriften in mgvo_sniplet_gruppen_entry() nicht gleich";
			return $sniplet;
        }  
		
        $sanitize_fields_table  = explode(",","startdat,enddat");
		 
         // Liest die Gruppen
         $resar = $this->api->read_gruppen();
         if ($arrayindex =='' ) { // dann wohl eine GruppenID 
            $work_array = $resar['objar'];
            foreach($work_array as $idx => $vkr) {
                //error_log('test gruid:'.$vkr['gruid']);
                if ($vkr['gruid'] == $gruid) {
                    $arrayindex = $idx;
                    //error_log('idx found');
                    break;
                }
            }
         }
		 
		 return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-gruppen-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, NULL );
	  }
      
      
      
      
	
	  function mgvo_gen_sniplet_event_entry($arrayindex, $use_fields_table = Null, $head_fields_table = Null) {
         // Liest den Veranstaltungseintrag mit Nr. 
         //  Verf�gbare Felder: eventnr,name,ort,startdate,starttime,enddate,endtime,bgcol,email_org,publish,resmaildat,helfermaildat,abrechdat,pubdate,description,vorldat,pub_helfer,wartelisteaktiv,notiz,betr01,betrbez01,ticketbis,saalplan,link,pm_bar,teilnehmerflag,emtickflag,besturl
		 // Konfig-Anleitung siehe oben
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
			// $use_fields_table = explode(",","eventnr,name,ort,startdate,starttime,enddate,endtime,bgcol,email_org,publish,resmaildat,helfermaildat,abrechdat,pubdate,description,vorldat,pub_helfer,wartelisteaktiv,notiz,betr01,betrbez01,ticketbis,saalplan,link,pm_bar,teilnehmerflag,emtickflag,besturl");
            // $head_fields_table = explode(",","eventnr,name,ort,startdate,starttime,enddate,endtime,bgcol,email_org,publish,resmaildat,helfermaildat,abrechdat,pubdate,description,vorldat,pub_helfer,wartelisteaktiv,notiz,betr01,betrbez01,ticketbis,saalplan,link,pm_bar,teilnehmerflag,emtickflag,besturl");
		    $use_fields_table = explode(",","startdate,starttime,name,ort");
			$head_fields_table = explode(",","Datum,Uhrzeit,Veranstaltung,Ort");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und �berschriften in mgvo_sniplet_vkal_entry() nicht gleich";
			return $sniplet;
        }  
		
        $vkal_sanitize_fields_table  = explode(",","starttime,endtime");
 
         // Liest den Vereinskalender mit Nr. vkalnr mit Terminen des Jahres seljahr
         $resar = $this->api->read_events();
		 
		 return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-event-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, NULL );
	  }
	
	  function mgvo_gen_sniplet_notraining_entry($arrayindex, $use_fields_table = Null, $head_fields_table = Null) {
         // Liest den Veranstaltungseintrag mit Nr. 
         //  Verf�gbare Felder: resdat,starttime,endtime,neustarttime,neuendtime,gruid,abtbez,normbelegung,ortsbez,reservierungsgrund
		 // Konfig-Anleitung siehe oben
		 if (empty($use_fields_table) or empty($head_fields_table)) {
			// Alle Felder
			// $use_fields_table = explode(",","resdat,starttime,endtime,neustarttime,neuendtime,gruid,abtbez,normbelegung,ortsbez,reservierungsgrund");
            // $head_fields_table = explode(",","resdat,starttime,endtime,neustarttime,neuendtime,gruid,abtbez,normbelegung,ortsbez,reservierungsgrund");
		    $use_fields_table = explode(",","resdat,starttime,gruid,ortsbez,reservierungsgrund ");
			$head_fields_table = explode(",","Datum,Uhrzeit,Gruppe,Ort,Grund");			
        }
        if (count($use_fields_table) != count($head_fields_table)) {
            $sniplet .= "Anzahl der Felder und �berschriften in mgvo_sniplet_notraining_entry() nicht gleich";
			return $sniplet;
        }  
		
        $vkal_sanitize_fields_table  = explode(",","starttime,endtime");
 
         // Liest die Trainingsausf�lle
         $resar = $this->api->read_training_fail();
		 
		 return $this->mgvo_generic_sniplet_entry($resar , $arrayindex, "mgvo-notraining-entry" ,$use_fields_table, $head_fields_table, $sanitize_fields_table, NULL );
	  }
	
	
	  function mgvo_generic_sniplet_entry($resar, $arrayindex, $css_class, $use_fields_table, $head_fields_table, $sanitize_fields_table, $rewrite ) {
             
         //error_log("Resarray:");
         //error_log(print_r($resar, true));    
             
         $sniplet = "<div class='mgvo ".$css_class."'>";
         $sniplet .= $this->write_headline($resar['headline']);
         $sniplet .= "<table cellpadding=1 cellspacing=0 border=1>";
		 
		 //error_log("mgvo_generic_sniplet_entry: arrayindex:".$arrayindex." ccs ".$css_class." use_field ".print_r($use_fields_table, true)." head ".print_r($head_fields_table, true));
         
		 $fields_table = array_combine($use_fields_table, $head_fields_table);
		 //error_log("gen_entry:".substr (print_r($resar, true),0,2500));
		 
		 // Rewrite fehlt noch

 	 
		 $sniplet .= "<tr>";
		 foreach ($fields_table  as  $field => $header) {
			$sniplet .= "<tr><th class='mgvo-h-".$field."'>".$header."</th>";
			
			if(!empty($rewrite) &&  in_array( $field,$rewrite)) {
				$resar['objar'][$arrayindex][$field] = $resar['objar'][$arrayindex][$rewrite[$field]];
			}
			
			//error_log("gen_snpilet_entry: field:".$field." val:".$resar['objar'][$arrayindex][$field]);
			if (($field == 'grubez') && (  stristr($resar['objar'][$arrayindex][$field],"!") != false )  ) {
				$resar['objar'][$arrayindex][$field] = stristr($resar['objar'][$arrayindex][$field],"!", true); 
				//error_log("gen_snpilet_entry: CHANGE  !!!:".$field." val:".$resar['objar'][$arrayindex][$field]);
			}
			
			
			if (!empty($sanitize_fields_table) && in_array($field, $sanitize_fields_table)) {
				$sniplet .= "<td class='mgvo-f-".$field."'>".date2user($resar['objar'][$arrayindex][$field])."</td></tr>"; 
			} else {
				$sniplet .= "<td class='mgvo-f-".$field."'>".$resar['objar'][$arrayindex][$field]."</td></tr>"; 
			}		
		 }
         $sniplet .= "</table>";
         $sniplet .= "</div>";
         return $sniplet;
      } 
	  
	  /*
	  public function sanitize_date($d){
		if ($d) {  // yyyy-mm-dd
		  return date2user(datefield);
		} else {
			if ($d match ) { // 
				return (subst($d, 0, 6);
			} else {
				return $d;
			}
		}			
	  }*/
	  
	  /** 
	  * Manipulationsfunktionen
	  * Diese k�nnen auf ein mehrdimensionales Array, so wie sie aus der API (d.h. alle Read-Funktionen 
	  * der API verwendet werden.
	  * Beispiel: 
	  * $result_array = read_gruppen();
	  * $obj_array = result_array['objar'];
	  * mgvo_add_index(&$obj_array); // �bergabe als Referenz
	  * $filtered_array = mgvo_filter_array( $ojb_array, 'grubez',"ball", "stripos"); // Ergibt alle  Gruppen die ein "ball" oder "Ball" enthalten.
	  *
	  */ 
	  
	  function mgvo_filter_array( $resar, $filterfield, $value, $operator = "==", $and = False) {  
		  // Filter ein Array nach einem Feldwert, R�ckgabe eines Array, 
		  // $resar - �bergebenen array 
		  // $filterfield - Feld, auf das gefiltert wird
		  // $value - Wert mit dem verglichen wird
		  // $operator - Vergleichsoperator, aktuell nur "==", "!=" "<", ">" und strpos bzw. stripos unterst�tzt.
		  // die filter, values und operatoren k�nnen auch Arrays sein, wenn dann aber alle als Array
		  // $and ob, bei mehrenen Vergleichen ein AND (true) oder OR (false) angewendet wird
		  // Achtung, die Funktion beh�lt den Index nixht bei.
		  $new_ar = array();
		  foreach($resar as $idx => $entry) { 
			  if(is_array($filterfield)) {
				    $and ? $result = true : $result = false;
					foreach ($filterfield as $filter_idx => $field) {
					   if ($this->mgvo_compare($entry[$field],$value[$idx], $operator[$ix])) {
							  if (!$and) $result = true; // bei OR reicht ein einziger positiver Vergleich
						  } else {
							  if ($and) $result = false; // bei AND ein einiger Negativer
						  }
					}
					if ($result) $new_ar[] = $entry;
			  } else {
					if ($this->mgvo_compare($entry[$filterfield],$value, $operator)) {
						$new_ar[] = $entry;
					}
			  }
		  }
		  return $new_ar;	
	  
	  }
	  function mgvo_compare($a, $b, $operator) {
		  // Hilfsfunktion f�r mgvo_filter_array
		  // Vergleich $a mit $b mit den angegebenen Operator
		  switch ($operator) {
			  case "==": return ($a == $b);
			  case "!=": return ($a != $b);
			  case "<" : return ($a < $b);
			  case ">" : return ($a > $b);
			  case "strpos": return (strpos($a, $b) !== false);
			  case "stripos": return (stripos($a, $b) !== false);
		  }
      }
		  

			  
	  
	  function mgvo_add_index( &$resar) {
	  // die Funktion f�gt jedem Hauptelement einen schl�ssel "idx" mit dem Wert des aktuellen index hinzu.
	  // praktisch, wenn die nachfolgenden Funktionen das Array bzw. den Index ggf. ver�ndern (z.B. mgvo_filter_array())
	  // wichtig ist dies f�r alle Sniplets und Wordpressfunktionen, die Links/URLs auf Einzelnwerte referenzieren. Hier muss der Index exakt mit dem Orinalindex �bereinstimmen. 
	  // Die Keys aus dem XML sind leider nicht immer zur Zuordnung verwendbar (z.B. erhalten mehrere Ortsreservierungen alle den selben Veranstaltungs-Key)
	  
		  foreach($resar as $idx => $entry) {
			  $entry['idx']= $idx;
		  }
		  return;  
	  }
	  
		function mgvo_merge_fields(&$resar, $from__field, $to_field, $force = FALSE) {
		  // kopiert Feldwerte in andere Felder (wenn diese leer sind)
		  // wenn es der Schl�ssel noch nicht angelegt ist, wird dieser erzeugt
		  // existiert der Schl�ssel und der Wert Wert ist ungleich "" wird dieser nicht �berschrieben, au�er $force = TRUE
		  // $from__field, $to_field, k�nnen jeweils Einzelwerte als String oder Arrays sein
		  // Praktisch ist die Funktion um Kalenderevents aus unterschiedlichen Qellen zusammenzufassen. Diese haben sehr z.T. sehr unterschiedliche Felder
		  // die hiermit bei Bedarf in einheitliche zusammengefasst werden k�nnen. (Eine Veranstaltung hat z.B. eine Start und ein Enddatum, eine Feiertag oder Traingsausfall aber nur genau ein "Datum")
		  foreach($resar as $idx => $entry) {
			  if(is_array($from__field)) {
					foreach ($from__field as $from_idx => $field) {
						  if (empty($entry[$to_field[$from_idx]]) or $force==true) {
								$entry[$to_field[$from_idx]] = $entry[$field];
						  }
					}
			  } else {
					if(empty($entry[$to_field])) {
						$entry[$to_field] = $entry[$from_field];
					}
			  }
			
			}
			return;
		}
	
	  
   }
   
   
$resource_table = array ();
   
class MGVO_FULLCALENDAR_SNIPLET extends MGVO_SNIPLET {
      
      // *************************
      // Generic Sniplets
      //
      //  Mit diesen Funktion lassen sich Kalendaransichten basierden auf der JavaScript-Bibliothek fullcalendar.io beliebige
      //HTML-Tabellen aus MGVO erstellen.
      // *************************
      
      
      public function mgvo_gen_resource_table (){
         // Die Funktion erstellt eine Resourcentabelle mit allen R�umen.
         // Dazu werden die Orte aus MGVO gelesen
         // Hierbei kann ein zentreales Konfigfile herangezogen werden
         // Output ist ein Array mit
         //   "ortbez" Ortname lang (aus MGVO)
         //	  "ortid" Orts ID (aus MGVO)
         //   "ortkb" (Ort Kurzbezeichnung) (aus MGVO)
         //	  "ortmatch" Regul�rer Ausdruck, der wenn ein Raum angegeben wurde, der nicht den ersten dreien entspricht, verwendet werden. (aus Konfigfile)
         //   Beispiel: Bei einer Veranstaltung wir als Ort (der dort Freitext ist) "Saal Wiesbaden" angegeben. Mit /*.Wiesbaden*./ wird dies der korrekten Bezeichung "Raum Wiesbaden" zugeordnet.
         //	  "ortcal" Raumname Kalenderspalte (aus Konfigdatei, sonst = ortkb)
         //	  "ortlist" Raumname f�r die Liste (aus Konfigdatei, sonst = ortkb)
         //	  "ortlink" Link auf eine Raum bzw. Anfahrtsbeschreibung. (aus Konfig, sonst leer)
         // Die Tabelle wird f�r die Ortszuordnung sowie f�r die Ortbeschreibungen und Verlinkungen ben�tigt.
         // Hinweis: in MGVO wird eine Trainingsst�tte mit "Ort" bezeichnet, in Fulcalender mit "Ressource", an anderen stellen mit "Room" alles ist aber identisch. Dies f�hrt schnell zu Misverst�ndnissen.
         // die Resourcetable wird auch in einer globlaen Variable bereitgestellt.
         
         // Sp�ter sollten diese Setting in der WP-Oberfl�che sein
         $settings_resources = array (
            "BSort" => array ('ortsmatch' => "/*.Bespiel*./", 'ortscal' => "Beispl." , 'ortlink' => "http://beispiellink.de")
         );
         
         global $resource_table;
         
         $resar = $this->api->read_orte();
         $resource_table = array();
         foreach($resar['objar'] as $or) {
            if (isset($or['ortid']) ) {
               $resource_table[$or['ortid']] = array (
                  'ortid' =>  $or['ortid'],
                  'ortbez' => isset($or['ortbez']) ? $or['ortbez'] : $or['ortid'],
                  'ortkb' => isset($or['ortkb']) ? $or['ortkb'] : $or['ortid'] ,
                  'ortmatch' => isset($settings_resources[$or['ortid']][ortmatch]) ? $settings_resources[$or['ortid']][ortmatch] :"",
                  'ortscal' => isset($settings_resources[$or['ortid']][ortscal]) ?$settings_resources[$or['ortid']][ortscal] :$or['ortkb'],
                  'ortlist' => isset($settings_resources[$or['ortid']][ortlist]) ? $settings_resources[$or['ortid']][ortlist] :$or['ortkb'],
                  'ortlink' => isset($settings_resources[$or['ortid']][ortlink]) ? $settings_resources[$or['ortid']][ortlink] :"",
               );
            } else {
               mgvo_log("Ortselement ohne ID aus MGVO gelesen",$or,MGVO_DEBUG_ERR, __FUNCTION__);
            }
         }
         return $resource_table;
         
      }
      
      public function sanitize_resource ($ort, $type = "ortid", $strikt = FALSE) {
         // versucht die passenden Resourceneintrag zu $ort finden.
         // Als R�ckgabe wird der Wert aus dem Resourcearray mit dem Schl�ssel "$type" (ortid, ortkb, etc.) zur�ckgegeben.
         // Alternativ liefert $type = "array" das komplette Array f�r diesen Eintrag zur�ck.
         // Mit $strikt = True muss $ort entweder ortid, ortkb oder ortbez exakt �bereinstimmen.
         // Mit $strikt = False werden leerzeichen am Anfang und Ende entfernt sowie alles Case-Insensitive verglichen.
         // Kommt es zu keiner �bereinstimmung, wird versucht mit ortmatch ein Matching zu erreichen.
         // Mit einem Match = "/.*/ kann auch ein Defaultraum bestimmt werden.
         // wird nichts gefunden, wird bei stric=False der Ort unver�ndert zur�ckgegeben
         
         global $resource_table;
         
         //
         if (isset ($resource_table[$ort])) {
            return $this->return_resource($resource_table[$ort], $type, $ort, $strikt);
            
         }
         
      }
      
      public function return_resource($resource_table_entry, $type, $ort, $strikt ) {
         // Hilffunktion, gibt den geforderterten Eintrag zur�ck.
         if ($type == "array") {
            return $resource_table_entry;
         } else {
            if (isset($resource_table_entry[$type])) {
               return $resource_table_entry[$type];
            } else {
               if ($strikt) {
                  return "";
                  mgvo_log("Ein Ort konte nicht aufgel�st werden, Resourcearray unvollst�ndig",$ort,MGVO_DEBUG_ERR, __FUNCTION__);
               } else {
                  return $ort;
               }
            }
         }
      }
      
   }
   
   
?>
