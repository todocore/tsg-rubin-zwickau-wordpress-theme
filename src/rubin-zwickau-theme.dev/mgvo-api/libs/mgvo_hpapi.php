<?php

   require_once(dirname(__FILE__)."/../include/ext_hlpfkt.php");
   require_once(dirname(__FILE__)."/../include/ext_cipher.php");
   
   class MGVO_HPAPI {
      protected $urlroot;
      protected $debuglevel;
      protected $call_id;
      protected $vcryptkey;
      protected $cacheon;
      protected $cachetime;
      protected $cachedir;
      protected $cacheprogar;
      protected $headline;
      protected $version;
      protected $verein;
      protected $tab;           // Tabelle nach JSON-Umwandlung
            
      function __construct($call_id,$vcryptkey=NULL,$cachetime=5,$cachedir=".") {
         // call_id: call_id des Vereins
         // vcryptkey: Schl�ssel f�r die synchrone Verschl�sselung. Wird in MGVO in den technischen Parametern eingetragen 
         // cachetime: Legt die Cachezeit in Minuten fest. Wenn nicht angegeben, werden 5 Minuten gesetzt
         $this->call_id = $call_id;
         $this->vcryptkey = $vcryptkey;
         $this->cachetime = $cachetime * 60;                // cachetime in Sekunden
         $this->cachedir = $cachedir;
         $this->urlroot = "https://www.mgvo.de/prog";
      }
      
      /**
       * Liest alle Gruppen
       * @return array $tab
       */
      function read_gruppen() {
         $this->cacheon = 1;
         $this->tab = $this->call_request(3);
         return $this->tab;
      }
      
      /**
       * Liest alle Gruppenkategorien
       * @return array $tab
       */
      function read_grukat() {
         $this->cacheon = 1;
         $this->tab = $this->call_request(4);
         return $this->tab;
      }
      
      /**
       * Liest alle Abteilungen
       * @return array $tab
       */
      function read_abt() {
         $this->cacheon = 1;
         $this->tab = $this->call_request(5);
         return $this->tab;
      }
      
      /**
       * Liest alle Trainer/Betreuer
       * @return array $tab
       */
      function read_betreuer() {
         $this->cacheon = 1;
         $this->tab = $this->call_request(1);
         return $this->tab;
      }
      
      /**
       * Liest Termine eines Kalenders
       * @return array $tab
       */
      function read_vkal($vkalnr,$seljahr) {
         $this->cacheon = 1;
         $paras['vkalnr'] = $vkalnr;
         $paras['seljahr'] = $seljahr;
         $this->tab = $this->call_request(30,$paras);
         return $this->tab;
      }
      
      /**
       * Liest alle Orte
       * @return array $tab
       */
      function read_orte() {
         $this->cacheon = 1;
         $this->tab = $this->call_request(2);
         return $this->tab;
      }
      
      /**
       * Liest alle �ffentlichen Veranstaltungen
       * @return array $tab
       */
      function read_events() {
         $this->cacheon = 1;
         $this->tab = $this->call_request(6);
         return $this->tab;
      }
      
      /**
       * Liest alle Trainingsausf�lle
       * @return array $tab
       */
      function read_training_fail() {
         $this->cacheon = 1;
         $this->tab = $this->call_request(31);
         return $this->tab;
      }
      
      /**
       * Liest alle Beitragsgruppen
       * @return array $tab
       */
      function read_beigru() {
         $this->cacheon = 1;
         $this->tab = $this->call_request(12);
         return $this->tab;
      }
      
      /**
       * Liest alle Tarife
       * @return array $tab
       */
      function read_tarife() {
         $this->cacheon = 1;
         $this->tab = $this->call_request(11);
         return $this->tab;
      }
      
      /**
       * Liest alle �ffentliche Dokumente, ggf. nur von einer Dokumentart
       * @param string $dokart
       * @return array $tab
       */
      function read_documents($dokart=NULL) {
         $this->cacheon = 1;
         $paras['dokart'] = $dokart;
         $this->tab = $this->call_request(30,$paras);
         return $this->tab;
      }
      
      /**
       * Liest Mitglieder in Abh�ngigkeit von Selektionsparameter<br>
       * Allgemeiner Suchbegriff: suchbeg<br>
       * Suchalter/Geburtsdatum: suchalterv - suchalterb<br>
       * Austritt: suchaustrittv - suchaustrittb<br>
       * Gruppen-ID: suchgruid<br>
       * Beitragsgruppe: suchbeigru<br>
       * Lastschriftzahler: lssel (Selektionswert: 1)<br>
       * Barzahler/�berweiser: barsel (Selektionswert: 1)<br>
       * Dauerauftrag: dasel (Selektionswert: 1)<br>
       * Geschlecht: geschl (x,m,w)<br>
       * Mitglied: ausgetr (x,m,a)<br>
       * Aktiv/Passiv: aktpass (x,a,p)<br>
       * Mailempf�nger: mailempf (x,e,s)<br>
       * Inland/Ausland: landsel (x,i,a)<br>
       * Mahnstufe: selmahnstufe (a,1,2,3)<br>
       * 
       * @param array $selparas
       * @return array $tab
       */
      function read_mitglieder($selparas) {
         $this->cacheon = 1;
         $this->tab = $this->call_request(7,$selparas,1);
         return $this->tab;
      }
      
      /**
       * Lies einen Mitgliederstammsatz
       * @param int $mgnr
       * @return array $mr         Mitgliedstammsatz
       */
      function show_mitglied($mgnr) {
         $selparar['suchbeg'] = $mgnr;
         $this->read_mitglieder($selparar);
         $mr = $this->tab['objar'][0];
         return $mr;
      }
      
      /**
       * Liest die Daten des Mitgliederpassbildes
       * @param int $mgnr
       * @return array $tab
       */
      function get_mitpict($mgnr) {
         $paras['mgnr'] = $mgnr;
         $this->tab = $this->call_request(8,$paras,1);
         return $this->tab;
      }
      
      /**
       * Erzeugt einen Mitgliederstammsatz. Die Parameter entsprechen den Importdaten
       * Returncodes:<br>
       *  -1: Parameter missing<br>
       * -16: Mussfeld nicht vorhanden<br>
       * -17: Feldwert nicht im Definitionsbereich<br>
       *   0: Mitgliedsnummer<br>
       * @param array $inar
       * @return array $tab      Die Tabelle enth�lt den Returncode und -text
       */
      function create_mitstamm($inar) {
         $paras['inar'] = $inar;
         $this->tab = $this->call_request(20,$paras,1);
         return $this->tab;
      }
      
      function login($email_id,$passwd,$smscode) {
         // Die Methode hat folgende Returncodes:
         // 0  : Passwort nicht in Ordnung / User nicht vorhanden
         // 1  : Login ok
         // 11 : Max. Logon-Versuche �berschritten
         // 12 : Geheimcode generiert und an Mobilger�t versendet, Logon muss mit Code erfolgen
         // 13 : Geheimcode (SMS-Code) stimmt nicht �berein
        
         $this->cacheon = 0;
         $vars['call_id'] = $this->call_id;
         $vars['email_id'] = $email_id;
         $vars['passwd'] = $passwd;
         $vars['smscode'] = $smscode;
         $paras = http_build_query($vars);
         $url = "$this->urlroot/pub_mgb_validate.php?$paras";
         $ret = http_get($url);
         $retcode = (int) $ret;
         return $retcode;
      }
      
      // Allgemeine Funktionen
      
      function set_debuglevel($debuglevel) {
         global $mgvo_debug;
         $this->debuglevel = $debuglevel;
         $mgvo_debug = $debuglevel;
      }
      
      /**
       * Ruft zentral f�r alle Requests das Programm api_entry.php auf. 
       * @param int $reqtype
       * @param array $paras
       * @param number $secured
       * @param number $postflg
       * @return array $ergtab
       */
      function call_request($reqtype,$paras=[],$secured=0,$postflg=0) {
         $basisurl = "https://www.mgvo.de/api/api_entry.php";
         $cparas['reqtype'] = $reqtype;
         $cparas['outmode'] = 1;              // JSON
         $cparas['call_id'] = $this->call_id;
         if ($secured) {
            $paras['call_id'] = $this->call_id;
            $paras['time'] = time();
            $parasc = paras_encrypt($paras,$this->vcryptkey);
            $cparas['paras'] = $parasc;
         }
         else $cparas = array_merges($cparas,$paras);
         $urlparas = http_build_query($cparas);
         
         $fn = sprintf("%s/mgvo-hpapi-%s.cache",$this->cachedir,$urlparas);
         if ($this->cacheon && is_file($fn)) {
            $filetime = filemtime($fn);
            if (time() - $filetime <= $this->cachetime) $ret = file_get_contents($fn);
         }
         if (empty($ret)) {
            if ($postflg) {
               $ret = http_post($url,$cparas);
            }
            else {
               $url .= $basisurl."?".$urlparas;
               $ret = http_get($url);
            }
            if (empty($ret) || substr($ret,0,5) == "ERROR") {
               mgvo_log("API Fehler:",substr($ret,5),MGVO_DEBUG_ERR);
            }
            elseif ($this->cachetime > 0) file_put_contents($fn,$ret);
         }
         $ergtab = json_decode($ret,true);
         $objname = $ergtab['objname'];
         if (isset($ergtab[$objname])) $ergtab['objar'] = $ergtab[$objname];
         unset($ergtab[$objname]);
         return $ergtab;
      }
      
   }
   
?>
