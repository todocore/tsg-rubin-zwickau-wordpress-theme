## MGVO API
Dieses Repository stellt den Code für einen externen Zugriff auf MGVO über eine API bereit.
In der Datei /libs/mgvo-hpapi.php ist die eigentliche API. In der Datei /libs/mgvo-sniplets.php befinden sich Funktionen, die auf der API aufbauen und fertige HTML-Sniplets zur Integration in eine Homepage anbieten.

Hierauf aufbauend gibt es auch ein fertiges Wordpress-Plugin (https://gitlab.com/mgvo-public/mgvo-wordpress), welches die Sniplets entsprechend einbindet.
Durch Anpassung nur in den Sniplets kann ohne tiefes Wordpress Wissen sehr leicht eine individuelle Wordpress-Integration vorgenommen werden. 

## Installation
Die Dateien werden in ein Verzeichnis auf dem Server kopiert.

## Test
Die Datei /test/mgvo_hpapi_tst.php enthält Beispielcoding, um die API zu instanziieren und eine Methode aufzurufen.

## Sicherheit
Es werden teilweise Funktionen bereit gestellt, die eine höhere Anforderung an die Datensicherheit haben. Zur Nutzung dieser Methoden muss im System ein geheimer Schlüssel unter "System -> Basiseinstellungen -> Allg. Parameter", Technik hinterlegt werden. Dieser Schlüssel wird bei der Instanziierung der API-Klasse als Parameter mitgegeben. (Im Wordpress-Plugin müssen diese Angaben auf den Einstellungsseiten hinterlegt werden. )
