<?php get_header(); ?>

<?php get_template_part('/parts/navbar', 'desktop'); ?>
<?php get_template_part('/parts/navbar', 'mobile'); ?>

<!-- TEMPLATE: single -->
<div class="lg:tw-w-rubin/lg tw-min-h-75% tw-mx-auto tw-mt-28 tw-bg-white tw-px-6 lg:tw-px-12 tw-py-12 tw-shadow-xl">
  <?php
    if (have_posts()) {
      while (have_posts()) {
        the_post();
        echo '<h1>'.get_the_title().'</h1>';
        echo '<img';
        echo '  alt=""';
        echo '  src="'.get_the_post_thumbnail_url(get_the_ID(), 'medium').'"';
        echo '  class="tw-float-left tw-w-full sm:tw-w-auto tw-mr-8 tw-mb-4"';
        echo '/>';
        the_content();
      }
    }
  ?>
</div>

<?php get_template_part('/parts/footer', 'default'); ?>

<?php get_footer(); ?>
