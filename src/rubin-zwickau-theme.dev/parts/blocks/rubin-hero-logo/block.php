<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_render_block_rubin_hero_logo($block_attributes, $content) {
  if (is_admin()) {
    return '';
  };

  ob_start();
    $scale = $block_attributes['scale'];
    $xOffset = $block_attributes['xOffset'];
    $yOffset = $block_attributes['yOffset'];

    get_template_part('/parts/blocks/rubin-hero-logo/layout', null, array(
      'id' => uniqid(),
      'scale' => isset($scale) ? $scale : 66,
      'xOffset' => isset($xOffset) ? $xOffset : 0,
      'yOffset' => isset($yOffset) ? $yOffset : 0,
    ));
  return ob_get_clean();
}

function rubin_register_block_rubin_hero_logo() {
  register_block_type('rubin/rubin-hero-logo', array(
    'api_version' => 2,
    'render_callback' => 'rubin_render_block_rubin_hero_logo',
  ));  
}
add_action('init', 'rubin_register_block_rubin_hero_logo');