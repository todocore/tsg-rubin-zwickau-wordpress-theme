<div id="rubin-widget-hero-logo-<?php echo $args['id']; ?>" class="tw-relative tw-w-full tw-flex tw-justify-center tw-items-center tw-z-10">
  <img
    alt="TSG Rubin Zwickau e.V."
    src="/wp-content/themes/rubin-zwickau-theme/assets/img/LogoRubin-hero.png"
    class="tw-absolute tw-top-1/2 tw-left-1/2 tw-pointer-events-none"
    style="
      max-width: none;
      width: <?php echo $args['scale'] ?>vW;
      transform: translate(calc(-50% + <?php echo $args['xOffset'] ?>%), calc(-50% + <?php echo $args['yOffset'] ?>%));
    "
  />
</div>
