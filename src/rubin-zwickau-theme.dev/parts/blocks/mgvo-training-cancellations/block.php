<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_render_block_mgvo_training_cancellations($block_attributes, $content) {
  if (is_admin()) {
    return '';
  };

  global $mgvoService;

  $limitByDays = $block_attributes['limitByDays'];
  $ignoreText = $block_attributes['ignoreText'];

  $instance = array(
    'limitByDays' => isset($limitByDays) ? $limitByDays : '0',
    'ignore' => isset($ignoreText) ? $ignoreText : $defaultIgnore
  );

  $refusals = $mgvoService->fetchRefusals();
  $refusals = $mgvoService->sanitizeRefusals($refusals, false, $instance);

  ob_start();
  if (sizeof($refusals) > 0) {
    get_template_part('/parts/blocks/mgvo-training-cancellations/layout', null, array(
      'id' => uniqid(),
      'data' => $refusals,
    ));
  } else {
    get_template_part('/parts/widget/message', null, array(
      'container' => 'bordered',
      'message' => __('Aktuell sind keine Trainingsausfälle gemeldet.', RUBIN_TEXT_DOMAIN),
    ));
  }
  return ob_get_clean();
}

function rubin_register_block_mgvo_training_cancellations() {
  register_block_type('rubin/mgvo-training-cancellations', array(
    'api_version' => 2,
    'render_callback' => 'rubin_render_block_mgvo_training_cancellations'
  ));  
}
add_action('init', 'rubin_register_block_mgvo_training_cancellations');