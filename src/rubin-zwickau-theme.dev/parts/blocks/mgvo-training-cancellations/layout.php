<div id="rubin-widget-mgvo-cancellations-<?php echo $args['id']; ?>" class="rubin-widget">
  <div class="rubin-widget-container bordered tw-flex tw-flex-col tw-justify-center tw-content-center">
    <div class="tw-py-4 tw-px-4 tw-text-lg tw-font-bold tw-text-rubinRot tw-text-center"><?php echo __('Folgende Trainingstermine fallen leider aus:', RUBIN_TEXT_DOMAIN); ?></div>
    <?php foreach ($args['data'] as $refusal) { ?>
      <div class="tw-border-0 tw-border-t-2 tw-border-solid tw-border-rubinRot-darkShadow tw-py-4 tw-px-4">
        <div class="tw-text-xl tw-font-bold"><?php echo $refusal['grbez']; ?></div>
        <div class="tw-text-base tw-flex tw-flex-row tw-flex-nowrap">
          <div class="tw-flex-auto tw-text-left"><?php echo date('d.m.Y', strtotime($refusal['sdat']))  ?></div>
          <div class="tw-flex-auto tw-text-right"><?php echo date('H:i', strtotime($refusal['starttime']))  ?><?php echo __(' Uhr', RUBIN_TEXT_DOMAIN); ?></div>
        </div>
      </div>
    <?php } ?>
  </div>
</div>