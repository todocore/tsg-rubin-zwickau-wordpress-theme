<?php
  $templateId = 'rubin-widget-mgvo-calendar-list-'.$args['id'];
?>

<div id="<?php echo $templateId ?>" class="rubin-widget">
  <div class="rubin-widget-container bordered tw-flex tw-flex-col tw-justify-center tw-content-start tw-w-full tw-overflow-hidden">
    <div>
      <div class="tw-flex-grow-0 tw-flex-shrink-0 tw-flex tw-justify-center tw-items-center tw-w-full tw-h-12">
        <?php rubin_svg(get_template_directory().'/assets/img/chevron.svg', array(
          'v=if' => 'offset &gt; 0',
          '@click' => 'offset = Math.max(0, offset - 1)',
          'class' => 'tw-block tw-transform tw-rotate-180 tw-w-8 tw-h-8 tw-text-gray-300 tw-transition-colors tw-duration-300 tw-cursor-pointer hover:tw-text-rubinRot'
        )); ?>
      </div>
      <?php foreach ($args['allEvents'] as $idx => $events) { ?>
      <div
        :class="{
          'tw-hidden': offset !== <?php echo $idx; ?>
        }"
        class="tw-px-4"
      >
        <?php foreach ($events as $event) { ?>
        <div class="tw-pb-8 last:tw-pb-0">
          <div class="tw-text-base">
            <span><?php echo date('d.', strtotime($event['date'])) ?></span>
            <span>{{ <?php echo "month".date('m', strtotime($event['date'])) ?> }}</span>
            <span><?php echo date('Y', strtotime($event['date'])) ?></span>
          </div>
          <div class="tw-text-base"><?php echo $event['zeitraum']; ?></div>
          <div class="tw-text-base"><?php echo $event['ort']; ?></div>
          <div class="tw-font-bold"><?php echo $event['name']; ?></div>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <div class="tw-flex-grow-0 tw-flex-shrink-0 tw-flex tw-justify-center tw-items-center tw-w-full tw-h-12">
        <?php rubin_svg(get_template_directory().'/assets/img/chevron.svg', array(
          'v=if' => 'offset &lt; maxOffset',
          '@click' => 'offset = Math.min(maxOffset, offset + 1)',
          'class' => 'tw-block tw-w-8 tw-h-8 tw-text-gray-300 tw-transition-colors tw-duration-300 tw-cursor-pointer hover:tw-text-rubinRot'
        )); ?>
      </div>
    </div>
  </div>
</div>

<script type="application/javascript">
const TEMPLATE_ID = '<?php echo $templateId ?>'

const rubinWidgetMgvoCalendarList = new Vue({
  el: `#${TEMPLATE_ID}`,
  name: 'MGVO Calendar Widget (list layout)',

  data() {
    return {
      offset: 0,
      maxOffset: <?php echo (sizeof($args['allEvents']) - 1) ?>,
      month01: 'Januar',
      month02: 'Februar',
      month03: 'März',
      month04: 'April',
      month05: 'Mai',
      month06: 'Juni',
      month07: 'Juli',
      month08: 'August',
      month09: 'September',
      month10: 'Oktober',
      month11: 'November',
      month12: 'Dezember',
    }
  }
})
</script>
