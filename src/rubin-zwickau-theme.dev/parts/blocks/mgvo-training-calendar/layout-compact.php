<?php
  $templateId = 'rubin-widget-mgvo-calendar-compact-'.$args['id'];
?>

<div id="<?php echo $templateId ?>" class="rubin-widget">
  <!-- SIDE MENU -->
  <div
    id="rubin-side-menu"
    class="
      tw-fixed
      tw-left-1/2 lg:tw-left-0
      tw-bottom-0 lg:tw-bottom-auto lg:tw-top-1/2
      tw-transform tw--translate-x-1/2 lg:tw--translate-x-0 lg:tw--translate-y-1/2
      tw-z-30
    "
  >
    <div
      class="
        tw-flex tw-flex-row lg:tw-flex-col tw-flex-nowrap
        tw-border-4 tw-border-b-0 lg-border-b-4 lg:tw-border-0
        tw-rounded-t-lg lg:tw-rounded-t-none lg:tw-rounded-r-lg
        tw-border-solid tw-border-rubinRot
        tw-bg-rubinRot-shadow
        tw-overflow-hidden
      "
    >
      <!-- BACK TO TODAY -->
      <div :class="cssSideMenuButton" @click="handleBackToToday">
        <?php rubin_svg(get_template_directory().'/assets/img/calendar-today.svg', array('class' => 'tw-inline-block tw-w-6 tw-h-6')); ?>
        <div class="tw-text-xs">{{ i18n.event.backToToday }}</div>
      </div>
      <!-- SWITCH TO DAY VIEW -->
      <div :class="cssSideMenuButton" @click="handleSwitchToDay">
        <?php rubin_svg(get_template_directory().'/assets/img/calendar-daily.svg', array('class' => 'tw-inline-block tw-w-6 tw-h-6')); ?>
        <div class="tw-text-xs">{{ i18n.event.switchToDayView }}</div>
      </div>
      <!-- SWITCH TO MONTH VIEW -->
      <div :class="cssSideMenuButton" @click="handleSwitchToMonth">
        <?php rubin_svg(get_template_directory().'/assets/img/calendar-monthly.svg', array('class' => 'tw-inline-block tw-w-6 tw-h-6')); ?>
        <div class="tw-text-xs">{{ i18n.event.switchToMonthView }}</div>
      </div>
    </div>
  </div>
  <!-- MAIN CONTENT AREA -->
  <div class="rubin-widget-container bordered tw-relative tw-flex tw-flex-col tw-justify-center tw-content-center tw-w-full tw-overflow-hidden">
    <!-- LOADING ANIMATION -->
    <div id="rubin-loading-in-progress" class="tw-py-8 tw-text-center" style="display: block">
      <div class="tw-pb-4 tw-text-xl tw-text-rubinRot">
        Kalenderdaten werden geladen
      </div>
      <div class="tw-w-24 tw-mx-auto">
        <svg class="tw-animate-spin" viewBox="0 0 100 100">
          <linearGradient id="pleaseWaitGradient">
            <stop offset="0"style="stop-color:#9e0707;stop-opacity:0.25;" />
            <stop offset="0.75"style="stop-color:#9e0707;stop-opacity:0.25;" />
            <stop offset="1"style="stop-color:#9e0707;stop-opacity:1;" />
          </linearGradient>
          <circle cx="50" cy="50" r="40" stroke="url(#pleaseWaitGradient)" stroke-width="10" fill="none" />
        </svg>
      </div>
    </div>
    <!-- CALENDAR -->
    <div id="rubin-loading-finished" style="display: none" :class="{ 'tw-pb-4': showDay }">
      <!-- DATE SWITCHER -->
      <div class="tw-sticky tw-flex tw-flex-row tw-flex-nowrap tw-justify-center tw-items-center tw-py-2 tw-font-bold tw-bg-rubinRot-darkShadow">
        <!-- SELECTED DAY OF WEEK -->
        <div class="tw-hidden sm:tw-flex sm:tw-flex-col tw-w-36 tw-text-right">
          <div class="tw-mr-2">{{ weekDayOfSelectedDate }},</div>
        </div>
        <!-- SELECTED DAY -->
        <div class="tw-flex tw-flex-col">
          <div class="rubin-chevron up tw-h-8 tw-w-8 tw-mx-auto" :class="{'disabled': !selectedDay.prevDay}" @click="handlePrevDay"></div>
          <div>{{ dayOfSelectedDate }}.</div>
          <div class="rubin-chevron down tw-h-8 tw-w-8 tw-mx-auto" :class="{'disabled': !selectedDay.nextDay}" @click="handleNextDay"></div>
        </div>
        <!-- SELECTED MONTH -->
        <div class="tw-flex tw-flex-col">
          <div class="rubin-chevron up tw-h-8 tw-w-8 tw-mx-auto" :class="{'disabled': !selectedDay.prevMonth}" @click="handlePrevMonth"></div>
          <div class="tw-ml-1 tw-mr-2">{{ monthOfSelectedDate }}</div>
          <div class="rubin-chevron down tw-h-8 tw-w-8 tw-mx-auto" :class="{'disabled': !selectedDay.nextMonth}" @click="handleNextMonth"></div>
        </div>
        <!-- SELECTED YEAR -->
        <div class="tw-flex tw-flex-col">
          <div class="rubin-chevron up tw-h-8 tw-w-8 tw-mx-auto" :class="{'disabled': !selectedDay.prevYear}" @click="handlePrevYear"></div>
          <div>{{ yearOfSelectedDate }}</div>
          <div class="rubin-chevron down tw-h-8 tw-w-8 tw-mx-auto" :class="{'disabled': !selectedDay.nextYear}" @click="handleNextYear"></div>
        </div>
        <!-- SELECTED WEEK -->
        <div class="tw-hidden sm:tw-flex sm:tw-flex-col">
          <div class="rubin-chevron up tw-h-8 tw-w-8 tw-mx-auto" :class="{'disabled': !selectedDay.prevWeek}" @click="handlePrevWeek"></div>
          <div class="tw-ml-2">({{ i18n.event.calendarWeek }} {{ weekOfSelectedDate }})</div>
          <div class="rubin-chevron down tw-h-8 tw-w-8 tw-mx-auto" :class="{'disabled': !selectedDay.nextWeek}" @click="handleNextWeek"></div>
        </div>
      </div>

      <!-- CALENDAR MODE: DAY -->
      <div v-if="showDay">
        <!-- NUMBER OF VISIBLE LOCATIONS -->
        <div class="tw-mt-4 tw-mb-2 tw-pl-8 sm:tw-pl-24 tw-text-xs tw-text-gray-300 tw-text-center tw-cursor-pointer">
          <span class="tw-inline sm:tw-hidden">1</span>
          <span class="tw-hidden sm:tw-inline md:tw-hidden">2</span>
          <span class="tw-hidden md:tw-inline">3</span>
          {{ i18n.location.count }}
          {{ selectedLocationUpperLimit + 1 }}
          {{ i18n.location.locations }}
        </div>
        <!-- EVENT TABLE -->
        <div class="tw-relative tw-flex tw-flex-row tw-flex-nowrap tw-text-rubinRot">

          <div class="tw-flex-shrink-0 tw-flex-grow-0 tw-w-20 tw-leading-8 tw-text-center">
            <div class="tw-h-16">&nbsp;</div>
            <template v-for="hour in i18n.hod">
              <div class="tw-bg-rubinRot-darkShadow" style="height: 1px; width: 9999px;"></div>
              <div style="height: 29px;">{{ hour }}</div>
            </template>
          </div>

          <div class="tw-flex-1 tw-relative tw-flex tw-flex-row tw-flex-nowrap">
            <div
              v-for="(locationId, index) in sortedLocations"
              class="tw-relative"
              :class="{
                'tw-w-full sm:tw-w-1/2 md:tw-w-1/3': index === 0,
                'tw-hidden sm:tw-block tw-w-1/2 md:tw-w-1/3': index === 1,
                'tw-hidden md:tw-block tw-w-1/3': index === 2,
              }"
            >
              <div class="tw-flex tw-flex-row tw-flex-nowrap tw-h-16">
                <!-- PREV LOCATION SWITCHER -->
                <div
                  v-if="index === 0"
                  class="tw-block md:tw-hidden tw-flex-shrink-0 tw-flex-grow-0 rubin-chevron dark left tw-h-8 tw-w-8"
                  @click="handlePrevLocation"
                ></div>
                <div
                  v-if="showNextLocationIconAboveMD(0, index)"
                  class="tw-hidden md:tw-block tw-flex-shrink-0 tw-flex-grow-0 rubin-chevron dark left tw-h-8 tw-w-8"
                  @click="handlePrevLocation"
                ></div>
                <!-- LOCATION -->
                <div class="tw-flex-1 tw-text-center">
                  {{ eventsByLocation[locationId].label }}
                </div>
                <!-- NEXT LOCATION SWITCHER -->
                <div
                  v-if="index === 0"
                  class="tw-block sm:tw-hidden tw-flex-shrink-0 tw-flex-grow-0 rubin-chevron dark right tw-h-8 tw-w-8 tw-mr-4"
                  @click="handleNextLocation"
                ></div>
                <div
                  v-if="index === 1"
                  class="tw-hidden sm:tw-block md:tw-hidden tw-flex-shrink-0 tw-flex-grow-0 rubin-chevron dark right tw-h-8 tw-w-8 tw-mr-4"
                  @click="handleNextLocation"
                ></div>
                <div
                  v-if="showNextLocationIconAboveMD(2, index)"
                  class="tw-hidden md:tw-block tw-flex-shrink-0 tw-flex-grow-0 rubin-chevron dark right tw-h-8 tw-w-8 tw-mr-4"
                  @click="handleNextLocation"
                ></div>
              </div>
              <!-- EVENTS BY LOCATION -->
              <div class="tw-relative">
                <div
                  v-for="event in eventsByLocation[locationId].events"
                  :key="event.id"
                  class="
                    tw-group tw-absolute tw-left-0
                    tw-text-sm
                    tw-border tw-border-rubinRot-shadowDark tw-border-solid tw-rounded
                    tw-cursor-default
                  "
                  :style="{ top: `${event.msm}px`, width: 'calc(100% - 0.25rem)', height: `${event.duration}px`, backgroundColor: event.bgColor }"
                >
                  <div class="tw-relative">
                    <!-- TOOLTIP -->
                    <div class="
                      tw-hidden group-hover:tw-block
                      tw-absolute tw-bottom-0
                      tw-border-2 tw-border-solid tw-border-rubinRot tw-shadow
                      tw-transform tw--translate-x-8 tw--translate-y-2
                      tw-rounded
                      tw-p-2
                      tw-bg-rubinRot-shadow
                      tw-z-10
                    ">
                      <div class="tw-font-bold">{{ event.name }}</div>
                      <div class="tw-text-base">{{ event.zeitraum }}</div>
                      <div v-if="!!event.ort" class="tw-text-base">{{ event.ort }}</div>
                      <div v-if="!!event.trainer" class="tw-text-base">{{ i18n.event.with }} {{ event.trainer }}</div>
                    </div>
                  </div>
                  <!-- EVENT -->
                  <div class="tw-px-2 tw-py-1 tw-overflow-hidden" :style="{ maxHeight: `${event.duration}px` }">
                    <div class="tw-font-bold" :style="{ color: event.bgColor, filter: 'invert(1)' }">{{ event.name }}</div>
                    <div v-if="!!event.trainer" :style="{ color: event.bgColor, filter: 'invert(1)' }">{{ i18n.event.with }} {{ event.trainer }}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- CALENDAR MODE: MONTH -->
      <div v-if="showMonth">
        <div v-if="magnifiedWeekDay === undefined" class="tw-flex tw-flex-row tw-flex-nowrap tw-text-rubinRot">
          <div class="tw-hidden md:tw-block tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dow[1] }}</div>
          <div class="tw-hidden md:tw-block tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dow[2] }}</div>
          <div class="tw-hidden md:tw-block tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dow[3] }}</div>
          <div class="tw-hidden md:tw-block tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dow[4] }}</div>
          <div class="tw-hidden md:tw-block tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dow[5] }}</div>
          <div class="tw-hidden md:tw-block tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dow[6] }}</div>
          <div class="tw-hidden md:tw-block tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dow[7] }}</div>

          <div class="md:tw-hidden tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dowShort[1] }}</div>
          <div class="md:tw-hidden tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dowShort[2] }}</div>
          <div class="md:tw-hidden tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dowShort[3] }}</div>
          <div class="md:tw-hidden tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dowShort[4] }}</div>
          <div class="md:tw-hidden tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dowShort[5] }}</div>
          <div class="md:tw-hidden tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dowShort[6] }}</div>
          <div class="md:tw-hidden tw-w-1/7 tw-leading-8 tw-text-center">{{ i18n.dowShort[7] }}</div>
        </div>
        <div class="tw-flex tw-flex-row tw-flex-wrap">
          <div
            v-for="(day, dayIndex) in daysOfMonth"
            class="tw-min-h-48 tw-border tw-border-solid tw-border-rubinRot-darkerShadow tw-py-1 tw-cursor-pointer"
            :class="{
              'tw-hidden': showDayOfMonthHidden(dayIndex),
              'tw-w-1/7': showDayOfMonthNormal(dayIndex),
              'tw-w-5/7': showDayOfMonthMagnified(dayIndex),
            }"
          >
            <div
              class="tw-h-full tw-flex tw-flex-col tw-item-stretch  tw-leading-8 tw-text-left tw-px-1 tw-text-rubinRot"
              :class="{
                'tw-opacity-10': day.thisMonth !== monthOfDay,
              }"
            >
              <div @click=handleSwitchToDate(day) class="tw-flex-shrinking-0 tw-flex-grow-0 tw-flex tw-flex-row tw-flex-nowrap tw-justify-between hover:tw-bg-rubinRot-darkShadow tw-cursor-pointer">
                <div class="tw-text-left">
                  {{ day.uiDate }}
                </div>
                <div v-if="showDayOfMonthMagnified(dayIndex)" class="tw-text-center tw-px-2">
                  {{ i18n.dow[day.thisWeekDay] }}
                </div>
                <div
                  class="tw-leading-8 tw-text-right tw-pr-2 tw-text-sm"
                  :class="[cssCalendarWeek(dayIndex)]"
                >
                  {{ i18n.event.calendarWeek }} {{ day.thisWeek }}
                </div>
              </div>
              <!-- LIST OF EVENTS -->
              <div class="tw-flex-1" @click="handleClickOnWeekDay(day, dayIndex)">
                <div
                  v-for="event in day.events"
                  :key="event.id"
                  class="tw-group tw-text-xs"
                >
                  <div
                    v-if="showDayOfMonthMagnified(dayIndex)"
                    class="tw-mx-2 tw-mb-1 tw-px-6 tw-py-4 tw-rounded"
                    :style="{ backgroundColor: event.bgColor }"
                  >
                    <div v-if="" :style="{ color: event.bgColor, filter: 'invert(1)' }">
                      <div class="tw-font-bold">{{ event.name }}</div>
                      <div v-if="showEventDetails(event)">
                        <div v-if="!!event.zeitraum">{{ event.zeitraum }}</div>
                        <div v-if="!!event.ort">{{ event.ort }}</div>
                        <div v-if="!!event.trainer">{{ i18n.event.with }} {{ event.trainer }}</div>
                      </div>
                    </div>
                  </div>
                  <div v-else class="tw-relative">
                    <!-- TOOLTIP -->
                    <div
                      v-if="showEventDetails(event)"
                      class="
                        tw-hidden group-hover:tw-block
                        tw-absolute tw-bottom-3/4
                        tw-border-2 tw-border-solid tw-border-rubinRot tw-shadow
                        tw-transform tw--translate-x-8 tw--translate-y-2
                        tw-rounded
                        tw-p-2
                        tw-bg-rubinRot-shadow
                        tw-z-10
                      "
                    >
                      <div class="tw-font-bold">{{ event.name }}</div>
                      <div v-if="!!event.zeitraum" class="tw-text-base">{{ event.zeitraum }}</div>
                      <div v-if="!!event.ort" class="tw-text-base">{{ event.ort }}</div>
                      <div v-if="!!event.trainer" class="tw-text-base">{{ i18n.event.with }} {{ event.trainer }}</div>
                    </div>
                    <!-- EVENT -->
                    <div class="tw-overflow-hidden">
                      <div class="tw-px-2 tw-py-1 tw-min-h-4" :style="{ backgroundColor: event.bgColor }">
                        <div class="tw-hidden md:tw-block" :style="{ color: event.bgColor, filter: 'invert(1)' }">
                          {{ event.name }}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="application/javascript">
const TEMPLATE_ID = '<?php echo $templateId ?>'

const rubinWidgetMgvoCalendarCompact = new Vue({
  el: `#${TEMPLATE_ID}`,
  name: 'MGVO Calendar Widget (compact layout)',

  data() {
    return {
      allEvents: <?php echo json_encode($args['allEvents']) ?>,
      i18n: <?php echo json_encode($args['i18n']) ?>,
      selectedDate: '<?php echo $args['today'] ?>',
      selectedDisplayMode: 'MONTH',
      selectedLocation: 0,
      today: '<?php echo $args['today'] ?>',
      magnifiedWeekDay: undefined,
    }
  },

  computed: {
    selectedDay() {
      return this.allEvents[this.selectedDate] || {}
    },
    dayOfSelectedDate() {
      return this.selectedDate.substring(8, 10)
    },
    monthOfSelectedDate() {
      return this.i18n.moy[this.selectedDate.substring(5, 7)] 
    },
    yearOfSelectedDate() {
      return this.selectedDate.substring(0, 4)
    },
    weekOfSelectedDate() {
      return this.selectedDay.thisWeek
    },
    weekDayOfSelectedDate() {
      return this.i18n.dow[this.selectedDay.thisWeekDay]
    },
    showDay() {
      return this.selectedDisplayMode === 'DAY'
    },
    showMonth() {
      return this.selectedDisplayMode === 'MONTH'
    },
    monthOfDay() {
      return this.selectedDay.thisMonth
    },
    daysOfMonth() {
      if (!this.showMonth) {
        return undefined
      }
      const events = []
      let counter = 0
      let event = this.allEvents[this.selectedDay.thisFirstMonday]
      while (!!event && counter < 42) {
        events.push(event)
        event = event.nextDay ? this.allEvents[event.nextDay] : undefined
        counter++
      }
      return events
    },
    eventsOfDay() {
      if (!this.showDay) {
        return undefined
      }
      return this.selectedDay.events || []
    },
    eventsByLocation() {
      if (!this.showDay) {
        return undefined
      }
      // already add default locations
      const locations = {
        'Saal1': { label: this.i18n.location.Saal1, events: [] },
        'Saal2': { label: this.i18n.location.Saal2, events: [] },
        'Saal3': { label: this.i18n.location.Saal3, events: [] },
      }
      console.log('eventsByLocation')
      // now add events and non-default locations
      let locationIds = Object.keys(locations)
      console.log('init', locationIds)
      this.eventsOfDay.forEach(event => {
        if (event.type !== 6) { // Holiday marker
          const locationId = event.ortid || 'others'
          const hasLocationId = locationId in locations
          console.log(locationId, 'present', hasLocationId, 'but', locationId in locations)
          if (!hasLocationId) {
            locations[locationId] = {
              label: event.ortid ? event.ort : this.i18n.location.others,
              events: [],
            }
            locationIds = Object.keys(locations)
            console.log('added', locationId)
          }
          locations[locationId].events.push(event)
          console.log('push', event.name)
        }
      })
      return locations
    },
    selectedLocationUpperLimit() {
      if (!this.showDay) {
        return undefined
      }
      return Object.keys(this.eventsByLocation).length - 1
    },
    sortedLocations() {
      if (!this.showDay) {
        return undefined
      }
      const locations = [...Object.keys(this.eventsByLocation)]
      locations.sort((locationA, locationB) => {
        const index = {
          'Saal1': 1,
          'Saal2': 2,
          'Saal3': 3,
          'others': 9,
        }
        const indexA = index[locationA] || 5
        const indexB = index[locationB] || 5
        return indexA - indexB
      })
      const repeatedLocations = locations.concat(locations)
      const displayLimit = 3
      return repeatedLocations.slice(this.selectedLocation, this.selectedLocation + displayLimit)
    },
    cssSideMenuButton() {
      return 'tw-flex-shrink-0 tw-flex-grow-0' +
        ' tw-w-20' +
        ' tw-p-4' +
        ' hover:tw-bg-rubinRot tw-text-gray-500 hover:tw-text-white' +
        ' tw-text-center' +
        ' tw-cursor-pointer' +
        ' tw-transition-colors tw-duration-300'
    }
  },

  mounted() {
    this.$nextTick(() => {
      jQuery(`#${TEMPLATE_ID} #rubin-loading-in-progress`).hide()
      jQuery(`#${TEMPLATE_ID} #rubin-loading-finished`).fadeIn(500)
      jQuery(`#${TEMPLATE_ID} #rubin-side-menu`).fadeIn(500)
    })
  },

  watch: {
    selectedDate() {
      this.selectedLocation = 0
    },
  },

  methods: {
    handleBackToToday() {
      this.selectedDate = this.today
    },
    handleSwitchToDate(day) {
      const isInMonth = day.thisMonth === this.monthOfDay
      if (isInMonth) {
        this.selectedDate = day.thisDay
        this.selectedDisplayMode = 'DAY'
      }
    },
    handleSwitchToDay() {
      this.selectedDisplayMode = 'DAY'
    },
    handleSwitchToMonth() {
      this.selectedDisplayMode = 'MONTH'
    },
    handleClickOnWeekDay(day, dayIndex) {
      const isInMonth = day.thisMonth === this.monthOfDay
      if (!isInMonth || this.magnifiedWeekDay === dayIndex) {
        this.magnifiedWeekDay = undefined
      } else {
        this.magnifiedWeekDay = dayIndex
      }
    },
    handlePrevDay() {
      this.selectedDate = this.selectedDay.prevDay || this.selectedDate
    },
    handleNextDay() {
      this.selectedDate = this.selectedDay.nextDay || this.selectedDate
    },
    handlePrevWeek() {
      this.selectedDate = this.selectedDay.prevWeek || this.selectedDate
    },
    handleNextWeek() {
      this.selectedDate = this.selectedDay.nextWeek || this.selectedDate
    },
    handlePrevMonth() {
      this.selectedDate = this.selectedDay.prevMonth || this.selectedDate
    },
    handleNextMonth() {
      this.selectedDate = this.selectedDay.nextMonth || this.selectedDate
    },
    handlePrevYear() {
      this.selectedDate = this.selectedDay.prevYear || this.selectedDate
    },
    handleNextYear() {
      this.selectedDate = this.selectedDay.nextYear || this.selectedDate
    },
    handlePrevLocation() {
      this.selectedLocation = this.selectedLocation - 1 < 0 ? this.selectedLocationUpperLimit : this.selectedLocation - 1
    },
    handleNextLocation() {
      this.selectedLocation = this.selectedLocation + 1 > this.selectedLocationUpperLimit ? 0 : this.selectedLocation + 1
    },
    showNextLocationIconAboveMD(position, index) {
      return index === position && this.selectedLocationUpperLimit > 2
    },
    showDayOfMonthNormal(dayIndex) {
      return this.magnifiedWeekDay === undefined || Math.abs(this.magnifiedWeekDay - dayIndex) === 1
    },
    showDayOfMonthMagnified(dayIndex) {
      return this.magnifiedWeekDay !== undefined && Math.abs(this.magnifiedWeekDay - dayIndex) === 0
    },
    showDayOfMonthHidden(dayIndex) {
      return this.magnifiedWeekDay !== undefined && Math.abs(this.magnifiedWeekDay - dayIndex) > 1
    },
    showEventDetails(event) {
      return !['Saal 1', 'Saal 2', 'Saal 3'].includes(event.name)
    },
    cssCalendarWeek(dayIndex) {
      const isMonday = dayIndex % 7 === 0
      if (this.magnifiedWeekDay !== undefined) {
        return this.showDayOfMonthMagnified(dayIndex) ? '' : 'tw-hidden'
      } else if (isMonday) {
        return 'tw-hidden md:tw-block'
      } else {
        return 'tw-hidden'
      }
    },
  },
})
</script>
