<div id="rubin-widget-mgvo-calendar-mini-<?php echo $args['id']; ?>" class="rubin-widget">
  <div class="rubin-widget-container bordered tw-flex tw-flex-col tw-justify-center tw-content-center">
    <?php
      echo '<div class="mgvo-calendar">';
      echo '  <div class="mgvo-month-slider" :style="{\'left\': (offset * -100) + \'%\'}">';
      for ($m=0;$m<36;$m++) {
        $day = strtotime($args['months'][$m]['year'].'-'.$args['months'][$m]['month'].'-01');
        if (date('w', $day) !== 1) {
          $day = strtotime('last Monday', $day);
        }
        echo '<div class="mgvo-month">';
        echo '  <div class="tw-relative tw-text-center tw-pt-8 tw-px-8"><span class="tw-text-4xl tw-font-bold">'.$args['months'][$m]['year'].'</span>';
          rubin_svg(get_template_directory().'/assets/img/chevron.svg', array(
            '@click' => 'offset = Math.max(0, offset - 12)',
            'class' => 'tw-block tw-absolute tw-transform tw-rotate-90 tw-left-8 tw-top-10 tw-w-4 tw-h-4 tw-text-gray-300 tw-transition-colors tw-duration-300 tw-cursor-pointer'.($m === 0 ? ' tw-hidden' : ' hover:tw-text-rubinRot')
          ));
          rubin_svg(get_template_directory().'/assets/img/chevron.svg', array(
            '@click' => 'offset = Math.min(35, offset + 12)',
            'class' => 'tw-block tw-absolute tw-transform tw--rotate-90 tw-right-8 tw-top-10 tw-w-4 tw-h-4 tw-text-gray-300 tw-transition-colors tw-duration-300 tw-cursor-pointer'.($m === 35 ? ' tw-hidden' : ' hover:tw-text-rubinRot')
          ));
        echo '  </div>';
        echo '  <div class="tw-relative tw-font-bold tw-text-center tw-px-8 tw-pb-4"><span>'.$args['months'][$m]['name'].'</span>';
          rubin_svg(get_template_directory().'/assets/img/chevron.svg', array(
            '@click' => 'offset = Math.max(0, offset - 1)',
            'class' => 'tw-block tw-absolute tw-transform tw-rotate-90 tw-left-8 tw-top-2 tw-w-4 tw-h-4 tw-text-gray-300 tw-transition-colors tw-duration-300 tw-cursor-pointer'.($m === 0 ? ' tw-hidden' : ' hover:tw-text-rubinRot')
          ));
          rubin_svg(get_template_directory().'/assets/img/chevron.svg', array(
            '@click' => 'offset = Math.min(35, offset + 1)',
            'class' => 'tw-block tw-absolute tw-transform tw--rotate-90 tw-right-8 tw-top-2 tw-w-4 tw-h-4 tw-text-gray-300 tw-transition-colors tw-duration-300 tw-cursor-pointer'.($m === 35 ? ' tw-hidden' : ' hover:tw-text-rubinRot')
          ));
        echo '  </div>';
        for ($w=1;$w<=6;$w++) {
          echo '<div class="mgvo-week tw-px-8">';
          for ($d=1;$d<=7;$d++) {
            $isToday = date('Y-m-d', $day) === $args['today'];  
            $isInMonth = date('m', $day) === $args['months'][$m]['month'];
            $key = date('Y-m-d', $day);
            $events = array_key_exists($key, $args['allEvents']) ? $args['allEvents'][$key] : array();
            $size = is_array($events) ? sizeof($events) : 0;
            $hasEvent = $isInMonth && $size === 1;
            $hasEvents = $isInMonth && $size > 1;
            $style = $hasEvent ? 'border-color: '.$events[0]['color'].';' : '';
            $class = "";
            $class .= $hasEvents ? '  has-events' : '';
            $class .= $hasEvent ? '  has-event' : '';
            $class .= $d === 7 ? ' is-sunday' : '';
            $class .= $isToday ? ' is-today' : '';
            $class .= $isInMonth ? '' : ' outside-month';
            echo ''
              .'<div'
              .'  @click="handleOpenDetailsClick(\''.$key.'\')"'
              .'  class="mgvo-day'.$class.'"'
              .'  style="'.$style.'"'
              .'><span>'.date('d', $day).'</span>';
            if ($hasEvent || $hasEvents) {
              echo ''
                .'<div'
                .'  :id="\'details-'.$key.'\'"'
                .'  class="'
                .'    tw-absolute'
                .'    tw-top-0'
                .'    tw-left-0'
                .'    tw-flex'
                .'    tw-flex-row'
                .'    tw-flex-nowrap'
                .'    tw-w-full'
                .'    tw-h-full'
                .'    tw-cursor-default'
                .'  "'
                .'  style="display: none;"'
                .'>';
              echo '<div @click.stop="handleCloseDetailsClick" class="tw-absolute center-v tw-left-2 tw-w-8 tw-h-8 tw-cursor-pointer">';
                rubin_svg(get_template_directory().'/assets/img/chevron.svg', array(
                  'class' => ''
                    .' tw-transform'
                    .' tw-rotate-90'
                    .' tw-block'
                    .' tw-w-full'
                    .' tw-h-full'
                    .' tw-text-gray-300'
                    .' tw-transition-colors'
                    .' tw-duration-300'
                    .' hover:tw-text-rubinRot'
                ));
              echo '</div>';
              echo ''
                .'<div'
                .'  class="'
                .'    tw-flex'
                .'    tw-flex-col'
                .'    tw-flex-nowrap'
                .'    tw-w-full'
                .'    tw-h-full'
                .'    tw-py-8'
                .'    tw-bg-rubinRot-shadow'
                .'    tw-whitespace-normal'
                .'    tw-cursor-default'
                .'    tw-overflow-hidden'
                .'    tw-overflow-y-auto'
                .'  "'
                .'>';
              foreach ($events as $event) {
                echo '<div class="tw-text-gray-800 tw-pb-4 tw-pl-16 tw-pr-8 tw-mb-4">';
                if ($event['zeitraum'] !== false) {
                  echo '  <div class="tw-text-left tw-font-dejaVuSlim tw-text-sm">'.$event['zeitraum'].'</div>';
                }
                echo '  <div class="tw-text-left tw-font-dejaVuSlim tw-font-bold">'.$event['tage'].'</div>';
                echo '  <div class="tw-text-left tw-font-dejaVuSlim">'.$event['name'].'</div>';
                if ($event['trainer'] !== false) {
                  echo '  <div class="tw-text-left tw-font-dejaVuSlim tw-text-sm">'.__('mit ', RUBIN_TEXT_DOMAIN).$event['trainer'].'</div>';
                }
                echo '</div>';
              }
              echo '  </div>';
              echo '</div>';
            }
            echo '</div>';
            $day = strtotime('+ 1 day', $day);
          }
          echo '</div>';
        }
        echo '  <div class="tw-pb-8">';
        echo '    <div v-if="offset!='.$args['offset'].'" @click="offset='.$args['offset'].'" class="tw-px-8 tw-text-center tw-font-dejaVuSlim tw-text-sm tw-cursor-pointer tw-pt-4 tw-text-gray-300 hover:tw-text-rubinRot tw-transition-colors tw-duration-300">'.__('[ zurück zum aktuellen Monat ]', RUBIN_TEXT_DOMAIN).'</div>';
        echo '  </div>';
        echo '</div>';
      }
      echo '  </div>';
      echo '</div>';
    ?>
  </div>
</div>

<script type="application/javascript">
const rubinWidgetMgvoCalendarMini = new Vue({
  el: '#rubin-widget-mgvo-calendar-mini-<?php echo $args['id']; ?>',
  name: 'MGVO Calendar Widget (mini layout)',

  data() {
    return {
      offset: <?php echo $args['offset']; ?>,
      open: false,
    }
  },

  methods: {
    handleOpenDetailsClick(id) {
      this.open = id
      jQuery('#details-' + this.open).fadeIn(300)
    },

    handleCloseDetailsClick(id) {
      jQuery('#details-' + this.open).fadeOut(300)
      this.open = false
    }
  },
})
</script>
