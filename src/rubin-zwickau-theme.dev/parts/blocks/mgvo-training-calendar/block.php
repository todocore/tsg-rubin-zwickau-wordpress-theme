<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_render_block_mgvo_training_calendar($block_attributes, $content) {
  if (is_admin()) {
    return '';
  };

  global $mgvoService;

  $defaultIgnore = get_theme_mod('mgvo-ignore-setting');
  $defaultIgnore = isset($defaultIgnore) ? $defaultIgnore : '';

  $layout = $block_attributes['layout'];
  $calNr = $block_attributes['calNr'];
  $includeOpen = $block_attributes['includeOpen'];
  $includeClosed = $block_attributes['includeClosed'];
  $includeRefusals = $block_attributes['includeRefusals'];
  $includeTrainings = $includeOpen || $includeClosed;
  $includeHolidays = $block_attributes['includeHolidays'];
  $forceHolidays = $block_attributes['forceHolidays'];
  $ignoreText = $block_attributes['ignoreText'];

  $instance = array(
    'layout' => isset($layout) ? $layout : 'COMPACT',
    'calNr' => $calNr,
    'includeOpen' => $includeOpen,
    'includeClosed' => $includeClosed,
    'includeRefusals' => $includeRefusals,
    'forceHolidays' => $forceHolidays,
    'ignore' => isset($ignoreText) ? $ignoreText : $defaultIgnore
  );

  ob_start();
    if (!isset($instance['calNr'])) {
      get_template_part('/parts/widget/message', null , array(
        'size' => '',
        'container' => 'bordered',
        'class' => 'tw-text-rubinRot',
        'message' => __('Bitte die Nummer des Kalenders setzen!', RUBIN_TEXT_DOMAIN)
      ));
      return ob_get_clean();
    }

    $layoutMap = array(
      'COMPACT' => '/parts/blocks/mgvo-training-calendar/layout-compact',
      'LIST' => '/parts/blocks/mgvo-training-calendar/layout-list',
      'MINI' => '/parts/blocks/mgvo-training-calendar/layout-mini'
    );
    $layout = $layoutMap['COMPACT'];
    if (array_key_exists($instance['layout'], $layoutMap)) {
      $layout = $layoutMap[$instance['layout']];
    }
    $isCompactLayout = $layout === $layoutMap['COMPACT'];
    $isListLayout = $layout === $layoutMap['LIST'];

    $now = strtotime('now');

    $thisYear = $now;
    $prevYear = strtotime('-1 year', $now);
    $nextYear = strtotime('+1 year', $now);

    $firstDay = strtotime('last Monday', strtotime(date('Y', $prevYear).'-01-01'));
    $lastDay = strtotime('next Sunday', strtotime(date('Y', $nextYear).'-12-31'));

    $prevYearEvents = $isListLayout ? [] : $mgvoService->fetchEventsByYear($calNr, $prevYear);
    $thisYearEvents = $mgvoService->fetchEventsByYear($calNr, $thisYear);
    $nextYearEvents = $mgvoService->fetchEventsByYear($calNr, $nextYear);
    $trainings = [];
    if ($includeTrainings) {
      $refusals = $mgvoService->fetchRefusals();
      $refusals = $mgvoService->sanitizeRefusals($refusals, true, []);

      $trainings = $mgvoService->fetchTrainingGroups();
      $trainings = $mgvoService->convertTrainingGroupsToEvents($trainings, $refusals, $instance);
    }

    $allEvents = array();
    $allEvents = $mgvoService->mergeEvents($allEvents, $mgvoService->sanitizeEventsByDate($trainings));
    $allEvents = $mgvoService->mergeEvents($allEvents, $mgvoService->sanitizeEventsByDate($prevYearEvents));
    $allEvents = $mgvoService->mergeEvents($allEvents, $mgvoService->sanitizeEventsByDate($thisYearEvents));
    $allEvents = $mgvoService->mergeEvents($allEvents, $mgvoService->sanitizeEventsByDate($nextYearEvents));
    $allEvents = $mgvoService->sortEvents($allEvents);

    if (!$includeHolidays) {
      $allEvents = $mgvoService->cleanTrainingsFromHolidays($allEvents, $instance);
    }

    $offset = 12 + (intval(date('n', $thisYear)) - 1);
    if ($isListLayout) {
      $allEvents = $mgvoService->convertEventsToListLayout($allEvents, 4);
      $offset = 0;
    } else if ($isCompactLayout) {
      $allEvents = $mgvoService->convertEventsToCompactLayout($firstDay, $lastDay, $allEvents);
    }

    get_template_part($layout, null, array(
      'id' => uniqid(),
      'today' => date('Y-m-d', $now),
      'allEvents' => $allEvents,
      'i18n' => [
        'event' => [
          'with' => __('mit', RUBIN_TEXT_DOMAIN),
          'backToToday' => __('Heute', RUBIN_TEXT_DOMAIN),
          'switchToDayView' => __('Tag', RUBIN_TEXT_DOMAIN),
          'switchToMonthView' => __('Monat', RUBIN_TEXT_DOMAIN),
          'calendarWeek' => __('KW', RUBIN_TEXT_DOMAIN),
        ],
        'dow' => [
          '1' => __('Montag', RUBIN_TEXT_DOMAIN),
          '2' => __('Dienstag', RUBIN_TEXT_DOMAIN),
          '3' => __('Mittwoch', RUBIN_TEXT_DOMAIN),
          '4' => __('Donnerstag', RUBIN_TEXT_DOMAIN),
          '5' => __('Freitag', RUBIN_TEXT_DOMAIN),
          '6' => __('Samstag', RUBIN_TEXT_DOMAIN),
          '7' => __('Sonntag', RUBIN_TEXT_DOMAIN),
        ],
        'dowShort' => [
          '1' => __('Mo', RUBIN_TEXT_DOMAIN),
          '2' => __('Di', RUBIN_TEXT_DOMAIN),
          '3' => __('Mi', RUBIN_TEXT_DOMAIN),
          '4' => __('Do', RUBIN_TEXT_DOMAIN),
          '5' => __('Fr', RUBIN_TEXT_DOMAIN),
          '6' => __('Sa', RUBIN_TEXT_DOMAIN),
          '7' => __('So', RUBIN_TEXT_DOMAIN),
        ],
        'hod' => [
          '08:00',
          '08:30',
          '09:00',
          '09:30',
          '10:00',
          '10:30',
          '11:00',
          '11:30',
          '12:00',
          '12:30',
          '13:00',
          '13:30',
          '14:00',
          '14:30',
          '15:00',
          '15:30',
          '16:00',
          '16:30',
          '17:00',
          '17:30',
          '18:00',
          '18:30',
          '19:00',
          '19:30',
          '20:00',
          '20:30',
          '21:00',
          '21:30',
          '22:00',
        ],
        'location'=> [
          'Saal1' => __('Saal 1', RUBIN_TEXT_DOMAIN),
          'Saal2' => __('Saal 2', RUBIN_TEXT_DOMAIN),
          'Saal3' => __('Saal 3', RUBIN_TEXT_DOMAIN),
          'others' => __('Sonstige', RUBIN_TEXT_DOMAIN),
          'count' => __('von', RUBIN_TEXT_DOMAIN),
          'locations' => __('Locations', RUBIN_TEXT_DOMAIN),
        ],
        'moy' => [
          '01' => __('Januar', RUBIN_TEXT_DOMAIN),
          '02' => __('Februar', RUBIN_TEXT_DOMAIN),
          '03' => __('März', RUBIN_TEXT_DOMAIN),
          '04' => __('April', RUBIN_TEXT_DOMAIN),
          '05' => __('Mai', RUBIN_TEXT_DOMAIN),
          '06' => __('Juni', RUBIN_TEXT_DOMAIN),
          '07' => __('Juli', RUBIN_TEXT_DOMAIN),
          '08' => __('August', RUBIN_TEXT_DOMAIN),
          '09' => __('September', RUBIN_TEXT_DOMAIN),
          '10' => __('Oktober', RUBIN_TEXT_DOMAIN),
          '11' => __('November', RUBIN_TEXT_DOMAIN),
          '12' => __('Dezember', RUBIN_TEXT_DOMAIN),
        ],

      ],
    ));
  return ob_get_clean();
}

function rubin_register_block_mgvo_training_calendar() {
  register_block_type('rubin/mgvo-training-calendar', array(
    'api_version' => 2,
    'render_callback' => 'rubin_render_block_mgvo_training_calendar',
  ));  
}
add_action('init', 'rubin_register_block_mgvo_training_calendar');