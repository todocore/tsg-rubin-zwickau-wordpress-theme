<?php include_once get_template_directory().'/parts/forms/rubin-checkbox.vue.php'; ?>
<?php include_once get_template_directory().'/parts/forms/rubin-dropdown.vue.php'; ?>
<?php include_once get_template_directory().'/parts/forms/rubin-inpot.vue.php'; ?>
<?php include_once get_template_directory().'/parts/forms/rubin-input.vue.php'; ?>
<?php include_once get_template_directory().'/parts/forms/rubin-submit.vue.php'; ?>
<?php include_once get_template_directory().'/parts/forms/rubin-textarea.vue.php'; ?>
<?php include_once get_template_directory().'/parts/blocks/mgvo-training-wizard/contact.vue.php'; ?>
<?php include_once get_template_directory().'/parts/blocks/mgvo-training-wizard/groups.vue.php'; ?>
<?php include_once get_template_directory().'/parts/blocks/mgvo-training-wizard/weekdays.vue.php'; ?>

<div id="rubin-widget-mgvo-training-<?php echo $args['id']; ?>" class="rubin-widget seamless">
  <div class="rubin-widget-container bordered tw-flex tw-flex-col tw-justify-center tw-content-center tw-pb-8">
    <div id="rubin-widget-mgvo-training-selection">
      <div class="tw-text-center tw-text-2xl md:tw-text-3xl tw-pt-8 tw-px-8"><?php echo $args['title']; ?></div>
      <div class="tw-pt-8 tw-px-8">
        <select
          @change="handleCategorySelection"
          :disabled="disableCategory"
          :class="{
            'tw-cursor-pointer': !disableCategory,
            'tw-cursor-default': !!disableCategory,
            'tw-text-rubinRot': !!disableCategory,
          }"
          class="
            tw-w-full
            tw-border-2 tw-border-solid tw-border-gray-900 tw-rounded-lg tw-shadow
            tw-p-4 tw-px-8
            tw-bg-rubinRot-darkShadow
            tw-text-dejaVuSlim tw-text-base md:tw-text-2xl tw-text-center
            tw-transition-colors tw-duration-300
        ">
          <option
            :selected="category === ''"
            class="tw-text-gray-600"
            value=""
          ><?php echo __('Für was interessierst Du Dich?', RUBIN_TEXT_DOMAIN); ?></option>
          <option
            v-for="(cName, cId) in server.byId"
            :key="cId"
            :selected="category === cId"
            :value="cId"
          >{{ cName }}</option>
        </select>
      </div>
      <rubin-weekday-selection
        @click="handleDayOfWeekSelection"
        :category="category"
        :day-of-week="dayOfWeek"
        :week="server.week"
        id="mgvo-training-overview"
        style="display: none;"
      >
        <rubin-training-groups
          @open="handleGroupOpening"
          @select="handleGroupSelection"
          :groups="groups"
          :opened="openGroup"
          :show-info-link="!disableCategory"
        />
      </rubin-weekday-selection>
    </div>

    <rubin-training-contact
      @close="handleGroupSelection({})"
      :group="formGroup"
      id="rubin-widget-mgvo-training-contact"
      style="display: none;"
    />
  </div>
</div>

<script type="application/javascript">
const rubinWidgetMgvoTraining = new Vue({
  el: '#rubin-widget-mgvo-training-<?php echo $args['id']; ?>',
  name: 'MGVO Training Widget',

  components: {
    rubinCheckbox,
    rubinDropdown,
    rubinInput,
    rubinSubmit,
    rubinTextarea,
    rubinTrainingContact,
    rubinTrainingGroups,
    rubinWeekdaySelection,
  },

  data() {
    return {
      category: '<?php echo $args['instance']['limitCategory'] == RUBIN_TRAINING_ASK ? '' : $args['instance']['limitCategory']; ?>',
      dayOfWeek: '',
      openGroup: '',
      formGroup: {},
      server: <?php echo json_encode($args); ?>,
      disableCategory: <?php echo $args['instance']['limitCategory'] == RUBIN_TRAINING_ASK ? 'false' : 'true'; ?>,
    }
  },

  mounted() {
    if (!!this.category) {
      this.handleCategorySelection({
        target: {
          value: this.category,
        }
      });
    }
  },

  computed: {
    groups() {
      const weekday = this.server.week[this.dayOfWeek] || {groups: []}
      return weekday.groups[this.category] || []
    },
  },

  methods: {
    handleCategorySelection(e) {
      this.category = e.target.value
      if (this.category === '') {
        jQuery('#mgvo-training-overview').fadeOut(500)
      } else {
        jQuery('#mgvo-training-overview').fadeIn(500)
      }
      this.handleDayOfWeekSelection('')
    },

    handleDayOfWeekSelection(dId) {
      this.dayOfWeek = dId
      this.handleGroupOpening('', 0)
    },

    handleGroupOpening(gId, timeout=300) {
      if (gId === '' || this.openGroup === gId) {
        jQuery('#'+this.openGroup+'-data').slideUp(timeout)
        this.openGroup = ''
      } else {
        jQuery('#'+this.openGroup+'-data').slideUp(timeout)
        this.openGroup = gId
        jQuery('#'+this.openGroup+'-data').slideDown(timeout)
      }
    },

    handleGroupSelection(group) {
      this.formGroup = group
      if (Object.keys(group).length === 0) {
        jQuery('#rubin-widget-mgvo-training-selection').slideDown(300)
        jQuery('#rubin-widget-mgvo-training-contact').slideUp(300)
        setTimeout(function() {
          jQuery('html, body').animate({scrollTop: jQuery('#rubin-widget-mgvo-training-selection').offset().top - 150 }, 'slow')
        }, 350)
      } else {
        jQuery('#rubin-widget-mgvo-training-selection').slideUp(300)
        jQuery('#rubin-widget-mgvo-training-contact').slideDown(300)
        setTimeout(function() {
          jQuery('html, body').animate({scrollTop: jQuery('#rubin-widget-mgvo-training-contact').offset().top - 150 }, 'slow')
        }, 350)
      }
    },
  },
})
</script>