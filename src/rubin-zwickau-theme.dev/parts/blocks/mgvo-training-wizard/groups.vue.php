<script type="text/x-template" id="rubin-template-mgvo-training-groups">
  <div
    class="
      tw-w-full
      tw-flex-col tw-flex-nowrap
      tw-border-2 tw-border-solid tw-border-gray-900
      tw-bg-rubinRot-darkShadow
      tw-font-dejaVuSlim
      tw-text-rubinRot
    "
  >
    <div
      v-if="groups.length === 0"
      class="
        tw-p-8
        tw-text-center tw-text-gray-600
      "
    >
      <span class="tw-font-bold">
        <?php echo __('An den hervorgehobenen Wochentagen könntest Du dafür bei uns trainieren.', RUBIN_TEXT_DOMAIN); ?>
      </span>
      <br/>
      <span>
        <?php echo __('Bitte wähle den Tag, der Dir am besten passen würde!', RUBIN_TEXT_DOMAIN); ?>
      </span>
    </div>
    <div
      v-else
      v-for="group in sortedGroups"
      :key="group.id"
      class="
        tw-flex tw-flex-col tw-flex-nowrap
        tw-cursor-pointer
        even:tw-bg-rubinRot-darkerShadow
      "
    >
      <div
        @click="handleClickToOpen(group.id)"
        class="
          tw-flex-auto tw-flex tw-flex-col md:tw-flex-row tw-flex-nowrap
          tw-border-0 tw-border-solid tw-border-rubinRot
          tw-py-4 tw-px-8
          tw-transition-all tw-duration-300
          tw-group
        "
      >
        <div
          :class="{
            'tw-text-gray-400': fadeToGray(group.id),
            'group-hover:tw-text-rubinRot': fadeToGray(group.id),
          }"
          class="
            tw-flex-auto tw-flex tw-flex-col md:tw-flex-row tw-flex-nowrap
            tw-transition-all tw-duration-300
          "
        >
          <div class="
            md:tw-w-1/3
            tw-font-bold md:tw-font-normal
            tw-text-xl
            tw-text-left
          ">{{ group.zeitraum }}</div>

          <div class="
            md:tw-w-1/3
            tw-text-xl
            tw-text-left
          ">{{ group.gName }}</div>

          <div class="
            md:tw-w-1/3
            tw-text-base
            tw-text-left
          ">{{ group.turnus }}</div>
        </div>
        <div class="tw-flex-auto md:tw-flex-grow-0 md:tw-flex-shrink-0 md:tw-w-10">
          <?php rubin_svg(get_template_directory().'/assets/img/chevron.svg', array(
            ':class' => '{\'tw-rotate-0\': opened != group.id, \'tw-rotate-180\': opened === group.id,}',
            'class' => 'tw-block tw-transform tw-w-8 tw-h-8 tw-mx-auto tw-text-gray-400 group-hover:tw-text-rubinRot tw-transition-all tw-duration-300 tw-cursor-pointer',
          )); ?>
        </div>
      </div>

      <div
        :id="group.id + '-data'"
        class="
          tw-w-full
          tw-flex tw-flex-col md:tw-flex-row tw-flex-wrap
          tw-px-8 tw-py-4
          tw-text-lg md:tw-text-xl tw-text-left tw-text-rubinRot
          tw-cursor-default
        "
        style="display: none;"
      >
        <div class="md:tw-w-1/3 tw-font-dejaVuSlim">
          <strong><?php echo __('nächste Termine:', RUBIN_TEXT_DOMAIN); ?></strong>
          <div
            v-for="date in group.nextdat"
            :class="{
              'tw-text-gray-400': !!date.refused,
            }"
          >
            <span :class="{'tw-line-through': !!date.refused}">{{ date.uiDate }}</span>
            <span v-if="!!date.refused"><?php echo __('(abgesagt)', RUBIN_TEXT_DOMAIN); ?></span>
          </div>
        </div>
        <div class="md:tw-w-1/3 tw-font-dejaVuSlim tw-mt-4 md:tw-mt-0">
          <strong><?php echo __('Ort:', RUBIN_TEXT_DOMAIN); ?></strong>
          <br/>
          <span>{{ group.oName }}</span>
        </div>
        <div class="md:tw-w-1/3 tw-font-dejaVuSlim tw-mt-4 md:tw-mt-0">
          <a
            v-if="showInfoLink"
            role="button"
            class="
              tw-block
              tw-w-full
              tw-border-2 tw-border-solid tw-border-gray-900 tw-rounded-lg
              tw-shadow
              tw-p-4
              tw-bg-rubinRot-shadow
              tw-text-center tw-text-base tw-text-gray-900 hover:tw-text-rubinRot
              tw-cursor-pointer
              tw-transition-colors tw-duration-300
            "
            :href="'/trainingskategorie-' + group.cId.toLowerCase()"
          ><?php echo __('Weitere Informationen', RUBIN_TEXT_DOMAIN); ?></a>
          <div v-else class="tw-text-base">
            {{ group.description }}
          </div>
          <button
            @click="handleClickToSelect(group)"
            role="button"
            class="
              tw-w-full
              tw-mt-4
              tw-border-2 tw-border-solid tw-border-gray-900 tw-rounded-lg
              tw-shadow
              tw-p-4
              tw-bg-rubinRot-shadow
              tw-text-base tw-text-gray-900 hover:tw-text-rubinRot
              tw-cursor-pointer
              tw-transition-colors tw-duration-300
            "
          >
            <span v-if="group.isFull">
              <?php echo __('In Warteliste eintragen', RUBIN_TEXT_DOMAIN); ?>
            </span>
            <span v-else>
              <?php echo __('Jetzt Probetraining vereinbaren', RUBIN_TEXT_DOMAIN); ?>
            </span>
          </button>
        </div>
        <div v-if="group.isFull" class="tw-hidden md:tw-block tw-w-1/3 tw-mt-4">&nbsp;</div>
        <div v-if="group.isFull" class="
          tw-relative
          tw-w-full md:tw-w-2/3
          tw-mt-4
          tw-border tw-border-solid tw-border-rubinRot tw-rounded-lg tw-shadow
          tw-p-4
          tw-bg-rubinRot-shadow
          tw-text-base
        ">
          <?php rubin_svg(get_template_directory().'/assets/img/queue.svg', array('class' => 'tw-inline-block tw-h-16 tw-mr-4 tw-fill-current tw-text-rubinRot tw-float-left')); ?>
          <?php echo __('Diese Trainingsgruppe ist derzeit voll belegt und kann keine neuen Interessenten aufnehmen.', RUBIN_TEXT_DOMAIN); ?>
          <?php echo __('Du kannst Dich gerne auf unserer Warteliste eintragen oder Du wählst eine der andere Trainingsgruppen.', RUBIN_TEXT_DOMAIN); ?>
        </div>
        <div class="tw-hidden md:tw-block md:tw-flex-grow-0 md:tw-flex-shrink-0 md:tw-w-10">
          &nbsp;
        </div>
      </div>
    </div>
  </div>
</script>

<script type="application/javascript">
const rubinTrainingGroups = Vue.component('rubin-training-groups', {
  template: '#rubin-template-mgvo-training-groups',

  props: {
    groups: {
      type: Array,
      required: true,
    },
    opened: {
      type: String,
      required: true,
    },
    showInfoLink: {
      type: Boolean,
      default: true,
    }
  },

  computed: {
    sortedGroups() {
      return Object.values(this.groups || {}).sort(function(a,b) {
        return a.zeitraum.localeCompare(b.zeitraum)
      })
    },
  },

  methods: {
    handleClickToOpen(id) {
      this.$emit('open', id)
    },

    handleClickToSelect(group) {
      this.$emit('select', group)
    },

    fadeToGray(id) {
      return !!this.opened && this.opened != id
    },
  },
})
</script>
