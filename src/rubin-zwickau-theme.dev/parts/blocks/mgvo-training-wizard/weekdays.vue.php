<script type="text/x-template" id="rubin-template-mgvo-training-weekdays">
  <div class="tw-flex tw-flex-row md:tw-flex-col tw-flex-nowrap tw-pt-8 tw-px-8">
    <div class="tw-flex tw-flex-col md:tw-flex-row tw-flex-nowrap tw--mr-2px md:tw-mr-0 md:tw--mb-2px">
      <div
        v-for="day in ['Mo','Di','Mi','Do','Fr','Sa','So']"
        @click="handleClick(day)"
        :key="category + day"
        :class="{
          'tw-border-gray-300': !hasGroups(day),
          'rubin-border-r-gray-900': !hasGroups(day),
          'md:rubin-border-r-not-gray': !hasGroups(day),
          'md:rubin-border-b-gray-900': !hasGroups(day),
          'tw-text-gray-300': !hasGroups(day),
          'tw-cursor-default': !hasGroups(day),

          'tw-border-gray-900': hasGroups(day),
          'tw-text-gray-900': hasGroups(day),
          'tw-cursor-pointer': hasGroups(day),

          'hover:tw-bg-rubinRot-darkShadow': and(hasGroups(day), !isSameDay(day)),
          'hover:tw-text-rubinRot': and(hasGroups(day), !isSameDay(day)),

          'rubin-border-r-rubinRot-darkShadow': and(hasGroups(day), isSameDay(day)),
          'md:rubin-border-r-not-rubinRot': and(hasGroups(day), isSameDay(day)),
          'md:rubin-border-b-rubinRot-darkShadow': and(hasGroups(day), isSameDay(day)),
          'tw-bg-rubinRot-darkShadow': and(hasGroups(day), isSameDay(day)),
          'tw-text-rubinRot': and(hasGroups(day), isSameDay(day)),
          'tw-z-10': and(hasGroups(day), isSameDay(day)),
        }"
        class="
          tw-flex-grow-0 tw-flex-shrink-0 md:tw-flex-auto
          tw-border-2 tw-border-solid tw-rounded-l-xl tw-rounded-r-none md:tw-rounded-t-xl md:tw-rounded-b-none
          tw-px-4 md:tw-px-0 tw-py-4
          tw-font-dejaVuSlim
          tw-text-xl tw-text-center
          tw-transition-colors tw-duration-300
        "
      >
        <span class="tw-inline md:tw-hidden">{{ day }}</span>
        <span class="tw-hidden md:tw-inline">{{ week[day].name }}</span>
      </div>
    </div>
    <slot></slot>
  </div>
</script>

<script type="application/javascript">
const rubinWeekdaySelection = Vue.component('rubin-weekday-selection', {
  template: '#rubin-template-mgvo-training-weekdays',

  props: {
    category: {
      type: String,
      required: true,
    },

    dayOfWeek: {
      type: String,
      required: true,
    },

    week: {
      type: Object,
      default: function() {return {}},
    },
  },

  methods: {
    hasGroups(day) {
      return !!this.week[day] && !!this.week[day]['groups'] && !!this.week[day]['groups'][this.category]
    },

    isSameDay(day) {
      return this.dayOfWeek === day
    },

    and(a, b) {
      return a && b
    },

    handleClick(day) {
      if (this.hasGroups(day)) {
        this.$emit('click', day)
      }
    },
  },
})
</script>