<script type="text/x-template" id="rubin-template-mgvo-training-contact">
  <div class="
      rubin-training-form
      tw-relative
      tw-px-8 tw-py-4
      tw-text-lg md:tw-text-xl tw-text-left tw-text-rubinRot
  ">
    <?php rubin_svg(get_template_directory().'/assets/img/close.svg', array(
      '@click' => 'handleClickToClose',
      'class' => 'tw-absolute tw-top-0 tw-right-0 tw-block tw-w-4 tw-h-4 tw-mt-5 tw-mr-5 md:tw-mr-11 tw-text-gray-400 hover:tw-text-rubinRot tw-transition-all tw-duration-300 tw-cursor-pointer tw-z-10',
    )); ?>
    <div class="
      tw-relative
      tw-w-full
      tw-cursor-default
    ">
      <div class="
        tw-font-bold
        tw-text-2xl
        tw-mb-4
      ">
        <span v-if="group.isFull"><?php echo __('Eintragung in Warteliste', RUBIN_TEXT_DOMAIN); ?></span>
        <span v-else><?php echo __('Terminanfrage', RUBIN_TEXT_DOMAIN); ?></span>
      </div>

      <div class="tw-text-base tw-text-right tw-pb-2">
        <?php echo __('Felder, die mit (*) markiert sind, sind Pflichtfelder.', RUBIN_TEXT_DOMAIN); ?>
      </div>

      <div class="
        tw-mb-8
        tw-p-8
        tw-rounded-lg tw-shadow-md
        tw-bg-rubinRot-darkShadow
      ">
        <div>
          <span class="tw-font-bold">{{ group.cName }} &ndash; {{ group.gName }}</span>
          <br/>
          <span>{{ group.dName }}, </span>
          <span>{{ group.oName }}</span>
          <span>{{ group.zeitraum }}</span>
          <span>({{ group.turnus }})</span>
        </div>
        <div v-if="!group.isFull" class="tw-text-base">
          <span><?php echo __('nächste Termine: ', RUBIN_TEXT_DOMAIN); ?></span>
          <span v-for="(date, idx) in unrefused"><span v-if="!date.refused">{{ (idx === 0 ? '' : ', ') + date.uiDate }}</span></span>
        </div>
      </div>

      <rubin-input
        v-model="form.email.value"
        @error="form.email.readyToSubmit=false"
        @ready="form.email.readyToSubmit=true"
        :mandatory="form.email.mandatory"
        label="<?php echo __('Unter welche Emailadresse können wir Dich erreichen?', RUBIN_TEXT_DOMAIN); ?>"
        pattern="^[^\s]+@[^\s]+\.[^\s]+$"
      />

      <rubin-input
        v-model="form.phone.value"
        @error="form.phone.readyToSubmit=false"
        @ready="form.phone.readyToSubmit=true"
        :mandatory="form.phone.mandatory"
        label="<?php echo __('Für schnelle Rückfragen wäre Deine Telefonnummer hilfreich.', RUBIN_TEXT_DOMAIN); ?>"
      />

      <rubin-input
        v-model="form.name.value"
        @error="form.name.readyToSubmit=false"
        @ready="form.name.readyToSubmit=true"
        :mandatory="form.name.mandatory"
        label="<?php echo __('Wie lautet Dein Name?', RUBIN_TEXT_DOMAIN); ?>"
      />

      <rubin-dropdown
        v-model="form.who.value"
        @error="form.who.readyToSubmit=false"
        @ready="form.who.readyToSubmit=true"
        :mandatory="form.who.mandatory"
        :options="[
          {value: 'me', name: '<?php echo __('Für mich selbst.', RUBIN_TEXT_DOMAIN); ?>'},
          {value: 'child', name: '<?php echo __('Für mein Kind.', RUBIN_TEXT_DOMAIN); ?>'},
          {value: 'other', name: '<?php echo __('Für eine andere Person (Gutschein).', RUBIN_TEXT_DOMAIN); ?>'},
        ]"
        label="<?php echo __('Für wen stellst Du diese Anfrage?', RUBIN_TEXT_DOMAIN); ?>"
      />

      <rubin-checkbox
        v-model="form.member.value"
        @error="form.member.readyToSubmit=false"
        @ready="form.member.readyToSubmit=true"
        :label="msg.member.ask[form.who.value]"
        :name="msg.member.yes[form.who.value]"
        :mandatory="form.member.mandatory"
      />

      <rubin-input
        v-if="askForOtherName"
        v-model="form.otherName.value"
        @error="form.otherName.readyToSubmit=false"
        @ready="form.otherName.readyToSubmit=true"
        :mandatory="form.otherName.mandatory"
        :label="msg.askOtherName[form.who.value]"
      />

      <rubin-input
        v-model="form.age.value"
        @error="form.age.readyToSubmit=false"
        @ready="form.age.readyToSubmit=true"
        :label="msg.askAge[form.who.value]"
        :mandatory="form.age.mandatory"
        pattern="^[1-9]+[0-9]*$"
      />

      <rubin-dropdown
        v-model="form.skill.value"
        @error="form.skill.readyToSubmit=false"
        @ready="form.skill.readyToSubmit=true"
        :label="msg.askSkill[form.who.value]"
        :mandatory="form.skill.mandatory"
        :options="[
          {value: '', name: '<?php echo __('Wie schätzt Du das ein?', RUBIN_TEXT_DOMAIN); ?>'},
          {value: 'no', name: '<?php echo __('Nein, bisher keine Nennenswerten.', RUBIN_TEXT_DOMAIN); ?>'},
          {value: 'beginner', name: '<?php echo __('Ja, aber noch auf Einsteiger-Niveau.', RUBIN_TEXT_DOMAIN); ?>'},
          {value: 'advanced', name: '<?php echo __('Ja, bereits auf Fortgeschrittenen-Niveau.', RUBIN_TEXT_DOMAIN); ?>'},
        ]"
      />

      <rubin-textarea
        v-model="form.text.value"
        @error="form.text.readyToSubmit=false"
        @ready="form.text.readyToSubmit=true"
        :mandatory="form.text.mandatory"
        label="<?php echo __('Hast Du noch Anmerkungen oder konkrete Fragen?', RUBIN_TEXT_DOMAIN); ?>"
      />

      <rubin-inpot
        v-model="form.city.value"
        @error="form.city.readyToSubmit=false"
        @ready="form.city.readyToSubmit=true"
        label="Stadt"
        :mandatory="form.city.mandatory"
      />

      <rubin-inpot
        v-model="form.street.value"
        @error="form.street.readyToSubmit=false"
        @ready="form.street.readyToSubmit=true"
        label="Straße"
        :mandatory="form.street.mandatory"
      />

      <rubin-checkbox
        v-model="form.dsgvo.value"
        @error="form.dsgvo.readyToSubmit=false"
        @ready="form.dsgvo.readyToSubmit=true"
        :label="msg.dsgvo"
        label-class="tw-text-base"
        name="Alles klar, ich bin damit einverstanden."
        :mandatory="form.dsgvo.mandatory"
      />

      <rubin-submit
        v-if="!sendStatus || sendStatus !== 'success'"
        @click="handleSubmit"
        :can-submit="canSubmit"
        :busy="sending"
        :message-text="submitButtonMsg.msg"
        :message-class="submitButtonMsg.cls"
        :label="group.isFull ? '<?php echo __('In Warteliste eintragen', RUBIN_TEXT_DOMAIN); ?>' : '<?php echo __('Anfrage absenden', RUBIN_TEXT_DOMAIN); ?>'"
      />
      <div
        v-else
        class="
          tw-p-8
          tw-rounded-lg tw-shadow-md
          tw-bg-rubinRot-darkShadow
          tw-text-center
        "
      >
        <div class="tw-mb-8 tw-text-2xl">{{ msg.sended.ok.headline }}</div>
        <div class="tw-mb-8">{{ msg.sended.ok.text }}</div>
        <div
          @click="handleClickToClose"
          class="
            tw-block
            tw-w-full md:tw-w-1/2
            tw-mx-auto
            tw-border-2 tw-border-solid tw-border-gray-900 tw-rounded-lg tw-shadow
            tw-p-4
            tw-bg-rubinRot-darkerShadow
            tw-text-base tw-text-gray-900 hover:tw-text-rubinRot
            tw-cursor-pointer
            tw-transition-colors tw-duration-300
          "
        >{{ msg.sended.ok.label }}</div>
      </div>
    </div>
  </div>
</script>

<script type="application/javascript">
const rubinTrainingContact = Vue.component('rubin-training-contact', {
  template: '#rubin-template-mgvo-training-contact',

  props: {
    group: {
      type: Object,
      required: true,
    },
  },

  data() {
    return {
      form: this.initialForm(),
      sending: false,
      sendStatus: '',
      msg: {
        askAge: {
          me: '<?php echo __('Wie alt bist Du?', RUBIN_TEXT_DOMAIN); ?>',
          child: '<?php echo __('Wie alt ist Dein Kind?', RUBIN_TEXT_DOMAIN); ?>',
          other: '<?php echo __('Wie alt ist diese Person?', RUBIN_TEXT_DOMAIN); ?>',
        },
        askSkill: {
          me: '<?php echo __('Hast Du schon Tanz-Erfahrung?', RUBIN_TEXT_DOMAIN); ?>',
          child: '<?php echo __('Hat Dein Kind schon Tanz-Erfahrung?', RUBIN_TEXT_DOMAIN); ?>',
          other: '<?php echo __('Hat diese Person schon Tanz-Erfahrung?', RUBIN_TEXT_DOMAIN); ?>',
        },
        askOtherName: {
          me: '',
          child: '<?php echo __('Wie lautet der Name Deines Kindes?', RUBIN_TEXT_DOMAIN); ?>',
          other: '<?php echo __('Wie lautet der Name dieser Person?', RUBIN_TEXT_DOMAIN); ?>',
        },
        dsgvo: '<?php echo __('Du kannst sicher sein, dass wir Deine Angaben vertraulich behandeln. Sie werden nur für die Bearbeitung Deiner Anfrage gespeichert.', RUBIN_TEXT_DOMAIN); ?>',
        member: {
            ask: {
                me: '<?php echo __('Bist Du Mitglied im TSG Rubin Zwickau e.V.?', RUBIN_TEXT_DOMAIN); ?>',
                child: '<?php echo __('Ist Dein Kind Mitglied im TSG Rubin Zwickau e.V.?', RUBIN_TEXT_DOMAIN); ?>',
                other: '<?php echo __('Ist diese Person Mitglied im TSG Rubin Zwickau e.V.?', RUBIN_TEXT_DOMAIN); ?>',
            },
            yes: {
                me: '<?php echo __('Ja, ich bin Vereinsmitglied', RUBIN_TEXT_DOMAIN); ?>',
                child: '<?php echo __('Ja, mein Kind ist Vereinsmitglied', RUBIN_TEXT_DOMAIN); ?>',
                other: '<?php echo __('Ja, die Person ist Vereinsmitglied', RUBIN_TEXT_DOMAIN); ?>',
            },
            no: {
                me: '<?php echo __('Nein, ich bin kein Vereinsmitglied', RUBIN_TEXT_DOMAIN); ?>',
                child: '<?php echo __('Nein, mein Kind ist kein Vereinsmitglied', RUBIN_TEXT_DOMAIN); ?>',
                other: '<?php echo __('Nein, diese Person ist kein Vereinsmitglied', RUBIN_TEXT_DOMAIN); ?>',
            },
        },
        sended: {
          ok: {
            msg: '<?php echo __('Die Anfrage wurde erfolgreich versendet.', RUBIN_TEXT_DOMAIN); ?>',
            headline: '<?php echo __('Vielen Dank für Deine Anfrage.', RUBIN_TEXT_DOMAIN); ?>',
            text: '<?php echo __('Einer unserer Trainer wird sich in Kürze bei Dir melden.', RUBIN_TEXT_DOMAIN); ?>',
            label: '<?php echo __('Alles klar. Bis dann.', RUBIN_TEXT_DOMAIN); ?>',
          },
          error: '<?php echo __('Die Anfrage konnte nicht versendet werden. Bitte versuche es später noch einmal!', RUBIN_TEXT_DOMAIN); ?>',
        }
      },
    }
  },

  computed: {
    unrefused() {
      return (this.group.nextdat || []).filter(function(g) {return !g.refused})
    },

    askForOtherName() {
      return this.form.who.value !== 'me'
    },

    canSubmit() {
      let ok = true
      const form = this.form
      Object.keys(form).forEach(function(key) {
        ok = ok && form[key].readyToSubmit
      })
      return ok && this.sendStatus !== 'success'
    },

    submitButtonMsg() {
      if (!!this.sendStatus && this.sendStatus === 'success') {
        return {
          msg: this.msg.sended.ok.msg,
          cls: 'tw-text-green-600',
        }
      } else if (!!this.sendStatus) {
        return {
          msg: this.msg.sended.error,
          cls: 'tw-text-red-600',
        }
      } else {
        return {
          msg: '',
          cls: '',
        }
      }
    }
  },

  methods: {
    initialForm() {
      return {
        age: {value: '', mandatory: false, readyToSubmit: true},
        city: {value: '', mandatory: false, readyToSubmit: true},
        dsgvo: {value: false, mandatory: true, readyToSubmit: false},
        email: {value: '', mandatory: true, readyToSubmit: false},
        member: {value: false, mandatory: false, readyToSubmit: true},
        name: {value: '', mandatory: false, readyToSubmit: true},
        otherName: {value: '', mandatory: false, readyToSubmit: true},
        phone: {value: '', mandatory: false, readyToSubmit: true},
        skill: {value: '', mandatory: true, readyToSubmit: false},
        street: {value: '', mandatory: false, readyToSubmit: true},
        text: {value: '', mandatory: false, readyToSubmit: true},
        who: {value: 'me', mandatory: true, readyToSubmit: true},
      }
    },

    handleClickToClose() {
      this.form = this.initialForm()
      this.sendStatus = ''
      this.$emit('close')
    },

    handleSubmit() {
      const url = '/wp-json/rubin/v1/mgvo/training/sendmail'
      const data = {
        age: this.form.age.value,
        city: this.form.city.value,
        dsgvo: this.form.dsgvo.value === true ? this.msg.dsgvo : '',
        email: this.form.email.value,
        group: this.group.id,
        phone: this.form.phone.value,
        member: this.form.member.value === true ? this.msg.member.yes[this.form.who.value] : this.msg.member.no[this.form.who.value],
        name: this.form.name.value,
        otherName: this.form.otherName.value,
        skill: this.form.skill.value,
        street: this.form.street.value,
        text: this.form.text.value,
        who: this.form.who.value,
      }
      const self = this
      self.sending = true
      jQuery.post(url, data)
      .always(function() {
        self.sending = false
      })
      .done(function() {
        self.sendStatus = 'success'
      })
      .fail(function(response) {
        self.sendStatus = 'failed'
      })
    },
  },
})
</script>
