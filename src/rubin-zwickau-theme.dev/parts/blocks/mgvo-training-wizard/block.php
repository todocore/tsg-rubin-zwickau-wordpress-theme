<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_render_block_mgvo_training_wizard($block_attributes, $content) {
  if (is_admin()) {
    return '';
  };
  
  global $mgvoService;

  $defaultIgnore = get_theme_mod('mgvo-ignore-setting');
  $defaultIgnore = isset($defaultIgnore) ? $defaultIgnore : '';

  $title = $block_attributes['title'];
  $dateCount = $block_attributes['dateCount'];
  $includeOpen = $block_attributes['includeOpen'];
  $includeClosed = $block_attributes['includeClosed'];
  $includeRefusals = $block_attributes['includeRefusals'];
  $includeTrainings = $includeOpen || $includeClosed;
  $limitCategory = $block_attributes['limitCategory'];
  $ignoreText = $block_attributes['ignoreText'];

  $instance = array(
    'title' => isset($title) ? $title : __('Unsere Trainingstermine', RUBIN_TEXT_DOMAIN),
    'dateCount' => isset($dateCount) ? $dateCount : 4,
    'includeOpen' => $includeOpen,
    'includeClosed' => $includeClosed,
    'includeRefusals' => $includeRefusals,
    'limitCategory' => isset($limitCategory) ? $limitCategory : 'WIDGET_ASK',
    'ignore' => isset($ignoreText) ? $ignoreText : $defaultIgnore
  );

  $refusals = $mgvoService->fetchRefusals();
  $refusals = $mgvoService->sanitizeRefusals($refusals, false, []);

  $groups = $mgvoService->fetchTrainingGroups();
  $groups = $mgvoService->sanitizeTrainingGroupsByWeek($groups, $refusals, $instance);

  ob_start();
    get_template_part('/parts/blocks/mgvo-training-wizard/layout', null, array(
      'id' => uniqid(),
      'byId' => $groups['byId'],
      'title' => $instance['title'],
      'week' => $groups['week'],
      'instance' => $instance,
    ));
  return ob_get_clean();
}

function rubin_register_block_mgvo_training_wizard() {
  register_block_type('rubin/mgvo-training-wizard', array(
    'api_version' => 2,
    'render_callback' => 'rubin_render_block_mgvo_training_wizard',
  ));  
}
add_action('init', 'rubin_register_block_mgvo_training_wizard');