<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_cancellations_feed_rss2() {
  global $mgvoService;

  $refusals = $mgvoService->fetchRefusals();
  $refusals = $mgvoService->sanitizeRefusals($refusals, false, []);

  get_template_part('/parts/feeds/mgvo-training-cancellations/rss2', null, array(
    'data' => $refusals,
  ));
}

function rubin_register_cancellations_feed() {
  add_feed('training-cancellations', 'rubin_cancellations_feed_rss2');
}
add_action('init', 'rubin_register_cancellations_feed');
