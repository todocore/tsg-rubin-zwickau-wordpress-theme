<?php
defined(ALLOW_RUBIN_THEME) or die();

header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss
  version="2.0"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:wfw="http://wellformedweb.org/CommentAPI/"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
  xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
  <?php do_action('rss2_ns'); ?>
>
<channel>
  <title>TSG Rubin Zwickau e.V. - Trainingsausfälle</title>
  <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
  <link><?php bloginfo_rss('url') ?></link>
  <description><?php bloginfo_rss('description') ?></description>
  <lastBuildDate><?php echo date('D, d M Y H:i:s +0000', strtotime('now')); ?></lastBuildDate>
  <language>de-DE</language>
  <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'hourly' ); ?></sy:updatePeriod>
  <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '1' ); ?></sy:updateFrequency>
  <?php foreach ($args['data'] as $key => $refusal) { ?>
  <item>
    <guid isPermaLink="false"><?php echo $key; ?></guid>
    <title><?php echo $refusal['grbez']; ?></title>
    <pubDate><?php echo date('D, d M Y H:i:s +0000', $refusal['pubDate']); ?></pubDate>
    <description><?php echo $refusal['zeitraum'].' ('.$refusal['ortsbez'].')'; ?></description>
    <?php rss_enclosure(); ?>
    <?php do_action('rss2_item'); ?>
  </item>
  <?php } ?>
</channel>
</rss>
