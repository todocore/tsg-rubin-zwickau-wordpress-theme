<?php
defined(ALLOW_RUBIN_THEME) or die();

header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss
  version="2.0"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:wfw="http://wellformedweb.org/CommentAPI/"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
  xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
  <?php do_action('rss2_ns'); ?>
>
<channel>
  <title>TSG Rubin Zwickau e.V. - Nächste Veranstaltungen</title>
  <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
  <link><?php bloginfo_rss('url') ?></link>
  <description><?php bloginfo_rss('description') ?></description>
  <lastBuildDate><?php echo date('D, d M Y H:i:s +0000', strtotime('now')); ?></lastBuildDate>
  <language>de-DE</language>
  <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'hourly' ); ?></sy:updatePeriod>
  <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '1' ); ?></sy:updateFrequency>
  <?php foreach ($args['data'] as $key => $event) { ?>
  <item>
    <guid isPermaLink="false"><?php echo $event['startdat'].$key; ?></guid>
    <title><?php echo $event['name']; ?></title>
    <pubDate><?php echo date('D, d M Y', strtotime($event['startdat'])).' '.date('H:i:s +0000', strtotime($event['startzeit'])); ?></pubDate>
    <description><?php echo $event['zeitraum']; ?></description>
    <?php rss_enclosure(); ?>
    <?php do_action('rss2_item'); ?>
  </item>
  <?php } ?>
</channel>
</rss>
