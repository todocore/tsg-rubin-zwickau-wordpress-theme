<?php
defined(ALLOW_RUBIN_THEME) or die();

function rubin_next_events_feed_rss2() {
  global $mgvoService;

  $calNr = sanitize_text_field( get_query_var( 'calnr' ) );
  if (!preg_match('/^[0-9]+$/', $calNr)) {
    $calNr = '1';
  }

  $now = strtotime('now');
  $next = strtotime('+1 year', $now);

  $nowEvents = $mgvoService->fetchEventsByYear($calNr, $now);
  $nextEvents = $mgvoService->fetchEventsByYear($calNr, $next);

  $allEvents = array();
  $allEvents = $mgvoService->mergeEvents($allEvents, $mgvoService->sanitizeEventsByDate($nowEvents));
  $allEvents = $mgvoService->mergeEvents($allEvents, $mgvoService->sanitizeEventsByDate($nextEvents));
  $allEvents = $mgvoService->sortEvents($allEvents);
  $allEvents = $mgvoService->convertEventsToListLayout($allEvents, 9999);

  get_template_part('/parts/feeds/mgvo-next-events/rss2', null, array(
    'data' => sizeof($allEvents) > 0 ? $allEvents[0] : [],
  ));
}

function rubin_register_next_events_feed_query_vars($qvars) {
    $qvars[] = 'calnr';
    return $qvars;
}
function rubin_register_next_events_feed() {
  add_feed('next-events', 'rubin_next_events_feed_rss2');
}
add_action('init', 'rubin_register_next_events_feed');
add_filter('query_vars', 'rubin_register_next_events_feed_query_vars');
