<div class="
  tw-hidden lg:tw-block
  tw-fixed tw-top-0 tw-left-0
  tw-w-full tw-h-28
  tw-bg-gray-50
  tw-shadow
  tw-z-50
">
  <a href="/" class="tw-absolute tw-left-4 tw-top-4 tw-h-20">
    <img
      alt="TSG Rubin"
      src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/LogoRubin-full.svg"
      class="tw-h-full"
    />
  </a>
  <div id="rubin-primary-menu" class="tw-h-28 tw-pr-4">
    <?php 
      wp_nav_menu(array(
        'menu' => 'primary',
        'container' => '',
        'theme_location' => 'primary'
      ));
    ?>
  </div>
</div>