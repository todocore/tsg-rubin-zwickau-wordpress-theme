<script type="text/x-template" id="vue-rubin-submit-template">
  <div>
    <button
      @click="handleClick"
      :class="{
        'tw-border-gray-300': !canSubmit,
        'tw-text-gray-300': !canSubmit,
        'tw-bg-gray-50': !canSubmit,
        'tw-cursor-default': !canSubmit,

        'tw-border-gray-900': canSubmit,
        'tw-bg-rubinRot-darkShadow': canSubmit,
        'tw-text-gray-900': canSubmit,
        'hover:tw-text-rubinRot': canSubmit,
        'tw-cursor-pointer': canSubmit,
      }"
      role="button"
      class="
        tw-block
        tw-w-full
        tw-border-2 tw-border-solid tw-rounded-lg
        tw-shadow
        tw-p-4
        tw-text-base 
        tw-transition-colors tw-duration-300
      "
    >{{ label }}</button>
    <div v-if="!!messageText" :class="messageClass" class="tw-font-dejaVuSlim tw-text-base tw-text-center">{{ messageText }}</div>
  </div>
</script>

<script type="application/javascript">
const rubinSubmit = Vue.component('rubin-submit', {
  template: '#vue-rubin-submit-template',

  props: {
    label: {
      type: String,
      default: '',
    },

    canSubmit: {
      type: Boolean,
      default: false,
    },

    messageText: {
      type: String,
      default: '',
    },

    messageClass: {
      type: String,
      default: '',
    },
  },

  methods: {
    handleClick() {
      if (this.canSubmit) {
        this.$emit('click')
      }
    }
  },
})
</script>
