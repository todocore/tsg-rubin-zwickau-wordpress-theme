<script type="text/x-template" id="vue-rubin-textarea-template">
  <div
    class="
      tw-flex tw-flex-col md:tw-flex-row tw-flex-wrap
      tw-w-full
      tw-mb-8
    "
  >
    <label
      class="
        tw-block
        tw-w-full md:tw-w-1/2
        md:tw-pr-8
      "
    >{{ label }} <sup v-if="mandatory">(*)</sup></label>
    <textarea
      @blur="handleBlur"
      @input="handleInput"
      :value="value"
      :class="{
        'tw-border-rubinRot-light': hasError,
        'focus:tw-border-gray-900': !hasError,
        'tw-border-gray-900': hasValue,
      }"
      class="
        tw-block
        tw-w-full md:tw-w-1/2
        tw-outline-none
        tw-border-2 tw-border-solid tw-border-gray-50 tw-rounded-lg tw-shadow
        tw-p-4
        tw-bg-gray-50
        tw-text-base tw-text-gray-900
        tw-cursor-text
        tw-resize-y
        tw-transition-colors tw-duration-300
        "
    ></textarea>
    <div
      v-if="hasError"
      class="
        tw-w-full
        tw-pt-1 tw-pl-0 md:tw-pl-2:1
        tw-font-dejaVuSlim
        tw-text-sm tw-text-rubinRot-light
      "
    >{{ msgError }}</div>
  </div>
</script>

<script type="application/javascript">
const rubinTextarea = Vue.component('rubin-textarea', {
  template: '#vue-rubin-textarea-template',

  props: {
    value: {
      type: String,
      default: '',
    },

    label: {
      type: String,
      default: '',
    },

    mandatory: {
      type: Boolean,
      default: false,
    },
  },

  data() {
    return {
      showError: false,
      msgError: '',
      msg: {
        missingValue: '<?php echo __('Bitte dieses Feld ausfüllen!', RUBIN_TEXT_DOMAIN); ?>',
      },
    }
  },

  computed: {
    hasError() {
      return !!this.showError && !!this.msgError
    },

    hasValue() {
      return !!this.value && !this.hasError
    }
  },

  methods: {
    handleBlur(e) {
      this.showError = true
    },

    handleInput(e) {
      const value = e.target.value
      if (!value) {
        this.showError = false
      } else if (!this.value) {
        this.showError = false
      }

      this.msgError = ''
      if (this.mandatory && !value) {
        this.msgError = this.msg.missingValue
      }

      this.$emit('input', value)
      if (!!this.msgError) {
        this.$emit('error')
      } else {
        this.$emit('ready')
      }
    },
  },
})
</script>
