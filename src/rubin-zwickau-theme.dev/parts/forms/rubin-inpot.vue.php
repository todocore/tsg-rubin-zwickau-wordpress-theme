<script type="text/x-template" id="vue-rubin-inpot-template">
  <div
    class="tw-absolute tw-z-mandatory"
  >
    <label
      class="
        tw-block
        tw-w-full md:tw-w-1/2
        md:tw-pr-8
      "
    >{{ label }} <sup v-if="mandatory">(*)</sup></label>
    <input
      @blur="handleBlur"
      @input="handleInput"
      :value="value"
      :class="{
        'tw-border-rubinRot-light': hasError,
        'focus:tw-border-gray-900': !hasError,
        'tw-border-gray-900': hasValue,
      }"
      class="
        tw-block
        tw-w-full md:tw-w-1/2
        tw-outline-none
        tw-border-2 tw-border-solid tw-border-gray-50 tw-rounded-lg tw-shadow
        tw-p-4
        tw-bg-gray-50
        tw-text-base tw-text-gray-900
        tw-cursor-text
        tw-transition-colors tw-duration-300
        "
      type="text"
      tabindex="-1"
    />
    <div
      v-if="hasError"
      class="
        tw-w-full
        tw-pt-1 tw-pl-0 md:tw-pl-2:1
        tw-font-dejaVuSlim
        tw-text-sm tw-text-rubinRot-light
      "
    >{{ msgError }}</div>
  </div>
</script>

<script type="application/javascript">
const rubinInpot = Vue.component('rubin-inpot', {
  template: '#vue-rubin-inpot-template',

  props: {
    value: {
      type: String,
      default: '',
    },

    label: {
      type: String,
      default: '',
    },

    pattern: {
      type: String,
      default: '',
    },

    mandatory: {
      type: Boolean,
      default: false,
    },
  },

  data() {
    return {
      showError: false,
      msgError: '',
      msg: {
        missingValue: '<?php echo __('Bitte dieses Feld ausfüllen!', RUBIN_TEXT_DOMAIN); ?>',
        invalidValue: '<?php echo __('Diese Eingabe ist ungültig. Bitte überprüfe sie noch einmal!', RUBIN_TEXT_DOMAIN); ?>',
      },
    }
  },

  computed: {
    hasError() {
      return !!this.showError && !!this.msgError
    },

    hasValue() {
      return !!this.value && !this.hasError
    }
  },

  methods: {
    handleBlur(e) {
      this.showError = true
    },

    handleInput(e) {
      const value = e.target.value
      if (!value) {
        this.showError = false
      } else if (!this.value) {
        this.showError = false
      }

      this.msgError = ''
      if (!!value && !!this.pattern) {
        const re = new RegExp(this.pattern)
        if (!value.match(re)) {
          this.msgError = this.msg.invalidValue
        }
      }

      if (this.mandatory && !value) {
        this.msgError = this.msg.missingValue
      }

      this.$emit('input', value)
      if (!!this.msgError) {
        this.$emit('error')
      } else {
        this.$emit('ready')
      }
    },
  },
})
</script>
