<script type="text/x-template" id="vue-rubin-checkbox-template">
  <div
    class="
      tw-flex tw-flex-col md:tw-flex-row tw-flex-wrap
      tw-w-full
      tw-mb-8
    "
  >
    <label
      :class="[labelClass]"
      class="
        tw-block
        tw-w-full md:tw-w-1/2
        md:tw-pr-8
      "
    >{{ label }}</label>
    <div
      class="
        tw-w-full md:tw-w-1/2
        tw-mt-4 md:tw-mt-0
        tw-flex tw-flex-row tw-flex-nowrap tw-items-center
      "
    >
      <input
        @blur="handleBlur"
        @click="handleClick"
        :checked="value ? 'checked' : ''"
        class="
          tw-block
          tw-flex-grow-0 tw-flex-shrink-0
          tw-w-6 tw-h-6
          tw-outline-none
          tw-cursor-pointer
        "
        type="checkbox"
      />
      <div
        class="
          tw-flex-1
          tw-pl-4
          tw-leading-5
          tw-cursor-pointer
        "
        @click="handleClick"
      >{{ name }} <sup v-if="mandatory">(*)</sup></div>
    </div>
    <div
      v-if="hasError"
      class="
        tw-w-full
        tw-pt-1 tw-pl-0 md:tw-pl-2:1
        tw-font-dejaVuSlim
        tw-text-sm tw-text-rubinRot-light
      "
    >{{ msgError }}</div>
  </div>
</script>

<script type="application/javascript">
const rubinCheckbox = Vue.component('rubin-checkbox', {
  template: '#vue-rubin-checkbox-template',

  props: {
    value: {
      type: Boolean,
      default: false,
    },

    label: {
      type: String,
      default: '',
    },

    labelClass: {
      type: String,
      default: '',
    },

    name: {
      type: String,
      default: '',
    },

    mandatory: {
      type: Boolean,
      default: false,
    },
  },

  data() {
    return {
      showError: false,
      msgError: '',
      msg: {
        missingValue: '<?php echo __('Bitte dieses Feld bestätigen!', RUBIN_TEXT_DOMAIN); ?>',
      },
    }
  },

  computed: {
    hasError() {
      return !!this.showError && !!this.msgError
    }
  },

  methods: {
    handleBlur(e) {
      this.showError = true
    },

    handleClick() {
      const newValue = !this.value

      this.msgError = ''
      if (this.mandatory && !newValue) {
        this.msgError = this.msg.missingValue
      }

      this.$emit('input', newValue)
      if (!!this.msgError) {
        this.$emit('error')
      } else {
        this.$emit('ready')
      }
    },
  },
})
</script>
