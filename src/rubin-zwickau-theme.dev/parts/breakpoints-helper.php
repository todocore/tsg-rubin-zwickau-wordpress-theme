<?php if (get_theme_mod('development-breakpoint-helper-setting') === true) { ?>
  <div class="
    tw-fixed tw-left-0 tw-bottom-0
    tw-ml-4 tw-mb-4
    tw-rounded
    tw-p-4
    tw-flex tw-items-center tw-justify-items-center
    tw-bg-blue-900
    tw-opacity-25
    tw-z-50"
  >
    <div class="tw-text-white tw-hidden sm:tw-hidden md:tw-hidden lg:tw-hidden xl:tw-block">XL</div>
    <div class="tw-text-white tw-hidden sm:tw-hidden md:tw-hidden lg:tw-block xl:tw-hidden">LG</div>
    <div class="tw-text-white tw-hidden sm:tw-hidden md:tw-block lg:tw-hidden xl:tw-hidden">MD</div>
    <div class="tw-text-white tw-hidden sm:tw-block md:tw-hidden lg:tw-hidden xl:tw-hidden">SM</div>
    <div class="tw-text-white tw-block sm:tw-hidden md:tw-hidden lg:tw-hidden xl:tw-hidden">XS</div>
  </div>
<?php } ?>