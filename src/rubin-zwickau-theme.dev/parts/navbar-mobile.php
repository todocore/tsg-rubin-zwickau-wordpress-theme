<div
  id="rubin-navbar-mobile"
  class="
    tw-block lg:tw-hidden
    tw-fixed tw-left-0
    tw-z-40
    rubin-top-nav-closed
    tw-w-full tw-h-screen
    tw-bg-rubinRot-darkShadow
    tw-transition-all tw-duration-500
  "
>
  <div id="rubin-mobile-menu" class="tw-w-full rubin-h-mobile-nav tw-overflow-hidden tw-overflow-y-auto tw-bg-gray-50">
    <?php 
      wp_nav_menu(array(
        'menu' => 'primary',
        'container' => '',
        'theme_location' => 'primary'
      ));
    ?>
  </div>

  <div id="rubin-mobile-menu-toggle">
    <a href="/">
      <img
        alt="TSG Rubin"
        src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/LogoRubin-full.svg"
        class="tw-absolute tw-left-4 tw-top-4 tw-h-20"
      />
    </a>
    <?php rubin_svg(get_template_directory().'/assets/img/chevron.svg', array(
      'class' => 'rubin-mobile-menu-toggle-chevron',
      'onclick' => ''
        .' jQuery(\'#rubin-navbar-mobile\').toggleClass([\'rubin-top-nav-closed\', \'rubin-top-nav-opened\']);'
        .' jQuery(\'#rubin-body\').toggleClass(\'rubin-no-scroll\'); '
    )); ?>
  </div>
</div>