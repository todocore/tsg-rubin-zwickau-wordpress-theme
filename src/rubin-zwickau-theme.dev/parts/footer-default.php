<div id="rubin-footer" class="tw-relative tw-bg-gray-800 tw-text-white tw-pr-4">
  <?php if (is_active_sidebar('footer_widgets')) { ?>
    <div id="rubin-footer-widgets" class="tw-relative tw-flex tw-flex-row tw-flex-wrap tw-items-stretch tw-justify-center tw-w-full tw-pt-6">
      <?php dynamic_sidebar('footer_widgets');  ?>
    </div>
  <?php } ?>
  <div class="tw-relative tw-min-h-28">
    <div id="rubin-footer-menu" class="tw-w-full sm:tw-h-20 tw-pt-4 tw-pb-4">
      <?php 
        wp_nav_menu(array(
          'menu' => 'footer',
          'container' => '',
          'theme_location' => 'footer'
        ));
      ?>
    </div>
    <div class="tw-w-full tw-h-auto sm:tw-absolute sm:tw-w-12 sm:tw-h-12 sm:tw-left-4 sm:tw-top-4 sm:tw-mx-0 tw-pb-16">
      <?php
        rubin_svg(get_template_directory().'/assets/img/LogoRubin-reduced.svg', array(
          'class' => 'tw-block tw-w-12 tw-h-12 tw-mx-auto tw-cursor-pointer',
          'title' => 'nach oben',
          '@click' => 'window.scrollTo({top:0, left: 0, behavior: \'smooth\'})'
        ));
      ?>
      <div class="tw-font-dejaVuSlim tw-text-base tw-text-white tw-text-center sm:tw-whitespace-nowrap">
        &copy; <?php echo date('Y'); ?>
      </div>
    </div>
  </div>
</div>