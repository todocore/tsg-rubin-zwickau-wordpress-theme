<?php get_header(); ?>

<?php get_template_part('/parts/navbar', 'desktop'); ?>
<?php get_template_part('/parts/navbar', 'mobile'); ?>

<!-- TEMPLATE: front-page -->
<div class="xl:tw-w-rubin/xl tw-mx-auto tw-mt-28 tw-bg-white tw-px-4 md:tw-px-12 tw-pb-12 tw-shadow-xl">
  <?php 
    if ( have_posts() ) {
	    while ( have_posts() ) {
        the_post();
        the_content();
	    }
    }
?>
</div>

<?php get_template_part('/parts/footer', 'default'); ?>

<?php get_footer(); ?>
