<?php get_header(); ?>

<?php get_template_part('/parts/navbar', 'desktop'); ?>
<?php get_template_part('/parts/navbar', 'mobile'); ?>

<!-- TEMPLATE: index -->
<div class="lg:tw-w-rubin/lg tw-min-h-75% tw-mx-auto tw-mt-28 tw-bg-white tw-px-6 lg:tw-px-12 tw-py-12 tw-shadow-xl">
  <?php
    if (have_posts()) {
      while (have_posts()) {
        echo '<h1>'.get_the_title().'</h1>';
        the_post();
        the_content();
      }
    }
  ?>
</div>

<?php get_template_part('/parts/footer', 'default'); ?>

<?php get_footer(); ?>
