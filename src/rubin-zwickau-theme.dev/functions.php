<?php
  defined(ALLOW_RUBIN_THEME) or die();

  define('RUBIN_TEXT_DOMAIN', 'rubin-zwickau');

  $userId = get_theme_mod('mgvo-userid-setting');
  $secret = get_theme_mod('mgvo-secret-setting');
  $cacheTime = get_theme_mod('mgvo-cache-setting');
  $upload = wp_upload_dir();
  $upload_dir = $upload['basedir'];
  $upload_dir = $upload_dir . '/mgvo-cache';
  if (! is_dir($upload_dir)) {
     mkdir( $upload_dir, 0700 );
  }

  include_once get_template_directory().'/includes/MGVOService.php';
  $mgvoService = new MGVOService($userId,$secret,$cacheTime,$upload_dir);

  $rubinStyleMap = json_decode(file_get_contents(get_template_directory().'/assets/css/style.map'), true);
  $CSS_RUBIN = 'assets/css/'.$rubinStyleMap['rubin.css'];
  $CSS_TAILWIND = 'assets/css/'.$rubinStyleMap['tailwind.css'];

  include_once get_template_directory().'/includes/api-mgvo-training-sendmail.php';
  include_once get_template_directory().'/includes/backend-defaults4js.php';
  include_once get_template_directory().'/includes/customizer-development.php';
  include_once get_template_directory().'/includes/customizer-mgvo-api.php';
  include_once get_template_directory().'/includes/customizer-mgvo-training.php';
  include_once get_template_directory().'/includes/dashboard-mgvo-training.php';
  include_once get_template_directory().'/includes/plugin-wpforms-setup.php';
  include_once get_template_directory().'/includes/theme-blocks.php';
  include_once get_template_directory().'/includes/theme-helper-args.php';
  include_once get_template_directory().'/includes/theme-helper-svg.php';
  include_once get_template_directory().'/includes/theme-menus.php';
  include_once get_template_directory().'/includes/theme-scripts.php';
  include_once get_template_directory().'/includes/theme-setup.php';
  include_once get_template_directory().'/includes/theme-styles.php';
  include_once get_template_directory().'/includes/theme-widgets.php';

  include_once get_template_directory().'/parts/feeds/mgvo-next-events/feed.php';
  include_once get_template_directory().'/parts/feeds/mgvo-training-cancellations/feed.php';

  include_once get_template_directory().'/parts/blocks/rubin-hero-logo/block.php';
  include_once get_template_directory().'/parts/blocks/mgvo-training-calendar/block.php';
  include_once get_template_directory().'/parts/blocks/mgvo-training-cancellations/block.php';
  include_once get_template_directory().'/parts/blocks/mgvo-training-wizard/block.php';
?>