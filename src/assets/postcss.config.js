module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss')('./tailwind.config.js'),
    require('autoprefixer'),
    require('cssnano')({
        preset: 'default',
    }),
    require('postcss-hash')({
      algorithm: 'sha256',
      manifest: './../rubin-zwickau-theme.dev/assets/css/style.map'
    }),
  ],
}
