import { registerBlockType } from '@wordpress/blocks';
import mgvoTrainingCalendar from './blocks/mgvo-training-calendar'
import mgvoTrainingCancellations from './blocks/mgvo-training-cancellations'
import mgvoTrainingWizard from './blocks/mgvo-training-wizard'
import rubinHeroLogo from './blocks/rubin-hero-logo'
 
registerBlockType('rubin/mgvo-training-calendar', mgvoTrainingCalendar)
registerBlockType('rubin/mgvo-training-cancellations', mgvoTrainingCancellations)
registerBlockType('rubin/mgvo-training-wizard', mgvoTrainingWizard)
registerBlockType('rubin/rubin-hero-logo', rubinHeroLogo)
