import {
    useBlockProps,
    InspectorControls,
} from '@wordpress/block-editor';
import {
  PanelBody,
  PanelRow,
  RangeControl,
} from '@wordpress/components';

export default {
    title: 'Rubin Hero Logo',
    icon: 'cover-image',
    category: 'widgets',
    attributes: {
      scale: {type: 'number'},
      xOffset: { type: 'number' },
      yOffset: { type: 'number' },
    },
    edit: (args) => {
      const props = args || {}
      const { attributes, setAttributes } = props || {}
      return (
        <div { ...useBlockProps() }>
          <InspectorControls>
            <PanelBody title="Anzeige">
              <PanelRow>
                <div className='tw-w-full'>
                  <RangeControl
                    label="Größe"
                    help="relativ zur Bildschirmbreite"
                    value={attributes.scale}
                    min={0}
                    max={200}
                    initialPosition={66}
                    withInputField={false}
                    onChange={(value) => setAttributes({ scale: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <RangeControl
                    label="Horizontale Verschiebung"
                    value={attributes.xOffset}
                    min={-200}
                    max={200}
                    step={1}
                    initialPosition={0}
                    withInputField={false}
                    onChange={(value) => setAttributes({ xOffset: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <RangeControl
                    label="Vertikale Verschiebung"
                    value={attributes.yOffset}
                    min={-200}
                    max={200}
                    step={1}
                    initialPosition={0}
                    withInputField={false}
                    onChange={(value) => setAttributes({ yOffset: value })}
                  />
                </div>
              </PanelRow>
            </PanelBody>
          </InspectorControls>
          <div className="tw-relative tw-w-full tw-flex tw-justify-center tw-items-center tw-border-2 tw-border-dashed tw-border-rubinRot tw-z-10">
            <img
              alt="TSG Rubin Zwickau e.V."
              src="/wp-content/themes/rubin-zwickau-theme/assets/img/LogoRubin-hero.png"
              className="tw-absolute tw-top-1/2 tw-left-1/2 tw-border tw-border-dotted tw-border-rubinRot tw-pointer-events-none"
              style={{
                width: `${attributes.scale || 66}vW`,
                maxWidth: 'none',
                transform: `translate(calc(-50% + ${attributes.xOffset || 0}%), calc(-50% + ${attributes.yOffset || 0}%))`,
              }} 
            />
            <div className="tw-absolute tw--top-4 tw-left-0 tw-w-full tw-text-rubinRot tw-text-center"><p><small>Markiere hier das Widget, um seine Einstellungen im Block-Menü bearbeiten zu können.</small></p></div>
          </div>
        </div>
      )
    },
}
