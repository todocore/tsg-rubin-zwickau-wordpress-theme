import {
  useBlockProps,
  InspectorControls,
} from '@wordpress/block-editor';
import {
  PanelBody,
  PanelRow,
  TextControl,
} from '@wordpress/components';

export default {
    title: 'MGVO Trainingsausfälle',
    icon: 'trash',
    category: 'widgets',
    attributes: {
      limitByDays: { type: 'string' },
      ignoreText: { type: 'string' },
    },
    edit: (args) => {
      const defaults = window.rubinThemeDefaults || {}
      const props = args || {}
      const { attributes, setAttributes } = props || {}
      return (
        <div { ...useBlockProps() }>
          <InspectorControls>
            <PanelBody title="Einstellungen zur Anzeige von Trainingsausfällen">
              <PanelRow>
                <div className='tw-w-full'>
                  <TextControl
                    label="Limit Tage"
                    help="Ausgehend von aktuellen Tag werden nur Trainingsausfälle der nächsten x Tage angezeigt. (0 oder leer bedeutet: kein Limit)"
                    value={attributes.limitByDays}
                    onChange={(value) => setAttributes({ limitByDays: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <TextControl
                    label="Einträge ausschliessen"
                    help=" Einträge mit diesem Text werden NICHT angezeigt. Die Angabe von mehreren Texten ist möglich, wenn diese durch Kommas getrennt sind."
                    value={attributes.ignoreText}
                    placeholder={'z.B. ' + defaults.ignore}
                    onChange={(value) => setAttributes({ ignoreText: value })}
                  />
                </div>
              </PanelRow>
            </PanelBody>
          </InspectorControls>
          <div className="tw-relative tw-w-full tw-p-4">
            <div className="
              tw-flex tw-flex-col tw-justify-center tw-content-center
              tw-border-4 tw-border-solid tw-border-rubinRot
              tw-rounded-lg
              tw-bg-rubinRot-shadow tw-shadow-xl
              tw-px-8 tw-pt-4 tw-pb-8
            ">
              <div><h2>MGVO Trainingsausfälle</h2></div>
              {
                (!attributes.limitByDays || attributes.limitByDays == 0) &&
                <div>
                  Es werden <strong>alle</strong> im MGVO eingetragenen Ausfälle als Liste angezeigt.
                </div>
              }
              {
                !!attributes.limitByDays &&
                <div>
                  Es werden die im MGVO eingetragenen Ausfälle der nächsten <strong>{ attributes.limitByDays } Tag(e)</strong> als Liste angezeigt.
                </div>
              }
              <div className="tw-text-rubinRot"><p><small>Markiere hier das Widget, um seine Einstellungen im Block-Menü bearbeiten zu können.</small></p></div>
            </div>
          </div>
        </div>
      )
    },
    save: () => null,
}
