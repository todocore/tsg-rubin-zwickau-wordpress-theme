import {
    useBlockProps,
    InspectorControls,
} from '@wordpress/block-editor';
import {
  PanelBody,
  PanelRow,
  SelectControl,
  TextControl,
  ToggleControl,
} from '@wordpress/components';

const options4Layout = {
  COMPACT: 'Kalenderansicht',
  LIST: 'Listenansicht',
  MINI: 'verkleinerte Ansicht',
}

export default {
    title: 'MGVO Trainingskalender',
    icon: 'calendar',
    category: 'widgets',
    attributes: {
      layout: { type: 'string' },
      calNr: { type: 'string' },
      includeOpen: { type: 'boolean' },
      includeClosed: { type: 'boolean' },
      includeRefusals: { type: 'boolean' },
      includeHolidays: { type: 'boolean' },
      forceHolidays: { type: 'string' },
      ignoreText: { type: 'string' },
    },
    edit: (args) => {
      const defaults = window.rubinThemeDefaults || {}
      const props = args || {}
      const { attributes, setAttributes } = props || {}
      if (Object.keys(options4Layout).includes(attributes.layout)) {
        attributes.layout = options4Layout.COMPACT
      }
      return (
        <div { ...useBlockProps() }>
          <InspectorControls>
            <PanelBody title="Einstellungen zur Datenquelle (MGVO)">
              <PanelRow>
                <div className='tw-w-full'>
                  <TextControl
                    label="Kalendernummer"
                    value={attributes.calNr}
                    onChange={(value) => setAttributes({ calNr: value })}
                  />
                </div>
              </PanelRow>
            </PanelBody>
            <PanelBody title="Anzeige">
              <PanelRow>
                <div className='tw-w-full'>
                  <SelectControl
                    label="Layout"
                    value={attributes.layout}
                    options={Object.keys(options4Layout).map((key) => ({ label: options4Layout[key], value: key }))}
                    onChange={(value) => setAttributes({ layout: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <ToggleControl
                    label="offene Trainingsgruppen anzeigen"
                    checked={attributes.includeOpen}
                    onChange={(value) => setAttributes({ includeOpen: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <ToggleControl
                    label="geschlossene Trainingsgruppen anzeigen"
                    checked={attributes.includeClosed}
                    onChange={(value) => setAttributes({ includeClosed: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <ToggleControl
                    label="Abgesagte Trainingstermine anzeigen"
                    disabled={!attributes.includeOpen && !attributes.includeClosed}
                    checked={attributes.includeRefusals}
                    onChange={(value) => setAttributes({ includeRefusals: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <ToggleControl
                    label="Trainingstermine an Feiertagen anzeigen"
                    checked={attributes.includeHolidays}
                    onChange={(value) => setAttributes({ includeHolidays: value })}
                  />
                  {
                    !attributes.includeHolidays &&
                    <TextControl
                      label="Anzeige an Feiertagen (Ausnahmen)"
                      help=" Einträge mit diesem Text werden an Feiertagen trotz deaktivierter Anzeige dennoch angezeigt. Die Angabe von mehreren Texten ist möglich, wenn diese durch Kommas getrennt sind."
                      value={attributes.forceHolidays}
                      placeholder={'Gruppen-ID oder -name'}
                      onChange={(value) => setAttributes({ forceHolidays: value })}
                    />
                  }
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <TextControl
                    label="Einträge IMMER ausschliessen"
                    help=" Einträge mit diesem Text werden NICHT angezeigt. Die Angabe von mehreren Texten ist möglich, wenn diese durch Kommas getrennt sind."
                    value={attributes.ignoreText}
                    placeholder={'z.B. ' + defaults.ignore}
                    onChange={(value) => setAttributes({ ignoreText: value })}
                  />
                </div>
              </PanelRow>
            </PanelBody>
          </InspectorControls>
          <div className="tw-relative tw-w-full tw-p-4">
            <div className="
              tw-flex tw-flex-col tw-justify-center tw-content-center
              tw-border-4 tw-border-solid tw-border-rubinRot
              tw-rounded-lg
              tw-bg-rubinRot-shadow tw-shadow-xl
              tw-px-8 tw-pt-4 tw-pb-8
            ">
              <div>
                <div className='tw-pt-4 tw-italic'><small>{ options4Layout[attributes.layout] }</small></div>
                <h2 className='tw-mt-0'>MGVO Trainingskalender</h2>
              </div>
              {
                attributes.calNr &&
                <div>
                  Es werden die Einträge von Kalender <strong>{ attributes.calNr }</strong> aus MGVO angezeigt.
                </div>
              }
              {
                !attributes.calNr &&
                <div>
                  Es werden Einträge eines bestimmten Kalenders aus MGVO angezeigt.
                </div>
              }
              <ul>
                <li>offenene Trainingsgruppen werden <strong>{attributes.includeOpen ? 'angezeigt' : 'ausgeblendet'}</strong></li>
                <li>geschlossene Trainingsgruppen werden <strong>{attributes.includeClosed ? 'angezeigt' : 'ausgeblendet'}</strong></li>
                { (attributes.includeOpen || attributes.includeClosed) &&
                  <li>Trainingsabsagen werden <strong>{attributes.includeRefusals ? 'angezeigt' : 'ausgeblendet'}</strong></li>
                }
                <li>
                  Trainingstermine an Feiertagen werden <strong>{attributes.includeHolidays ? 'angezeigt' : 'ausgeblendet'}</strong>
                  { (!attributes.includeHolidays && !(attributes.forceHolidays || '').trim()) &&
                    <div>(<strong>keine</strong> Ausnahmen)</div>
                  }
                  { (!attributes.includeHolidays && !!(attributes.forceHolidays || '').trim()) &&
                    <div>(außer &quot;<strong>{attributes.forceHolidays}&quot;</strong>)</div>
                  }
                </li>
              </ul>
              <div className="tw-text-rubinRot"><p><small>Markiere hier das Widget, um seine Einstellungen im Block-Menü bearbeiten zu können.</small></p></div>
            </div>
          </div>
        </div>
      )
    },
}
