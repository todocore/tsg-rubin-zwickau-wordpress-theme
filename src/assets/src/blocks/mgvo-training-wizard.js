import {
    useBlockProps,
    InspectorControls,
} from '@wordpress/block-editor';
import {
  Button,
  ButtonGroup,
  Dashicon,
  PanelBody,
  PanelRow,
  SelectControl,
  TextControl,
  ToggleControl,
} from '@wordpress/components';

export default {
    title: 'MGVO Trainingsübersicht',
    icon: 'welcome-widgets-menus',
    category: 'widgets',
    attributes: {
      title: { type: 'string' },
      dateCount: { type: 'string' },
      includeOpen: { type: 'boolean' },
      includeClosed: { type: 'boolean' },
      includeRefusals: { type: 'boolean' },
      limitCategory: { type: 'string' },
      ignoreText: { type: 'string' },
    },
    edit: (args) => {
      const defaults = window.rubinThemeDefaults || {}
      const categories = defaults.categories || {}
      const props = args || {}
      const { attributes, setAttributes } = props || {}
      return (
        <div { ...useBlockProps() }>
          <InspectorControls>
            <PanelBody title="Anzeige">
              <PanelRow>
                <div className='tw-w-full'>
                  <TextControl
                    label="Titel"
                    value={attributes.title}
                    onChange={(value) => setAttributes({ title: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <SelectControl
                    label="Trainingskategorie"
                    value={attributes.limitCategory}
                    options={[
                      { label: 'alle anzeigen', value: '' },
                      ...Object.keys(categories).map((key) => ({ label: categories[key], value: key }))
                    ]}
                    onChange={(value) => setAttributes({ limitCategory: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <SelectControl
                    label="Anzahl der Datumsvorschläge"
                    help="ohne abgesagte Termine"
                    value={attributes.dateCount}
                    options={[
                      { label: 'bitte eine Anzahl wählen', value: '' },
                      { label: 'nur den jeweils nächsten Termin', value: '1' },
                      { label: 'die nächsten zwei Termine', value: '2' },
                      { label: 'die nächsten drei Termine', value: '3' },
                      { label: 'die nächsten vier Termine', value: '4' },
                    ]}
                    onChange={(value) => setAttributes({ dateCount: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <ToggleControl
                    label="offene Trainingsgruppen anzeigen"
                    checked={attributes.includeOpen}
                    onChange={(value) => setAttributes({ includeOpen: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <ToggleControl
                    label="geschlossene Trainingsgruppen anzeigen"
                    checked={attributes.includeClosed}
                    onChange={(value) => setAttributes({ includeClosed: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <ToggleControl
                    label="Abgesagte Termine in den Datumsvorschlägen anzeigen"
                    disabled={!attributes.includeOpen && !attributes.includeClosed}
                    checked={attributes.includeRefusals}
                    onChange={(value) => setAttributes({ includeRefusals: value })}
                  />
                </div>
              </PanelRow>
              <PanelRow>
                <div className='tw-w-full'>
                  <TextControl
                    label="Einträge ausschliessen"
                    help=" Einträge mit diesem Text werden NICHT angezeigt. Die Angabe von mehreren Texten ist möglich, wenn diese durch Kommas getrennt sind."
                    value={attributes.ignoreText}
                    placeholder={'z.B. ' + defaults.ignore}
                    onChange={(value) => setAttributes({ ignoreText: value })}
                  />
                </div>
              </PanelRow>
            </PanelBody>
          </InspectorControls>
          <div className="tw-relative tw-w-full tw-p-4">
            <div className="
              tw-flex tw-flex-col tw-justify-center tw-content-center
              tw-border-4 tw-border-solid tw-border-rubinRot
              tw-rounded-lg
              tw-bg-rubinRot-shadow tw-shadow-xl
              tw-px-8 tw-pt-4 tw-pb-8
            ">
              {
                !!attributes.title
                  ? <div>
                      <div className='tw-pt-4 tw-italic'><small>MGVO Trainingsübersicht</small></div>
                      <h2 className='tw-mt-0'>{ attributes.title }</h2>
                    </div>
                  : <h2 className='tw-mt-0'>MGVO Trainingsübersicht</h2>
              }
              {
                !!attributes.limitCategory
                  ? <div>Es werden nur die Trainingstermine für <strong>{ categories[attributes.limitCategory] }</strong> angezeigt.</div>
                  : <div>Es werden <strong>alle</strong> Trainingstermine angezeigt.</div>
              }
              {
                !!attributes.dateCount
                  ? attributes.dateCount == 1
                    ? <div>Es wird immer nur <strong>das nächste</strong> Trainingsdatum angezeigt.</div>
                    : <div>Es werden <strong>{attributes.dateCount}</strong> Datumsvorschläge angezeigt.</div>
                  : <div><strong className='tw-text-rubinRot'>Bitte die Anzahl der Datumsvorschläge festlegen!</strong></div>
              }
              <ul>
                <li>offenene Trainingsgruppen werden <strong>{attributes.includeOpen ? 'angezeigt' : 'ausgeblendet'}</strong></li>
                <li>geschlossene Trainingsgruppen werden <strong>{attributes.includeClosed ? 'angezeigt' : 'ausgeblendet'}</strong></li>
                { (attributes.includeOpen || attributes.includeClosed) &&
                  <li>Trainingsabsagen werden <strong>{attributes.includeRefusals ? 'angezeigt' : 'ausgeblendet'}</strong></li>
                }
              </ul>
              <div className="tw-text-rubinRot"><p><small>Markiere hier das Widget, um seine Einstellungen im Block-Menü bearbeiten zu können.</small></p></div>
            </div>
          </div>
        </div>
      )
    },
}
